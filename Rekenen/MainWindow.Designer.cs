﻿using System.ComponentModel;
using Rekenen.Properties;

namespace Rekenen
{
  partial class MainWindow
  {
    #region Rest

    /// <summary>
    /// Required designer variable.
    /// </summary>
    private IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
        _session.Dispose();
      }
      base.Dispose(disposing);
    }

    #endregion

    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.TitleBar = new System.Windows.Forms.Panel();
      this.Title = new System.Windows.Forms.Label();
      this.Logo = new System.Windows.Forms.Panel();
      this.Minimize = new System.Windows.Forms.Panel();
      this.Exit = new System.Windows.Forms.Panel();
      this.Maximize = new System.Windows.Forms.Panel();
      this.Body = new System.Windows.Forms.Panel();
      this.PersonalStats = new System.Windows.Forms.Button();
      this.Options = new System.Windows.Forms.Button();
      this.StatusBar = new System.Windows.Forms.Panel();
      this.CancelSession = new System.Windows.Forms.Button();
      this.SessionProgress = new System.Windows.Forms.ProgressBar();
      this.Status = new System.Windows.Forms.Label();
      this.Start = new System.Windows.Forms.Button();
      this.TitleBar.SuspendLayout();
      this.Body.SuspendLayout();
      this.StatusBar.SuspendLayout();
      this.SuspendLayout();
      // 
      // TitleBar
      // 
      this.TitleBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.TitleBar.BackColor = System.Drawing.Color.Black;
      this.TitleBar.Controls.Add(this.Title);
      this.TitleBar.Controls.Add(this.Logo);
      this.TitleBar.Controls.Add(this.Minimize);
      this.TitleBar.Controls.Add(this.Exit);
      this.TitleBar.Controls.Add(this.Maximize);
      this.TitleBar.Location = new System.Drawing.Point(0, 0);
      this.TitleBar.Name = "TitleBar";
      this.TitleBar.Size = new System.Drawing.Size(1000, 26);
      this.TitleBar.TabIndex = 0;
      // 
      // Title
      // 
      this.Title.AutoSize = true;
      this.Title.BackColor = System.Drawing.Color.Black;
      this.Title.ForeColor = System.Drawing.Color.White;
      this.Title.Location = new System.Drawing.Point(29, 7);
      this.Title.Name = "Title";
      this.Title.Size = new System.Drawing.Size(51, 13);
      this.Title.TabIndex = 4;
      this.Title.Text = "Rekenen";
      // 
      // Logo
      // 
      this.Logo.BackColor = this.TitleBar.BackColor;
      this.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.Logo.Location = new System.Drawing.Point(3, 3);
      this.Logo.Name = "Logo";
      this.Logo.Size = new System.Drawing.Size(20, 20);
      this.Logo.TabIndex = 3;
      // 
      // Minimize
      // 
      this.Minimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.Minimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
      this.Minimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.Minimize.Location = new System.Drawing.Point(925, 3);
      this.Minimize.Name = "Minimize";
      this.Minimize.Size = new System.Drawing.Size(20, 20);
      this.Minimize.TabIndex = 2;
      // 
      // Exit
      // 
      this.Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.Exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
      this.Exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.Exit.Location = new System.Drawing.Point(977, 3);
      this.Exit.Name = "Exit";
      this.Exit.Size = new System.Drawing.Size(20, 20);
      this.Exit.TabIndex = 0;
      // 
      // Maximize
      // 
      this.Maximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.Maximize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
      this.Maximize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.Maximize.Location = new System.Drawing.Point(951, 3);
      this.Maximize.Name = "Maximize";
      this.Maximize.Size = new System.Drawing.Size(20, 20);
      this.Maximize.TabIndex = 1;
      // 
      // Body
      // 
      this.Body.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.Body.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
      this.Body.Controls.Add(this.PersonalStats);
      this.Body.Controls.Add(this.Options);
      this.Body.Controls.Add(this.StatusBar);
      this.Body.Controls.Add(this.Start);
      this.Body.Location = new System.Drawing.Point(3, 26);
      this.Body.Name = "Body";
      this.Body.Size = new System.Drawing.Size(994, 471);
      this.Body.TabIndex = 0;
      // 
      // PersonalStats
      // 
      this.PersonalStats.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.PersonalStats.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
      this.PersonalStats.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(150)))), ((int)(((byte)(255)))));
      this.PersonalStats.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(100)))), ((int)(((byte)(170)))));
      this.PersonalStats.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.PersonalStats.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.PersonalStats.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
      this.PersonalStats.Location = new System.Drawing.Point(130, 325);
      this.PersonalStats.Name = "PersonalStats";
      this.PersonalStats.Size = new System.Drawing.Size(732, 80);
      this.PersonalStats.TabIndex = 6;
      this.PersonalStats.Text = "Eigen records bekijken";
      this.PersonalStats.UseVisualStyleBackColor = false;
      // 
      // Options
      // 
      this.Options.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.Options.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
      this.Options.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
      this.Options.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
      this.Options.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.Options.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Options.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
      this.Options.Location = new System.Drawing.Point(130, 239);
      this.Options.Name = "Options";
      this.Options.Size = new System.Drawing.Size(732, 80);
      this.Options.TabIndex = 5;
      this.Options.Text = "Instellingen";
      this.Options.UseVisualStyleBackColor = false;
      this.Options.Click += new System.EventHandler(this.Options_Click);
      // 
      // StatusBar
      // 
      this.StatusBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(50)))), ((int)(((byte)(0)))));
      this.StatusBar.Controls.Add(this.CancelSession);
      this.StatusBar.Controls.Add(this.SessionProgress);
      this.StatusBar.Controls.Add(this.Status);
      this.StatusBar.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.StatusBar.Location = new System.Drawing.Point(0, 445);
      this.StatusBar.Name = "StatusBar";
      this.StatusBar.Size = new System.Drawing.Size(994, 26);
      this.StatusBar.TabIndex = 0;
      // 
      // CancelSession
      // 
      this.CancelSession.Location = new System.Drawing.Point(367, 7);
      this.CancelSession.Name = "CancelSession";
      this.CancelSession.Size = new System.Drawing.Size(13, 13);
      this.CancelSession.TabIndex = 0;
      this.CancelSession.UseVisualStyleBackColor = true;
      this.CancelSession.Visible = false;
      // 
      // SessionProgress
      // 
      this.SessionProgress.Location = new System.Drawing.Point(86, 7);
      this.SessionProgress.Name = "SessionProgress";
      this.SessionProgress.Size = new System.Drawing.Size(275, 13);
      this.SessionProgress.TabIndex = 1;
      this.SessionProgress.Visible = false;
      // 
      // Status
      // 
      this.Status.AutoSize = true;
      this.Status.ForeColor = System.Drawing.Color.White;
      this.Status.Location = new System.Drawing.Point(9, 7);
      this.Status.Name = "Status";
      this.Status.Size = new System.Drawing.Size(47, 13);
      this.Status.TabIndex = 0;
      this.Status.Text = "<status>";
      this.Status.Visible = false;
      // 
      // Start
      // 
      this.Start.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.Start.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
      this.Start.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(150)))), ((int)(((byte)(50)))));
      this.Start.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(170)))), ((int)(((byte)(60)))));
      this.Start.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.Start.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Start.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
      this.Start.Location = new System.Drawing.Point(130, 33);
      this.Start.Name = "Start";
      this.Start.Size = new System.Drawing.Size(240, 120);
      this.Start.TabIndex = 1;
      this.Start.Text = "Start";
      this.Start.UseVisualStyleBackColor = false;
      // 
      // MainWindow
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Black;
      this.ClientSize = new System.Drawing.Size(1000, 500);
      this.Controls.Add(this.Body);
      this.Controls.Add(this.TitleBar);
      this.DoubleBuffered = true;
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Icon = global::Rekenen.Properties.Resources.DefaultIcon;
      this.MinimumSize = new System.Drawing.Size(1000, 500);
      this.Name = "MainWindow";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Rekenen";
      this.TitleBar.ResumeLayout(false);
      this.TitleBar.PerformLayout();
      this.Body.ResumeLayout(false);
      this.StatusBar.ResumeLayout(false);
      this.StatusBar.PerformLayout();
      this.ResumeLayout(false);

    }

    private System.Windows.Forms.Panel TitleBar;
    private System.Windows.Forms.Panel Body;
    private System.Windows.Forms.Panel StatusBar;
    private System.Windows.Forms.Panel Minimize;
    private System.Windows.Forms.Panel Maximize;
    private System.Windows.Forms.Panel Exit;
    private System.Windows.Forms.Panel Logo;
    private System.Windows.Forms.Button Start;
    private System.Windows.Forms.Button PersonalStats;
    private System.Windows.Forms.Button Options;
    private System.Windows.Forms.Label Title;
    private System.Windows.Forms.Label Status;
    private System.Windows.Forms.ProgressBar SessionProgress;
    private System.Windows.Forms.Button CancelSession;
  }
}