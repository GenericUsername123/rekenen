﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Rekenen.Properties;

namespace Rekenen
{
  public partial class RecordsWindow : Form
  {
    private readonly IList<Panel> _records = new List<Panel>();

    public RecordsWindow()
    {
      InitializeComponent();
      CenterToParent();
      ConstructBody();
      AttachEvents();
    }

    private void ConstructBody()
    {
      var values = Resources.RecordsWindow_Values.Split(';');
      var record = PersonalRecords.Records.GetEnumerator();
      for (var i = 0; i < PersonalRecords.Records.Count && record.MoveNext(); i++) Add(i, values, record.Current);
    }

    private void Add(int index, IList<string> values, KeyValuePair<string, PersonalRecords.PersonalRecord> record)
    {
      _records.Add(GetPanel(index, values, record));
      _records[index].Controls[2].Click += Info_Click;
      _records[index].Controls[3].Click += Del_Click;
      var contents = values[1] + Helper.MsToString(record.Value.TimeSpent) + '\n' + values[2] +
                     record.Value.Accuracy.ToString("P");
      _toolTip.SetToolTip(_records[index], contents);
      foreach (Control control in _records[index].Controls) _toolTip.SetToolTip(control, contents);
      Reposition(_records[index].Name);
    }

    private Panel GetPanel(int index, IList<string> values, KeyValuePair<string, PersonalRecords.PersonalRecord> record)
    {
      return new Panel
      {
        Parent = Body,
        BackColor = index % 2 == 0 ? Color.FromArgb(0x33, 0x33, 0x33) : Color.FromArgb(0x23, 0x23, 0x23),
        Name = record.Key,
        Dock = DockStyle.Top,
        Height = 50,
        TabIndex = PersonalRecords.Records.Count - index,
        ForeColor = Color.FromArgb(0xcc, 0xcc, 0xcc),
        Controls =
        {
          new Label
          {
            Text = Helper.Translate(record.Key),
            Name = record.Key + "Profile",
            AutoSize = true,
            Left = 3
          },
          new Label
          {
            Text = values[0] + record.Value.Score + "",
            Name = record.Key + "Score",
            AutoSize = true
          },
          new Button
          {
            FlatStyle = FlatStyle.Flat,
            FlatAppearance =
            {
              BorderColor = index%2 == 0 ? Color.FromArgb(0x23, 0x23, 0x23) : Color.FromArgb(0x33, 0x33, 0x33)
            },
            // ReSharper disable once LocalizableElement
            Text = "Info",
            Name = record.Key + "Info",
            AutoSize = true,
            Top = 3
          },
          new Button
          {
            FlatStyle = FlatStyle.Flat,
            FlatAppearance =
            {
              BorderColor = index%2 == 0 ? Color.FromArgb(0x23, 0x23, 0x23) : Color.FromArgb(0x33, 0x33, 0x33)
            },
            // ReSharper disable once LocalizableElement
            Text = "Verwijder",
            Name = record.Key + "Delete",
            AutoSize = true,
            Top = 3
          }
        }
      };
    }

    private void Reposition(string key)
    {
      foreach (var record in _records)
      {
        if (record.Name != key) continue;
        record.Controls[0].Top = (record.Controls[2].Height - record.Controls[0].Height) / 2 + 3;
        record.Controls[1].Top = (record.Controls[2].Height - record.Controls[1].Height) / 2 + 3;
        record.Controls[1].Left = 120;
        record.Controls[2].Left = record.Width - record.Controls[2].Width - 3;
        record.Controls[3].Left = -record.Controls[3].Width + -3 + record.Controls[2].Left;
        record.Height = record.Controls[2].Height + 6;
        break;
      }
    }

    private void AttachEvents()
    {
      Body.Resize += (sender, e) =>
      {
        foreach (var record in _records) Reposition(record.Name);
      };
    }

    private void RecordsWindow_Shown(object sender, EventArgs e)
    {
      if (PersonalRecords.Records.Count != 0) return;
      Close();
      MessageBox.Show(Resources.RecordsWindow_NoRecordsToDisplay, Resources.RecordsWindow_NoRecords,
        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
    }

    private void Info_Click(object sender, EventArgs e)
    {
      var info = (Button)sender;
      var key = info.Name.Substring(0, info.Name.LastIndexOf("Info", StringComparison.InvariantCulture));
      PersonalRecords.PersonalRecord value;
      if (!PersonalRecords.Records.TryGetValue(key, out value)) return;
      if (value.Setup == null) return;
      new OptionWindow(value.Setup, key).ShowDialog(this);
    }

    private void Del_Click(object sender, EventArgs e)
    {
      var del = (Button)sender;
      var key = del.Name.Substring(0, del.Name.LastIndexOf("Delete", StringComparison.InvariantCulture));
      PersonalRecords.Records.Remove(key);
      foreach (var record in _records)
      {
        if (record.Name != key) continue;
        record.Dispose();
        _records.Remove(record);
        break;
      }
    }
  }
}