﻿using System.Drawing;
using System.Windows.Forms;
using GuiTools.Properties;
using static System.Windows.Forms.FormWindowState;

namespace GuiTools
{
  public static class TitleBar
  {
    public static readonly string[] Names = { "Minimize", "Maximize", "Exit", "Logo" };
    private static Point _cursorLocation;
    public static Color DefaultColor = Color.FromArgb(127, 127, 127);
    public static Color HoverColor = Color.Black;

    public static void Initialize(Form owner, Control titleBar)
    {
      titleBar.MouseMove += (sender, e) => Update(owner, e);
      titleBar.DoubleClick += (sender, e) => owner.WindowState = owner.WindowState == Maximized ? Normal : Maximized;
      foreach (Control control in titleBar.Controls)
      {
        for (byte n = 0; n < Names.Length; n++)
        {
          if (control.Name != Names[n]) continue;
          switch (n)
          {
            case 0:
              control.Click += (sender, e) => owner.WindowState = Minimized;
              AlterBackground(control, Resources.MinimizeDefault, Resources.MinimizeHover);
              break;
            case 1:
              control.Click += (sender, e) => owner.WindowState = owner.WindowState == Maximized ? Normal : Maximized;
              var defaultImage = Resources.MinimizeDefault;
              var hoverImage = Resources.MinimizeHover;
              defaultImage.RotateFlip(RotateFlipType.Rotate180FlipX);
              hoverImage.RotateFlip(RotateFlipType.Rotate180FlipX);
              AlterBackground(control, defaultImage, hoverImage);
              break;
            case 2:
              control.Click += (sender, e) => owner.Close();
              AlterBackground(control, Resources.ExitDefault, Resources.ExitHover);
              break;
            case 3:
              var logoMenuStrip = CreateLogoMenuStrip(owner);
              control.Click += (sender, e) =>
                logoMenuStrip.Show(new Point(owner.Location.X, owner.Location.Y + titleBar.Height));
              break;
          }
          break;
        }
      }
    }

    private static void AlterBackground(Control control, Image defaultImage, Image hoverImage)
    {
      control.BackColor = DefaultColor;
      control.BackgroundImage = defaultImage;
      control.MouseEnter += (sender, e) => control.BackColor = HoverColor;
      control.MouseLeave += (sender, e) => control.BackColor = DefaultColor;
      control.MouseEnter += (sender, e) => control.BackgroundImage = hoverImage;
      control.MouseLeave += (sender, e) => control.BackgroundImage = defaultImage;
    }

    private static ToolStripDropDown CreateLogoMenuStrip(Form owner)
    {
      var toolStripDropDown = new ToolStripDropDown
      {
        Items =
        {
          new ToolStripMenuItem
          {
            Name = "ExitToolStripMenu",
            Size = new Size(110, 22),
            Text = Resources.TitleBar_Close
          }
        },
        Name = "LogoMenuStrip",
        Size = new Size(111, 26)
      };
      toolStripDropDown.Click += (sender, e) => owner.Close();
      return toolStripDropDown;
    }

    private static void Update(Form sender, MouseEventArgs e)
    {
      if (e.Button == MouseButtons.Left && sender.WindowState == Normal)
      {
        if (_cursorLocation.X == -1) _cursorLocation = e.Location;
        sender.Location = new Point(sender.Location.X - (_cursorLocation.X - e.Location.X),
          sender.Location.Y - (_cursorLocation.Y - e.Location.Y));
      }
      else _cursorLocation = new Point(-1, -1);
    }
  }
}