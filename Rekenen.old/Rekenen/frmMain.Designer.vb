﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.grpCurrentVersion = New System.Windows.Forms.GroupBox()
        Me.Hulp = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdDeleteEvaluation = New System.Windows.Forms.Button()
        Me.cmdShowEvaluations = New System.Windows.Forms.Button()
        Me.cmdNewUser = New System.Windows.Forms.Button()
        Me.lblUsername = New System.Windows.Forms.Label()
        Me.lblAge = New System.Windows.Forms.Label()
        Me.rdoHard = New System.Windows.Forms.RadioButton()
        Me.rdoMedium = New System.Windows.Forms.RadioButton()
        Me.rdoEasy = New System.Windows.Forms.RadioButton()
        Me.cmdClear = New System.Windows.Forms.Button()
        Me.grpInfo = New System.Windows.Forms.GroupBox()
        Me.cmdCalcRules = New System.Windows.Forms.Button()
        Me.webInfo = New System.Windows.Forms.WebBrowser()
        Me.rtbCorrection = New System.Windows.Forms.RichTextBox()
        Me.cmdStart = New System.Windows.Forms.Button()
        Me.tmrClicks = New System.Windows.Forms.Timer(Me.components)
        Me.tmrRefreshForm = New System.Windows.Forms.Timer(Me.components)
        Me.grpConfig = New System.Windows.Forms.GroupBox()
        Me.tctrlType = New System.Windows.Forms.TabControl()
        Me.tpgSum = New System.Windows.Forms.TabPage()
        Me.tpgDifference = New System.Windows.Forms.TabPage()
        Me.tpgProduct = New System.Windows.Forms.TabPage()
        Me.tpgQuotient = New System.Windows.Forms.TabPage()
        Me.lblSymbol = New System.Windows.Forms.Label()
        Me.lblSelected = New System.Windows.Forms.Label()
        Me.lblShowType = New System.Windows.Forms.Label()
        Me.grpAdvanced = New System.Windows.Forms.GroupBox()
        Me.txtQuantityOfNums = New System.Windows.Forms.TextBox()
        Me.lblQuantityOfNums = New System.Windows.Forms.Label()
        Me.chkNoZero = New System.Windows.Forms.CheckBox()
        Me.txtMinOne = New System.Windows.Forms.TextBox()
        Me.lblMinOne = New System.Windows.Forms.Label()
        Me.txtMaxOne = New System.Windows.Forms.TextBox()
        Me.lblMaxOne = New System.Windows.Forms.Label()
        Me.txtMinOutput = New System.Windows.Forms.TextBox()
        Me.lblMinOutput = New System.Windows.Forms.Label()
        Me.txtMaxOutput = New System.Windows.Forms.TextBox()
        Me.lblMaxOutput = New System.Windows.Forms.Label()
        Me.cmdTestMode = New System.Windows.Forms.Button()
        Me.grpDifficulty = New System.Windows.Forms.GroupBox()
        Me.pnlChangeOfUser = New System.Windows.Forms.Panel()
        Me.txtAge = New System.Windows.Forms.TextBox()
        Me.txtUsername = New System.Windows.Forms.TextBox()
        Me.grpInfo.SuspendLayout()
        Me.grpConfig.SuspendLayout()
        Me.tctrlType.SuspendLayout()
        Me.tpgQuotient.SuspendLayout()
        Me.grpAdvanced.SuspendLayout()
        Me.grpDifficulty.SuspendLayout()
        Me.pnlChangeOfUser.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpCurrentVersion
        '
        Me.grpCurrentVersion.BackColor = System.Drawing.Color.Transparent
        Me.grpCurrentVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.grpCurrentVersion.Location = New System.Drawing.Point(93, 12)
        Me.grpCurrentVersion.MaximumSize = New System.Drawing.Size(185, 40)
        Me.grpCurrentVersion.MinimumSize = New System.Drawing.Size(185, 0)
        Me.grpCurrentVersion.Name = "grpCurrentVersion"
        Me.grpCurrentVersion.Size = New System.Drawing.Size(185, 25)
        Me.grpCurrentVersion.TabIndex = 1
        Me.grpCurrentVersion.TabStop = False
        '
        'Hulp
        '
        Me.Hulp.AutomaticDelay = 1500
        Me.Hulp.AutoPopDelay = 20000
        Me.Hulp.InitialDelay = 1500
        Me.Hulp.IsBalloon = True
        Me.Hulp.ReshowDelay = 300
        Me.Hulp.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.Hulp.ToolTipTitle = "Hulp:"
        '
        'cmdDeleteEvaluation
        '
        Me.cmdDeleteEvaluation.AutoSize = True
        Me.cmdDeleteEvaluation.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.cmdDeleteEvaluation.BackColor = System.Drawing.Color.WhiteSmoke
        Me.cmdDeleteEvaluation.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdDeleteEvaluation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdDeleteEvaluation.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.cmdDeleteEvaluation.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDeleteEvaluation.Location = New System.Drawing.Point(284, 12)
        Me.cmdDeleteEvaluation.Name = "cmdDeleteEvaluation"
        Me.cmdDeleteEvaluation.Size = New System.Drawing.Size(100, 25)
        Me.cmdDeleteEvaluation.TabIndex = 8
        Me.cmdDeleteEvaluation.Text = "Prestaties wissen"
        Me.Hulp.SetToolTip(Me.cmdDeleteEvaluation, "Dit wist alle gegevens van de vorige prestaties en is 100% onherstelbaar.")
        Me.cmdDeleteEvaluation.UseVisualStyleBackColor = False
        '
        'cmdShowEvaluations
        '
        Me.cmdShowEvaluations.AutoSize = True
        Me.cmdShowEvaluations.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.cmdShowEvaluations.BackColor = System.Drawing.Color.WhiteSmoke
        Me.cmdShowEvaluations.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdShowEvaluations.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdShowEvaluations.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.cmdShowEvaluations.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdShowEvaluations.Location = New System.Drawing.Point(390, 12)
        Me.cmdShowEvaluations.Name = "cmdShowEvaluations"
        Me.cmdShowEvaluations.Size = New System.Drawing.Size(95, 25)
        Me.cmdShowEvaluations.TabIndex = 7
        Me.cmdShowEvaluations.Text = "Prestaties tonen"
        Me.Hulp.SetToolTip(Me.cmdShowEvaluations, "Toon je vorige prestaties.")
        Me.cmdShowEvaluations.UseVisualStyleBackColor = False
        '
        'cmdNewUser
        '
        Me.cmdNewUser.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdNewUser.AutoSize = True
        Me.cmdNewUser.BackColor = System.Drawing.Color.WhiteSmoke
        Me.cmdNewUser.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdNewUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdNewUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.cmdNewUser.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNewUser.Location = New System.Drawing.Point(366, 3)
        Me.cmdNewUser.Name = "cmdNewUser"
        Me.cmdNewUser.Size = New System.Drawing.Size(104, 25)
        Me.cmdNewUser.TabIndex = 2
        Me.cmdNewUser.Text = "Nieuwe gebruiker"
        Me.Hulp.SetToolTip(Me.cmdNewUser, "Vul je leeftijd en naam en druk op de knop om je te laten registreren.")
        Me.cmdNewUser.UseVisualStyleBackColor = False
        '
        'lblUsername
        '
        Me.lblUsername.AutoSize = True
        Me.lblUsername.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.75!)
        Me.lblUsername.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblUsername.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblUsername.Location = New System.Drawing.Point(4, 7)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(102, 15)
        Me.lblUsername.TabIndex = 32
        Me.lblUsername.Text = "Gebruikersnaam:"
        Me.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Hulp.SetToolTip(Me.lblUsername, "Vul hier je naam in als je wilt duidelijk maken wie je bent.")
        '
        'lblAge
        '
        Me.lblAge.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblAge.AutoSize = True
        Me.lblAge.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.75!)
        Me.lblAge.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAge.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblAge.Location = New System.Drawing.Point(272, 7)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(50, 15)
        Me.lblAge.TabIndex = 36
        Me.lblAge.Text = "Leeftijd:"
        Me.lblAge.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Hulp.SetToolTip(Me.lblAge, "Vul hier je naam in als je wilt duidelijk maken wie je bent.")
        '
        'rdoHard
        '
        Me.rdoHard.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rdoHard.AutoSize = True
        Me.rdoHard.Cursor = System.Windows.Forms.Cursors.Hand
        Me.rdoHard.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.rdoHard.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rdoHard.Location = New System.Drawing.Point(384, 21)
        Me.rdoHard.Name = "rdoHard"
        Me.rdoHard.Size = New System.Drawing.Size(60, 17)
        Me.rdoHard.TabIndex = 2
        Me.rdoHard.Text = "Moeilijk"
        Me.Hulp.SetToolTip(Me.rdoHard, "Een niveau op voorhand ingesteld.")
        Me.rdoHard.UseVisualStyleBackColor = True
        '
        'rdoMedium
        '
        Me.rdoMedium.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.rdoMedium.AutoSize = True
        Me.rdoMedium.Cursor = System.Windows.Forms.Cursors.Hand
        Me.rdoMedium.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.rdoMedium.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rdoMedium.Location = New System.Drawing.Point(197, 21)
        Me.rdoMedium.Name = "rdoMedium"
        Me.rdoMedium.Size = New System.Drawing.Size(75, 17)
        Me.rdoMedium.TabIndex = 1
        Me.rdoMedium.Text = "Gemiddeld"
        Me.Hulp.SetToolTip(Me.rdoMedium, "Een niveau op voorhand ingesteld.")
        Me.rdoMedium.UseVisualStyleBackColor = True
        '
        'rdoEasy
        '
        Me.rdoEasy.AutoSize = True
        Me.rdoEasy.Checked = True
        Me.rdoEasy.Cursor = System.Windows.Forms.Cursors.Hand
        Me.rdoEasy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.rdoEasy.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rdoEasy.Location = New System.Drawing.Point(6, 21)
        Me.rdoEasy.Name = "rdoEasy"
        Me.rdoEasy.Size = New System.Drawing.Size(83, 17)
        Me.rdoEasy.TabIndex = 0
        Me.rdoEasy.TabStop = True
        Me.rdoEasy.Text = "Gemakkelijk"
        Me.Hulp.SetToolTip(Me.rdoEasy, "Een niveau op voorhand ingesteld.")
        Me.rdoEasy.UseVisualStyleBackColor = True
        '
        'cmdClear
        '
        Me.cmdClear.AutoSize = True
        Me.cmdClear.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.cmdClear.BackColor = System.Drawing.Color.Transparent
        Me.cmdClear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdClear.Location = New System.Drawing.Point(354, 7)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.Size = New System.Drawing.Size(104, 25)
        Me.cmdClear.TabIndex = 4
        Me.cmdClear.Text = "Ongedaan maken"
        Me.Hulp.SetToolTip(Me.cmdClear, "Gebruik het menu om te kunnen starten.")
        Me.cmdClear.UseVisualStyleBackColor = False
        '
        'grpInfo
        '
        Me.grpInfo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpInfo.BackColor = System.Drawing.Color.Transparent
        Me.grpInfo.Controls.Add(Me.cmdCalcRules)
        Me.grpInfo.Controls.Add(Me.webInfo)
        Me.grpInfo.Controls.Add(Me.rtbCorrection)
        Me.grpInfo.Cursor = System.Windows.Forms.Cursors.Default
        Me.grpInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.grpInfo.Location = New System.Drawing.Point(491, 12)
        Me.grpInfo.Name = "grpInfo"
        Me.grpInfo.Size = New System.Drawing.Size(441, 478)
        Me.grpInfo.TabIndex = 2
        Me.grpInfo.TabStop = False
        Me.grpInfo.Text = "Info"
        '
        'cmdCalcRules
        '
        Me.cmdCalcRules.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCalcRules.BackColor = System.Drawing.Color.WhiteSmoke
        Me.cmdCalcRules.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdCalcRules.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdCalcRules.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.cmdCalcRules.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCalcRules.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCalcRules.Location = New System.Drawing.Point(359, 18)
        Me.cmdCalcRules.Name = "cmdCalcRules"
        Me.cmdCalcRules.Size = New System.Drawing.Size(79, 24)
        Me.cmdCalcRules.TabIndex = 0
        Me.cmdCalcRules.Text = "Rekenregels"
        Me.cmdCalcRules.UseVisualStyleBackColor = False
        '
        'webInfo
        '
        Me.webInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.webInfo.Location = New System.Drawing.Point(3, 18)
        Me.webInfo.MinimumSize = New System.Drawing.Size(20, 20)
        Me.webInfo.Name = "webInfo"
        Me.webInfo.ScrollBarsEnabled = False
        Me.webInfo.Size = New System.Drawing.Size(435, 457)
        Me.webInfo.TabIndex = 19
        Me.webInfo.Url = New System.Uri("", System.UriKind.Relative)
        '
        'rtbCorrection
        '
        Me.rtbCorrection.BackColor = System.Drawing.Color.AliceBlue
        Me.rtbCorrection.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtbCorrection.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.rtbCorrection.ForeColor = System.Drawing.Color.Black
        Me.rtbCorrection.Location = New System.Drawing.Point(3, 18)
        Me.rtbCorrection.Name = "rtbCorrection"
        Me.rtbCorrection.ReadOnly = True
        Me.rtbCorrection.Size = New System.Drawing.Size(435, 457)
        Me.rtbCorrection.TabIndex = 18
        Me.rtbCorrection.TabStop = False
        Me.rtbCorrection.Text = "Voorbeeldtekst"
        '
        'cmdStart
        '
        Me.cmdStart.AccessibleName = ""
        Me.cmdStart.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.cmdStart.BackColor = System.Drawing.Color.WhiteSmoke
        Me.cmdStart.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdStart.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.cmdStart.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdStart.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdStart.Location = New System.Drawing.Point(12, 12)
        Me.cmdStart.Name = "cmdStart"
        Me.cmdStart.Size = New System.Drawing.Size(75, 25)
        Me.cmdStart.TabIndex = 0
        Me.cmdStart.Text = "Start"
        Me.cmdStart.UseVisualStyleBackColor = False
        '
        'tmrClicks
        '
        Me.tmrClicks.Interval = 300
        '
        'tmrRefreshForm
        '
        Me.tmrRefreshForm.Enabled = True
        Me.tmrRefreshForm.Interval = 1000
        '
        'grpConfig
        '
        Me.grpConfig.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.grpConfig.BackColor = System.Drawing.Color.Transparent
        Me.grpConfig.Controls.Add(Me.tctrlType)
        Me.grpConfig.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpConfig.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.grpConfig.Location = New System.Drawing.Point(12, 43)
        Me.grpConfig.Name = "grpConfig"
        Me.grpConfig.Size = New System.Drawing.Size(473, 417)
        Me.grpConfig.TabIndex = 26
        Me.grpConfig.TabStop = False
        Me.grpConfig.Text = "Configuratie"
        '
        'tctrlType
        '
        Me.tctrlType.Controls.Add(Me.tpgSum)
        Me.tctrlType.Controls.Add(Me.tpgDifference)
        Me.tctrlType.Controls.Add(Me.tpgProduct)
        Me.tctrlType.Controls.Add(Me.tpgQuotient)
        Me.tctrlType.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tctrlType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tctrlType.Location = New System.Drawing.Point(3, 20)
        Me.tctrlType.Name = "tctrlType"
        Me.tctrlType.SelectedIndex = 0
        Me.tctrlType.Size = New System.Drawing.Size(467, 394)
        Me.tctrlType.TabIndex = 1
        '
        'tpgSum
        '
        Me.tpgSum.BackColor = System.Drawing.Color.Transparent
        Me.tpgSum.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tpgSum.Location = New System.Drawing.Point(4, 22)
        Me.tpgSum.Name = "tpgSum"
        Me.tpgSum.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgSum.Size = New System.Drawing.Size(459, 368)
        Me.tpgSum.TabIndex = 0
        Me.tpgSum.Text = "Som, ""+"""
        '
        'tpgDifference
        '
        Me.tpgDifference.BackColor = System.Drawing.Color.Transparent
        Me.tpgDifference.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tpgDifference.Location = New System.Drawing.Point(4, 22)
        Me.tpgDifference.Name = "tpgDifference"
        Me.tpgDifference.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgDifference.Size = New System.Drawing.Size(459, 368)
        Me.tpgDifference.TabIndex = 1
        Me.tpgDifference.Text = "Verschil, ""-"""
        '
        'tpgProduct
        '
        Me.tpgProduct.BackColor = System.Drawing.Color.Transparent
        Me.tpgProduct.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tpgProduct.Location = New System.Drawing.Point(4, 22)
        Me.tpgProduct.Name = "tpgProduct"
        Me.tpgProduct.Size = New System.Drawing.Size(459, 368)
        Me.tpgProduct.TabIndex = 2
        Me.tpgProduct.Text = "Product, ""x"""
        '
        'tpgQuotient
        '
        Me.tpgQuotient.BackColor = System.Drawing.Color.Transparent
        Me.tpgQuotient.Controls.Add(Me.lblSymbol)
        Me.tpgQuotient.Controls.Add(Me.lblSelected)
        Me.tpgQuotient.Controls.Add(Me.lblShowType)
        Me.tpgQuotient.Controls.Add(Me.cmdClear)
        Me.tpgQuotient.Controls.Add(Me.grpAdvanced)
        Me.tpgQuotient.Controls.Add(Me.cmdTestMode)
        Me.tpgQuotient.Controls.Add(Me.grpDifficulty)
        Me.tpgQuotient.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tpgQuotient.Location = New System.Drawing.Point(4, 22)
        Me.tpgQuotient.Name = "tpgQuotient"
        Me.tpgQuotient.Size = New System.Drawing.Size(459, 368)
        Me.tpgQuotient.TabIndex = 3
        Me.tpgQuotient.Text = "Quotiënt, "":"""
        '
        'lblSymbol
        '
        Me.lblSymbol.BackColor = System.Drawing.Color.Silver
        Me.lblSymbol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSymbol.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSymbol.ForeColor = System.Drawing.Color.Black
        Me.lblSymbol.Location = New System.Drawing.Point(301, 7)
        Me.lblSymbol.Name = "lblSymbol"
        Me.lblSymbol.Size = New System.Drawing.Size(47, 58)
        Me.lblSymbol.TabIndex = 52
        Me.lblSymbol.Text = ":"
        Me.lblSymbol.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSelected
        '
        Me.lblSelected.BackColor = System.Drawing.Color.Transparent
        Me.lblSelected.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelected.Location = New System.Drawing.Point(6, 7)
        Me.lblSelected.Name = "lblSelected"
        Me.lblSelected.Size = New System.Drawing.Size(169, 24)
        Me.lblSelected.TabIndex = 51
        Me.lblSelected.Text = "Geselecteerd:"
        Me.lblSelected.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblShowType
        '
        Me.lblShowType.BackColor = System.Drawing.Color.Transparent
        Me.lblShowType.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShowType.Location = New System.Drawing.Point(6, 31)
        Me.lblShowType.Name = "lblShowType"
        Me.lblShowType.Size = New System.Drawing.Size(277, 31)
        Me.lblShowType.TabIndex = 50
        Me.lblShowType.Text = "oefeningen op gedeelt door"
        Me.lblShowType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'grpAdvanced
        '
        Me.grpAdvanced.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpAdvanced.BackColor = System.Drawing.Color.Transparent
        Me.grpAdvanced.Controls.Add(Me.txtQuantityOfNums)
        Me.grpAdvanced.Controls.Add(Me.lblQuantityOfNums)
        Me.grpAdvanced.Controls.Add(Me.chkNoZero)
        Me.grpAdvanced.Controls.Add(Me.txtMinOne)
        Me.grpAdvanced.Controls.Add(Me.lblMinOne)
        Me.grpAdvanced.Controls.Add(Me.txtMaxOne)
        Me.grpAdvanced.Controls.Add(Me.lblMaxOne)
        Me.grpAdvanced.Controls.Add(Me.txtMinOutput)
        Me.grpAdvanced.Controls.Add(Me.lblMinOutput)
        Me.grpAdvanced.Controls.Add(Me.txtMaxOutput)
        Me.grpAdvanced.Controls.Add(Me.lblMaxOutput)
        Me.grpAdvanced.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.grpAdvanced.Location = New System.Drawing.Point(3, 133)
        Me.grpAdvanced.Name = "grpAdvanced"
        Me.grpAdvanced.Size = New System.Drawing.Size(453, 232)
        Me.grpAdvanced.TabIndex = 3
        Me.grpAdvanced.TabStop = False
        Me.grpAdvanced.Text = "Geavanceerde instellingen"
        '
        'txtQuantityOfNums
        '
        Me.txtQuantityOfNums.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtQuantityOfNums.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtQuantityOfNums.Location = New System.Drawing.Point(426, 92)
        Me.txtQuantityOfNums.MaxLength = 1
        Me.txtQuantityOfNums.Name = "txtQuantityOfNums"
        Me.txtQuantityOfNums.Size = New System.Drawing.Size(21, 20)
        Me.txtQuantityOfNums.TabIndex = 5
        Me.txtQuantityOfNums.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblQuantityOfNums
        '
        Me.lblQuantityOfNums.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblQuantityOfNums.AutoSize = True
        Me.lblQuantityOfNums.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.lblQuantityOfNums.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblQuantityOfNums.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblQuantityOfNums.Location = New System.Drawing.Point(267, 95)
        Me.lblQuantityOfNums.Name = "lblQuantityOfNums"
        Me.lblQuantityOfNums.Size = New System.Drawing.Size(153, 13)
        Me.lblQuantityOfNums.TabIndex = 49
        Me.lblQuantityOfNums.Text = "Aantal getallen in een oefening"
        '
        'chkNoZero
        '
        Me.chkNoZero.AutoSize = True
        Me.chkNoZero.Cursor = System.Windows.Forms.Cursors.Hand
        Me.chkNoZero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.chkNoZero.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkNoZero.Location = New System.Drawing.Point(6, 94)
        Me.chkNoZero.Name = "chkNoZero"
        Me.chkNoZero.Size = New System.Drawing.Size(154, 17)
        Me.chkNoZero.TabIndex = 4
        Me.chkNoZero.Text = "Alle oefeningen zonder nul."
        Me.chkNoZero.UseVisualStyleBackColor = True
        '
        'txtMinOne
        '
        Me.txtMinOne.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMinOne.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtMinOne.Location = New System.Drawing.Point(422, 47)
        Me.txtMinOne.MaxLength = 3
        Me.txtMinOne.Name = "txtMinOne"
        Me.txtMinOne.Size = New System.Drawing.Size(25, 20)
        Me.txtMinOne.TabIndex = 3
        '
        'lblMinOne
        '
        Me.lblMinOne.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMinOne.AutoSize = True
        Me.lblMinOne.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.lblMinOne.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblMinOne.Location = New System.Drawing.Point(194, 50)
        Me.lblMinOne.Name = "lblMinOne"
        Me.lblMinOne.Size = New System.Drawing.Size(222, 13)
        Me.lblMinOne.TabIndex = 48
        Me.lblMinOne.Text = "Minimaal aantal keer 1 als getal in procent (%)"
        '
        'txtMaxOne
        '
        Me.txtMaxOne.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMaxOne.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtMaxOne.Location = New System.Drawing.Point(422, 21)
        Me.txtMaxOne.MaxLength = 3
        Me.txtMaxOne.Name = "txtMaxOne"
        Me.txtMaxOne.Size = New System.Drawing.Size(25, 20)
        Me.txtMaxOne.TabIndex = 2
        '
        'lblMaxOne
        '
        Me.lblMaxOne.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMaxOne.AutoSize = True
        Me.lblMaxOne.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.lblMaxOne.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblMaxOne.Location = New System.Drawing.Point(191, 24)
        Me.lblMaxOne.Name = "lblMaxOne"
        Me.lblMaxOne.Size = New System.Drawing.Size(225, 13)
        Me.lblMaxOne.TabIndex = 47
        Me.lblMaxOne.Text = "Maximaal aantal keer 1 als getal in procent (%)"
        '
        'txtMinOutput
        '
        Me.txtMinOutput.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtMinOutput.Location = New System.Drawing.Point(105, 47)
        Me.txtMinOutput.MaxLength = 4
        Me.txtMinOutput.Name = "txtMinOutput"
        Me.txtMinOutput.Size = New System.Drawing.Size(31, 20)
        Me.txtMinOutput.TabIndex = 1
        '
        'lblMinOutput
        '
        Me.lblMinOutput.AutoSize = True
        Me.lblMinOutput.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.lblMinOutput.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblMinOutput.Location = New System.Drawing.Point(9, 50)
        Me.lblMinOutput.Name = "lblMinOutput"
        Me.lblMinOutput.Size = New System.Drawing.Size(90, 13)
        Me.lblMinOutput.TabIndex = 45
        Me.lblMinOutput.Text = "Minimale uitkomst"
        '
        'txtMaxOutput
        '
        Me.txtMaxOutput.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtMaxOutput.Location = New System.Drawing.Point(105, 21)
        Me.txtMaxOutput.MaxLength = 4
        Me.txtMaxOutput.Name = "txtMaxOutput"
        Me.txtMaxOutput.Size = New System.Drawing.Size(31, 20)
        Me.txtMaxOutput.TabIndex = 0
        '
        'lblMaxOutput
        '
        Me.lblMaxOutput.AutoSize = True
        Me.lblMaxOutput.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.lblMaxOutput.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMaxOutput.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblMaxOutput.Location = New System.Drawing.Point(6, 24)
        Me.lblMaxOutput.Name = "lblMaxOutput"
        Me.lblMaxOutput.Size = New System.Drawing.Size(93, 13)
        Me.lblMaxOutput.TabIndex = 42
        Me.lblMaxOutput.Text = "Maximale uitkomst"
        '
        'cmdTestMode
        '
        Me.cmdTestMode.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdTestMode.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.cmdTestMode.BackColor = System.Drawing.Color.Transparent
        Me.cmdTestMode.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdTestMode.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdTestMode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.cmdTestMode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdTestMode.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTestMode.Location = New System.Drawing.Point(354, 38)
        Me.cmdTestMode.Name = "cmdTestMode"
        Me.cmdTestMode.Size = New System.Drawing.Size(105, 24)
        Me.cmdTestMode.TabIndex = 100
        Me.cmdTestMode.Text = "Test modus"
        Me.cmdTestMode.UseVisualStyleBackColor = False
        Me.cmdTestMode.Visible = False
        '
        'grpDifficulty
        '
        Me.grpDifficulty.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpDifficulty.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.grpDifficulty.BackColor = System.Drawing.Color.Transparent
        Me.grpDifficulty.Controls.Add(Me.rdoHard)
        Me.grpDifficulty.Controls.Add(Me.rdoMedium)
        Me.grpDifficulty.Controls.Add(Me.rdoEasy)
        Me.grpDifficulty.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.grpDifficulty.Location = New System.Drawing.Point(6, 68)
        Me.grpDifficulty.Name = "grpDifficulty"
        Me.grpDifficulty.Size = New System.Drawing.Size(450, 59)
        Me.grpDifficulty.TabIndex = 2
        Me.grpDifficulty.TabStop = False
        Me.grpDifficulty.Text = "Moeilijkheidsgraad"
        '
        'pnlChangeOfUser
        '
        Me.pnlChangeOfUser.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnlChangeOfUser.BackColor = System.Drawing.Color.Transparent
        Me.pnlChangeOfUser.Controls.Add(Me.txtAge)
        Me.pnlChangeOfUser.Controls.Add(Me.lblAge)
        Me.pnlChangeOfUser.Controls.Add(Me.cmdNewUser)
        Me.pnlChangeOfUser.Controls.Add(Me.txtUsername)
        Me.pnlChangeOfUser.Controls.Add(Me.lblUsername)
        Me.pnlChangeOfUser.Location = New System.Drawing.Point(12, 459)
        Me.pnlChangeOfUser.Name = "pnlChangeOfUser"
        Me.pnlChangeOfUser.Size = New System.Drawing.Size(473, 31)
        Me.pnlChangeOfUser.TabIndex = 2
        '
        'txtAge
        '
        Me.txtAge.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.txtAge.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtAge.Location = New System.Drawing.Point(328, 6)
        Me.txtAge.MaxLength = 2
        Me.txtAge.Name = "txtAge"
        Me.txtAge.Size = New System.Drawing.Size(21, 20)
        Me.txtAge.TabIndex = 1
        '
        'txtUsername
        '
        Me.txtUsername.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtUsername.Location = New System.Drawing.Point(112, 6)
        Me.txtUsername.MaxLength = 20
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(135, 20)
        Me.txtUsername.TabIndex = 0
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(944, 502)
        Me.Controls.Add(Me.pnlChangeOfUser)
        Me.Controls.Add(Me.grpConfig)
        Me.Controls.Add(Me.cmdStart)
        Me.Controls.Add(Me.cmdDeleteEvaluation)
        Me.Controls.Add(Me.cmdShowEvaluations)
        Me.Controls.Add(Me.grpInfo)
        Me.Controls.Add(Me.grpCurrentVersion)
        Me.MinimumSize = New System.Drawing.Size(960, 540)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Rekenen"
        Me.grpInfo.ResumeLayout(False)
        Me.grpConfig.ResumeLayout(False)
        Me.tctrlType.ResumeLayout(False)
        Me.tpgQuotient.ResumeLayout(False)
        Me.tpgQuotient.PerformLayout()
        Me.grpAdvanced.ResumeLayout(False)
        Me.grpAdvanced.PerformLayout()
        Me.grpDifficulty.ResumeLayout(False)
        Me.grpDifficulty.PerformLayout()
        Me.pnlChangeOfUser.ResumeLayout(False)
        Me.pnlChangeOfUser.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout

End Sub
    Friend WithEvents cmdStart As System.Windows.Forms.Button
    Friend WithEvents grpCurrentVersion As System.Windows.Forms.GroupBox
    Friend WithEvents Hulp As System.Windows.Forms.ToolTip
    Friend WithEvents grpInfo As System.Windows.Forms.GroupBox
    Friend WithEvents cmdShowEvaluations As System.Windows.Forms.Button
    Friend WithEvents cmdDeleteEvaluation As System.Windows.Forms.Button
    Friend WithEvents tmrClicks As System.Windows.Forms.Timer
    Friend WithEvents rtbCorrection As System.Windows.Forms.RichTextBox
    Friend WithEvents webInfo As System.Windows.Forms.WebBrowser
    Friend WithEvents cmdCalcRules As System.Windows.Forms.Button
    Friend WithEvents tmrRefreshForm As System.Windows.Forms.Timer
    Friend WithEvents grpConfig As System.Windows.Forms.GroupBox
    Friend WithEvents pnlChangeOfUser As System.Windows.Forms.Panel
    Friend WithEvents cmdNewUser As System.Windows.Forms.Button
    Friend WithEvents txtUsername As System.Windows.Forms.TextBox
    Friend WithEvents lblUsername As System.Windows.Forms.Label
    Friend WithEvents txtAge As System.Windows.Forms.TextBox
    Friend WithEvents lblAge As System.Windows.Forms.Label
    Friend WithEvents tctrlType As System.Windows.Forms.TabControl
    Friend WithEvents tpgSum As System.Windows.Forms.TabPage
    Friend WithEvents tpgDifference As System.Windows.Forms.TabPage
    Friend WithEvents tpgProduct As System.Windows.Forms.TabPage
    Friend WithEvents tpgQuotient As System.Windows.Forms.TabPage
    Friend WithEvents cmdTestMode As System.Windows.Forms.Button
    Friend WithEvents grpDifficulty As System.Windows.Forms.GroupBox
    Friend WithEvents rdoHard As System.Windows.Forms.RadioButton
    Friend WithEvents rdoMedium As System.Windows.Forms.RadioButton
    Friend WithEvents rdoEasy As System.Windows.Forms.RadioButton
    Friend WithEvents grpAdvanced As System.Windows.Forms.GroupBox
    Friend WithEvents txtQuantityOfNums As System.Windows.Forms.TextBox
    Friend WithEvents lblQuantityOfNums As System.Windows.Forms.Label
    Friend WithEvents chkNoZero As System.Windows.Forms.CheckBox
    Friend WithEvents txtMinOne As System.Windows.Forms.TextBox
    Friend WithEvents lblMinOne As System.Windows.Forms.Label
    Friend WithEvents txtMaxOne As System.Windows.Forms.TextBox
    Friend WithEvents lblMaxOne As System.Windows.Forms.Label
    Friend WithEvents txtMinOutput As System.Windows.Forms.TextBox
    Friend WithEvents lblMinOutput As System.Windows.Forms.Label
    Friend WithEvents txtMaxOutput As System.Windows.Forms.TextBox
    Friend WithEvents lblMaxOutput As System.Windows.Forms.Label
    Friend WithEvents cmdClear As System.Windows.Forms.Button
    Friend WithEvents lblShowType As System.Windows.Forms.Label
    Friend WithEvents lblSymbol As System.Windows.Forms.Label
    Friend WithEvents lblSelected As System.Windows.Forms.Label

End Class
