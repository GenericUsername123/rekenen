﻿namespace Rekenen
{
  partial class OptionWindow
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.Exceptions = new System.Windows.Forms.GroupBox();
      this.Exclusion = new System.Windows.Forms.GroupBox();
      this.label8 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.EPcntInfluencedNums = new System.Windows.Forms.Label();
      this.EPcntInfluencedExercises = new System.Windows.Forms.Label();
      this.EPcntFactors = new System.Windows.Forms.TextBox();
      this.EPcntExercises = new System.Windows.Forms.TextBox();
      this.EAbsolute = new System.Windows.Forms.CheckBox();
      this.Inclusion = new System.Windows.Forms.GroupBox();
      this.label6 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.IPcntInfluencedNums = new System.Windows.Forms.Label();
      this.IPcntInfluencedExercises = new System.Windows.Forms.Label();
      this.IPcntFactors = new System.Windows.Forms.TextBox();
      this.IPcntExercises = new System.Windows.Forms.TextBox();
      this.IAbsolute = new System.Windows.Forms.CheckBox();
      this.AddRecord = new System.Windows.Forms.Button();
      this.ExceptionContainer = new System.Windows.Forms.Panel();
      this.Basic = new System.Windows.Forms.GroupBox();
      this.label4 = new System.Windows.Forms.Label();
      this.MinLimit = new System.Windows.Forms.TextBox();
      this.MaxLimit = new System.Windows.Forms.TextBox();
      this.RangeLimits = new System.Windows.Forms.Label();
      this.ExercisesLabel = new System.Windows.Forms.Label();
      this.Exercises = new System.Windows.Forms.TextBox();
      this.Precision = new System.Windows.Forms.TextBox();
      this.Factors = new System.Windows.Forms.Label();
      this.PrecisionLabel = new System.Windows.Forms.Label();
      this.MaxFactors = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.MinFactors = new System.Windows.Forms.TextBox();
      this.MinResult = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.MaxResult = new System.Windows.Forms.TextBox();
      this.RangeNums = new System.Windows.Forms.Label();
      this.RangeResults = new System.Windows.Forms.Label();
      this.MaxNums = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.MinNums = new System.Windows.Forms.TextBox();
      this.Symbols = new System.Windows.Forms.GroupBox();
      this.Division = new System.Windows.Forms.CheckBox();
      this.Product = new System.Windows.Forms.CheckBox();
      this.Difference = new System.Windows.Forms.CheckBox();
      this.Sum = new System.Windows.Forms.CheckBox();
      this.Advanced = new System.Windows.Forms.Button();
      this.Exceptions.SuspendLayout();
      this.Exclusion.SuspendLayout();
      this.Inclusion.SuspendLayout();
      this.Basic.SuspendLayout();
      this.Symbols.SuspendLayout();
      this.SuspendLayout();
      // 
      // Exceptions
      // 
      this.Exceptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.Exceptions.Controls.Add(this.Exclusion);
      this.Exceptions.Controls.Add(this.Inclusion);
      this.Exceptions.Controls.Add(this.AddRecord);
      this.Exceptions.Controls.Add(this.ExceptionContainer);
      this.Exceptions.Location = new System.Drawing.Point(265, 12);
      this.Exceptions.Name = "Exceptions";
      this.Exceptions.Size = new System.Drawing.Size(485, 219);
      this.Exceptions.TabIndex = 2;
      this.Exceptions.TabStop = false;
      // 
      // Exclusion
      // 
      this.Exclusion.Controls.Add(this.label8);
      this.Exclusion.Controls.Add(this.label7);
      this.Exclusion.Controls.Add(this.EPcntInfluencedNums);
      this.Exclusion.Controls.Add(this.EPcntInfluencedExercises);
      this.Exclusion.Controls.Add(this.EPcntFactors);
      this.Exclusion.Controls.Add(this.EPcntExercises);
      this.Exclusion.Controls.Add(this.EAbsolute);
      this.Exclusion.Location = new System.Drawing.Point(226, 119);
      this.Exclusion.Name = "Exclusion";
      this.Exclusion.Size = new System.Drawing.Size(253, 94);
      this.Exclusion.TabIndex = 3;
      this.Exclusion.TabStop = false;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(220, 71);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(15, 13);
      this.label8.TabIndex = 15;
      this.label8.Text = "%";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(220, 45);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(15, 13);
      this.label7.TabIndex = 14;
      this.label7.Text = "%";
      // 
      // EPcntInfluencedNums
      // 
      this.EPcntInfluencedNums.AutoSize = true;
      this.EPcntInfluencedNums.Location = new System.Drawing.Point(6, 71);
      this.EPcntInfluencedNums.Name = "EPcntInfluencedNums";
      this.EPcntInfluencedNums.Size = new System.Drawing.Size(0, 13);
      this.EPcntInfluencedNums.TabIndex = 13;
      // 
      // EPcntInfluencedExercises
      // 
      this.EPcntInfluencedExercises.AutoSize = true;
      this.EPcntInfluencedExercises.Location = new System.Drawing.Point(6, 45);
      this.EPcntInfluencedExercises.Name = "EPcntInfluencedExercises";
      this.EPcntInfluencedExercises.Size = new System.Drawing.Size(0, 13);
      this.EPcntInfluencedExercises.TabIndex = 12;
      // 
      // EPcntFactors
      // 
      this.EPcntFactors.Location = new System.Drawing.Point(190, 68);
      this.EPcntFactors.MaxLength = 3;
      this.EPcntFactors.Name = "EPcntFactors";
      this.EPcntFactors.Size = new System.Drawing.Size(24, 20);
      this.EPcntFactors.TabIndex = 2;
      // 
      // EPcntExercises
      // 
      this.EPcntExercises.Location = new System.Drawing.Point(190, 42);
      this.EPcntExercises.MaxLength = 3;
      this.EPcntExercises.Name = "EPcntExercises";
      this.EPcntExercises.Size = new System.Drawing.Size(24, 20);
      this.EPcntExercises.TabIndex = 1;
      // 
      // EAbsolute
      // 
      this.EAbsolute.AutoSize = true;
      this.EAbsolute.Location = new System.Drawing.Point(6, 19);
      this.EAbsolute.Name = "EAbsolute";
      this.EAbsolute.Size = new System.Drawing.Size(15, 14);
      this.EAbsolute.TabIndex = 0;
      this.EAbsolute.UseVisualStyleBackColor = true;
      // 
      // Inclusion
      // 
      this.Inclusion.Controls.Add(this.label6);
      this.Inclusion.Controls.Add(this.label5);
      this.Inclusion.Controls.Add(this.IPcntInfluencedNums);
      this.Inclusion.Controls.Add(this.IPcntInfluencedExercises);
      this.Inclusion.Controls.Add(this.IPcntFactors);
      this.Inclusion.Controls.Add(this.IPcntExercises);
      this.Inclusion.Controls.Add(this.IAbsolute);
      this.Inclusion.Location = new System.Drawing.Point(226, 19);
      this.Inclusion.Name = "Inclusion";
      this.Inclusion.Size = new System.Drawing.Size(253, 94);
      this.Inclusion.TabIndex = 2;
      this.Inclusion.TabStop = false;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(220, 71);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(15, 13);
      this.label6.TabIndex = 15;
      this.label6.Text = "%";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(220, 45);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(15, 13);
      this.label5.TabIndex = 14;
      this.label5.Text = "%";
      // 
      // IPcntInfluencedNums
      // 
      this.IPcntInfluencedNums.AutoSize = true;
      this.IPcntInfluencedNums.Location = new System.Drawing.Point(6, 71);
      this.IPcntInfluencedNums.Name = "IPcntInfluencedNums";
      this.IPcntInfluencedNums.Size = new System.Drawing.Size(0, 13);
      this.IPcntInfluencedNums.TabIndex = 13;
      // 
      // IPcntInfluencedExercises
      // 
      this.IPcntInfluencedExercises.AutoSize = true;
      this.IPcntInfluencedExercises.Location = new System.Drawing.Point(6, 45);
      this.IPcntInfluencedExercises.Name = "IPcntInfluencedExercises";
      this.IPcntInfluencedExercises.Size = new System.Drawing.Size(0, 13);
      this.IPcntInfluencedExercises.TabIndex = 12;
      // 
      // IPcntFactors
      // 
      this.IPcntFactors.Location = new System.Drawing.Point(190, 68);
      this.IPcntFactors.MaxLength = 3;
      this.IPcntFactors.Name = "IPcntFactors";
      this.IPcntFactors.Size = new System.Drawing.Size(24, 20);
      this.IPcntFactors.TabIndex = 2;
      // 
      // IPcntExercises
      // 
      this.IPcntExercises.Location = new System.Drawing.Point(190, 42);
      this.IPcntExercises.MaxLength = 3;
      this.IPcntExercises.Name = "IPcntExercises";
      this.IPcntExercises.Size = new System.Drawing.Size(24, 20);
      this.IPcntExercises.TabIndex = 1;
      // 
      // IAbsolute
      // 
      this.IAbsolute.AutoSize = true;
      this.IAbsolute.Location = new System.Drawing.Point(6, 19);
      this.IAbsolute.Name = "IAbsolute";
      this.IAbsolute.Size = new System.Drawing.Size(15, 14);
      this.IAbsolute.TabIndex = 0;
      this.IAbsolute.UseVisualStyleBackColor = true;
      // 
      // AddRecord
      // 
      this.AddRecord.Location = new System.Drawing.Point(6, 190);
      this.AddRecord.Name = "AddRecord";
      this.AddRecord.Size = new System.Drawing.Size(214, 23);
      this.AddRecord.TabIndex = 1;
      this.AddRecord.Text = "Voeg uitzondering toe";
      this.AddRecord.UseVisualStyleBackColor = true;
      // 
      // ExceptionContainer
      // 
      this.ExceptionContainer.AutoScroll = true;
      this.ExceptionContainer.Location = new System.Drawing.Point(6, 19);
      this.ExceptionContainer.Name = "ExceptionContainer";
      this.ExceptionContainer.Size = new System.Drawing.Size(214, 165);
      this.ExceptionContainer.TabIndex = 0;
      // 
      // Basic
      // 
      this.Basic.Controls.Add(this.label4);
      this.Basic.Controls.Add(this.MinLimit);
      this.Basic.Controls.Add(this.MaxLimit);
      this.Basic.Controls.Add(this.RangeLimits);
      this.Basic.Controls.Add(this.ExercisesLabel);
      this.Basic.Controls.Add(this.Exercises);
      this.Basic.Controls.Add(this.Precision);
      this.Basic.Controls.Add(this.Factors);
      this.Basic.Controls.Add(this.PrecisionLabel);
      this.Basic.Controls.Add(this.MaxFactors);
      this.Basic.Controls.Add(this.label3);
      this.Basic.Controls.Add(this.MinFactors);
      this.Basic.Controls.Add(this.MinResult);
      this.Basic.Controls.Add(this.label1);
      this.Basic.Controls.Add(this.MaxResult);
      this.Basic.Controls.Add(this.RangeNums);
      this.Basic.Controls.Add(this.RangeResults);
      this.Basic.Controls.Add(this.MaxNums);
      this.Basic.Controls.Add(this.label2);
      this.Basic.Controls.Add(this.MinNums);
      this.Basic.Location = new System.Drawing.Point(12, 12);
      this.Basic.Name = "Basic";
      this.Basic.Size = new System.Drawing.Size(247, 142);
      this.Basic.TabIndex = 0;
      this.Basic.TabStop = false;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(166, 120);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(19, 13);
      this.label4.TabIndex = 30;
      this.label4.Text = "tot";
      // 
      // MinLimit
      // 
      this.MinLimit.Location = new System.Drawing.Point(110, 117);
      this.MinLimit.MaxLength = 19;
      this.MinLimit.Name = "MinLimit";
      this.MinLimit.Size = new System.Drawing.Size(49, 20);
      this.MinLimit.TabIndex = 27;
      // 
      // MaxLimit
      // 
      this.MaxLimit.Location = new System.Drawing.Point(193, 117);
      this.MaxLimit.MaxLength = 19;
      this.MaxLimit.Name = "MaxLimit";
      this.MaxLimit.Size = new System.Drawing.Size(48, 20);
      this.MaxLimit.TabIndex = 28;
      // 
      // RangeLimits
      // 
      this.RangeLimits.AutoSize = true;
      this.RangeLimits.Location = new System.Drawing.Point(6, 120);
      this.RangeLimits.Name = "RangeLimits";
      this.RangeLimits.Size = new System.Drawing.Size(0, 13);
      this.RangeLimits.TabIndex = 29;
      // 
      // ExercisesLabel
      // 
      this.ExercisesLabel.AutoSize = true;
      this.ExercisesLabel.Location = new System.Drawing.Point(6, 16);
      this.ExercisesLabel.Name = "ExercisesLabel";
      this.ExercisesLabel.Size = new System.Drawing.Size(0, 13);
      this.ExercisesLabel.TabIndex = 0;
      // 
      // Exercises
      // 
      this.Exercises.Location = new System.Drawing.Point(74, 13);
      this.Exercises.MaxLength = 3;
      this.Exercises.Name = "Exercises";
      this.Exercises.Size = new System.Drawing.Size(24, 20);
      this.Exercises.TabIndex = 0;
      // 
      // Precision
      // 
      this.Precision.Location = new System.Drawing.Point(223, 13);
      this.Precision.MaxLength = 2;
      this.Precision.Name = "Precision";
      this.Precision.Size = new System.Drawing.Size(18, 20);
      this.Precision.TabIndex = 2;
      // 
      // Factors
      // 
      this.Factors.AutoSize = true;
      this.Factors.Location = new System.Drawing.Point(6, 42);
      this.Factors.Name = "Factors";
      this.Factors.Size = new System.Drawing.Size(0, 13);
      this.Factors.TabIndex = 14;
      // 
      // PrecisionLabel
      // 
      this.PrecisionLabel.AutoSize = true;
      this.PrecisionLabel.Location = new System.Drawing.Point(104, 16);
      this.PrecisionLabel.Name = "PrecisionLabel";
      this.PrecisionLabel.Size = new System.Drawing.Size(0, 13);
      this.PrecisionLabel.TabIndex = 26;
      // 
      // MaxFactors
      // 
      this.MaxFactors.Location = new System.Drawing.Point(190, 39);
      this.MaxFactors.MaxLength = 2;
      this.MaxFactors.Name = "MaxFactors";
      this.MaxFactors.Size = new System.Drawing.Size(18, 20);
      this.MaxFactors.TabIndex = 3;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(166, 94);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(19, 13);
      this.label3.TabIndex = 25;
      this.label3.Text = "tot";
      // 
      // MinFactors
      // 
      this.MinFactors.Location = new System.Drawing.Point(111, 39);
      this.MinFactors.MaxLength = 2;
      this.MinFactors.Name = "MinFactors";
      this.MinFactors.Size = new System.Drawing.Size(18, 20);
      this.MinFactors.TabIndex = 2;
      // 
      // MinResult
      // 
      this.MinResult.Location = new System.Drawing.Point(110, 91);
      this.MinResult.MaxLength = 19;
      this.MinResult.Name = "MinResult";
      this.MinResult.Size = new System.Drawing.Size(49, 20);
      this.MinResult.TabIndex = 6;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(151, 42);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(19, 13);
      this.label1.TabIndex = 17;
      this.label1.Text = "tot";
      // 
      // MaxResult
      // 
      this.MaxResult.Location = new System.Drawing.Point(193, 91);
      this.MaxResult.MaxLength = 19;
      this.MaxResult.Name = "MaxResult";
      this.MaxResult.Size = new System.Drawing.Size(48, 20);
      this.MaxResult.TabIndex = 7;
      // 
      // RangeNums
      // 
      this.RangeNums.AutoSize = true;
      this.RangeNums.Location = new System.Drawing.Point(6, 68);
      this.RangeNums.Name = "RangeNums";
      this.RangeNums.Size = new System.Drawing.Size(0, 13);
      this.RangeNums.TabIndex = 18;
      // 
      // RangeResults
      // 
      this.RangeResults.AutoSize = true;
      this.RangeResults.Location = new System.Drawing.Point(6, 94);
      this.RangeResults.Name = "RangeResults";
      this.RangeResults.Size = new System.Drawing.Size(0, 13);
      this.RangeResults.TabIndex = 22;
      // 
      // MaxNums
      // 
      this.MaxNums.Location = new System.Drawing.Point(193, 65);
      this.MaxNums.MaxLength = 19;
      this.MaxNums.Name = "MaxNums";
      this.MaxNums.Size = new System.Drawing.Size(48, 20);
      this.MaxNums.TabIndex = 5;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(166, 68);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(19, 13);
      this.label2.TabIndex = 21;
      this.label2.Text = "tot";
      // 
      // MinNums
      // 
      this.MinNums.Location = new System.Drawing.Point(111, 65);
      this.MinNums.MaxLength = 19;
      this.MinNums.Name = "MinNums";
      this.MinNums.Size = new System.Drawing.Size(48, 20);
      this.MinNums.TabIndex = 4;
      // 
      // Symbols
      // 
      this.Symbols.Controls.Add(this.Division);
      this.Symbols.Controls.Add(this.Product);
      this.Symbols.Controls.Add(this.Difference);
      this.Symbols.Controls.Add(this.Sum);
      this.Symbols.Location = new System.Drawing.Point(12, 160);
      this.Symbols.Name = "Symbols";
      this.Symbols.Size = new System.Drawing.Size(247, 42);
      this.Symbols.TabIndex = 1;
      this.Symbols.TabStop = false;
      // 
      // Division
      // 
      this.Division.AutoSize = true;
      this.Division.Location = new System.Drawing.Point(175, 19);
      this.Division.Name = "Division";
      this.Division.Size = new System.Drawing.Size(15, 14);
      this.Division.TabIndex = 3;
      this.Division.UseVisualStyleBackColor = true;
      // 
      // Product
      // 
      this.Product.AutoSize = true;
      this.Product.Location = new System.Drawing.Point(120, 19);
      this.Product.Name = "Product";
      this.Product.Size = new System.Drawing.Size(15, 14);
      this.Product.TabIndex = 2;
      this.Product.UseVisualStyleBackColor = true;
      // 
      // Difference
      // 
      this.Difference.AutoSize = true;
      this.Difference.Location = new System.Drawing.Point(71, 19);
      this.Difference.Name = "Difference";
      this.Difference.Size = new System.Drawing.Size(15, 14);
      this.Difference.TabIndex = 1;
      this.Difference.UseVisualStyleBackColor = true;
      // 
      // Sum
      // 
      this.Sum.AutoSize = true;
      this.Sum.Location = new System.Drawing.Point(19, 19);
      this.Sum.Name = "Sum";
      this.Sum.Size = new System.Drawing.Size(15, 14);
      this.Sum.TabIndex = 0;
      this.Sum.UseVisualStyleBackColor = true;
      // 
      // Advanced
      // 
      this.Advanced.Location = new System.Drawing.Point(12, 208);
      this.Advanced.Name = "Advanced";
      this.Advanced.Size = new System.Drawing.Size(247, 23);
      this.Advanced.TabIndex = 3;
      this.Advanced.Text = "Geavanceerd";
      this.Advanced.UseVisualStyleBackColor = true;
      this.Advanced.Click += new System.EventHandler(this.Advanced_Click);
      // 
      // OptionWindow
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(760, 241);
      this.Controls.Add(this.Advanced);
      this.Controls.Add(this.Basic);
      this.Controls.Add(this.Symbols);
      this.Controls.Add(this.Exceptions);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.Icon = global::Rekenen.Properties.Resources.DefaultIcon;
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(766, 267);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(766, 267);
      this.Name = "OptionWindow";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OptionWindow_FormClosed);
      this.Exceptions.ResumeLayout(false);
      this.Exclusion.ResumeLayout(false);
      this.Exclusion.PerformLayout();
      this.Inclusion.ResumeLayout(false);
      this.Inclusion.PerformLayout();
      this.Basic.ResumeLayout(false);
      this.Basic.PerformLayout();
      this.Symbols.ResumeLayout(false);
      this.Symbols.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion
    private System.Windows.Forms.GroupBox Exceptions;
    private System.Windows.Forms.GroupBox Basic;
    private System.Windows.Forms.Label ExercisesLabel;
    private System.Windows.Forms.TextBox Exercises;
    private System.Windows.Forms.TextBox Precision;
    private System.Windows.Forms.Label Factors;
    private System.Windows.Forms.Label PrecisionLabel;
    private System.Windows.Forms.TextBox MaxFactors;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox MinFactors;
    private System.Windows.Forms.TextBox MinResult;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox MaxResult;
    private System.Windows.Forms.Label RangeNums;
    private System.Windows.Forms.Label RangeResults;
    private System.Windows.Forms.TextBox MaxNums;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox MinNums;
    private System.Windows.Forms.GroupBox Symbols;
    private System.Windows.Forms.CheckBox Division;
    private System.Windows.Forms.CheckBox Product;
    private System.Windows.Forms.CheckBox Difference;
    private System.Windows.Forms.CheckBox Sum;
    private System.Windows.Forms.Button Advanced;
    private System.Windows.Forms.Panel ExceptionContainer;
    private System.Windows.Forms.Button AddRecord;
    private System.Windows.Forms.GroupBox Inclusion;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label IPcntInfluencedNums;
    private System.Windows.Forms.Label IPcntInfluencedExercises;
    private System.Windows.Forms.TextBox IPcntFactors;
    private System.Windows.Forms.TextBox IPcntExercises;
    private System.Windows.Forms.CheckBox IAbsolute;
    private System.Windows.Forms.GroupBox Exclusion;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label EPcntInfluencedNums;
    private System.Windows.Forms.Label EPcntInfluencedExercises;
    private System.Windows.Forms.TextBox EPcntFactors;
    private System.Windows.Forms.TextBox EPcntExercises;
    private System.Windows.Forms.CheckBox EAbsolute;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox MinLimit;
    private System.Windows.Forms.TextBox MaxLimit;
    private System.Windows.Forms.Label RangeLimits;
  }
}