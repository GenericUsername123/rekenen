﻿using System.Collections.Generic;
using System.Globalization;

namespace Session
{
  public class NumericalOccurrences : List<double>
  {
    public readonly Occurrences Occurrences;

    public NumericalOccurrences(SessionSetup sessionSetup)
    {
      Occurrences = new Occurrences(sessionSetup, FactorRange.Limit);
    }

    public bool Absolute { get; set; } = true;

    public bool OnlyZero
    {
      get
      {
        var temp = new List<double>(this);
        if (Count == 0 || !temp.Remove(0)) return false;
        return temp.Count == 0;
      }
    }

    public new double this[int index]
    {
      get { return base[index]; }
      // ReSharper disable once UnusedMember.Global
      set { Add(value); }
    }

    public new void Add(double item)
    {
      if (double.IsNaN(item)) return;
      Remove(item);
      base.Add(item);
    }

    public new void AddRange(IEnumerable<double> collection)
    {
      if (collection == null) return;
      var enumerator = collection.GetEnumerator();
      while (enumerator.MoveNext()) Add(enumerator.Current);
    }

    public IList<double> Get(int precision)
    {
      var temp = this;
      var i = 0;
      while (i < temp.Count)
        if (HasPrecision(temp[i]) > precision) temp.RemoveAt(i);
        else i++;
      return temp;
    }

    public override string ToString()
    {
      if (Count == 0) return "{}";
      var collection = "";
      collection += base[0];
      for (var i = 1; i < Count; i++) collection += "," + base[i];
      return "{" + collection + "}";
    }

    public static int HasPrecision(double num)
    {
      if (num == 0) return 0;
      return HasPrecision((decimal)num);
    }

    public static int HasPrecision(decimal num)
    {
      if (num == 0) return 0;
      var number = num.ToString(CultureInfo.InvariantCulture);
      var commaIndex = number.IndexOf('.');
      if (commaIndex != -1) return number.Length - commaIndex - 1;
      var i = number.Length - 1;
      while (i > 0 && number[i] == '0') i--;
      return i + 1 - number.Length;
    }
  }
}