﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSession
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdOK = New System.Windows.Forms.Button()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.cmdClear = New System.Windows.Forms.Button()
        Me.prgScore = New System.Windows.Forms.ProgressBar()
        Me.prgQuantity = New System.Windows.Forms.ProgressBar()
        Me.lblEvaluation = New System.Windows.Forms.Label()
        Me.lblQuantity = New System.Windows.Forms.Label()
        Me.txtAnswer = New System.Windows.Forms.TextBox()
        Me.lblAnswer = New System.Windows.Forms.Label()
        Me.lblQuestion = New System.Windows.Forms.Label()
        Me.grpInputBox = New System.Windows.Forms.GroupBox()
        Me.grpStatus = New System.Windows.Forms.GroupBox()
        Me.grpInputBox.SuspendLayout()
        Me.grpStatus.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdOK.Location = New System.Drawing.Point(266, 64)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(65, 23)
        Me.cmdOK.TabIndex = 0
        Me.cmdOK.Text = "OK"
        Me.cmdOK.UseVisualStyleBackColor = True
        '
        'cmdCancel
        '
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdCancel.Location = New System.Drawing.Point(195, 64)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(65, 23)
        Me.cmdCancel.TabIndex = 1
        Me.cmdCancel.Text = "Annuleren"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'cmdClear
        '
        Me.cmdClear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdClear.Location = New System.Drawing.Point(124, 64)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.Size = New System.Drawing.Size(65, 23)
        Me.cmdClear.TabIndex = 2
        Me.cmdClear.Text = "Wissen"
        Me.cmdClear.UseVisualStyleBackColor = True
        '
        'prgScore
        '
        Me.prgScore.Location = New System.Drawing.Point(6, 32)
        Me.prgScore.Name = "prgScore"
        Me.prgScore.Size = New System.Drawing.Size(325, 10)
        Me.prgScore.TabIndex = 3
        '
        'prgQuantity
        '
        Me.prgQuantity.Location = New System.Drawing.Point(6, 61)
        Me.prgQuantity.Name = "prgQuantity"
        Me.prgQuantity.Size = New System.Drawing.Size(325, 10)
        Me.prgQuantity.TabIndex = 4
        '
        'lblEvaluation
        '
        Me.lblEvaluation.AutoSize = True
        Me.lblEvaluation.Location = New System.Drawing.Point(6, 16)
        Me.lblEvaluation.Name = "lblEvaluation"
        Me.lblEvaluation.Size = New System.Drawing.Size(44, 13)
        Me.lblEvaluation.TabIndex = 5
        Me.lblEvaluation.Text = "Punten:"
        '
        'lblQuantity
        '
        Me.lblQuantity.AutoSize = True
        Me.lblQuantity.Location = New System.Drawing.Point(6, 45)
        Me.lblQuantity.Name = "lblQuantity"
        Me.lblQuantity.Size = New System.Drawing.Size(48, 13)
        Me.lblQuantity.TabIndex = 6
        Me.lblQuantity.Text = "Voltooid:"
        '
        'txtAnswer
        '
        Me.txtAnswer.Location = New System.Drawing.Point(67, 66)
        Me.txtAnswer.MaxLength = 4
        Me.txtAnswer.Name = "txtAnswer"
        Me.txtAnswer.Size = New System.Drawing.Size(51, 20)
        Me.txtAnswer.TabIndex = 0
        Me.txtAnswer.Text = "Getal..."
        '
        'lblAnswer
        '
        Me.lblAnswer.AutoSize = True
        Me.lblAnswer.Location = New System.Drawing.Point(6, 69)
        Me.lblAnswer.Name = "lblAnswer"
        Me.lblAnswer.Size = New System.Drawing.Size(55, 13)
        Me.lblAnswer.TabIndex = 8
        Me.lblAnswer.Text = "Antwoord:"
        '
        'lblQuestion
        '
        Me.lblQuestion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.lblQuestion.Location = New System.Drawing.Point(9, 26)
        Me.lblQuestion.Name = "lblQuestion"
        Me.lblQuestion.Size = New System.Drawing.Size(318, 24)
        Me.lblQuestion.TabIndex = 9
        Me.lblQuestion.Text = "Wat is: 100 x 100"
        Me.lblQuestion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'grpInputBox
        '
        Me.grpInputBox.Controls.Add(Me.lblQuestion)
        Me.grpInputBox.Controls.Add(Me.lblAnswer)
        Me.grpInputBox.Controls.Add(Me.cmdCancel)
        Me.grpInputBox.Controls.Add(Me.txtAnswer)
        Me.grpInputBox.Controls.Add(Me.cmdOK)
        Me.grpInputBox.Controls.Add(Me.cmdClear)
        Me.grpInputBox.Location = New System.Drawing.Point(12, 12)
        Me.grpInputBox.Name = "grpInputBox"
        Me.grpInputBox.Size = New System.Drawing.Size(337, 100)
        Me.grpInputBox.TabIndex = 10
        Me.grpInputBox.TabStop = False
        Me.grpInputBox.Text = "Oefening"
        '
        'grpStatus
        '
        Me.grpStatus.Controls.Add(Me.prgScore)
        Me.grpStatus.Controls.Add(Me.prgQuantity)
        Me.grpStatus.Controls.Add(Me.lblQuantity)
        Me.grpStatus.Controls.Add(Me.lblEvaluation)
        Me.grpStatus.Location = New System.Drawing.Point(12, 118)
        Me.grpStatus.Name = "grpStatus"
        Me.grpStatus.Size = New System.Drawing.Size(337, 81)
        Me.grpStatus.TabIndex = 11
        Me.grpStatus.TabStop = False
        Me.grpStatus.Text = "Info"
        '
        'frmSession
        '
        Me.AccessibleRole = System.Windows.Forms.AccessibleRole.Dialog
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(361, 206)
        Me.ControlBox = False
        Me.Controls.Add(Me.grpStatus)
        Me.Controls.Add(Me.grpInputBox)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(377, 244)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(377, 244)
        Me.Name = "frmSession"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Oefeningen"
        Me.grpInputBox.ResumeLayout(False)
        Me.grpInputBox.PerformLayout()
        Me.grpStatus.ResumeLayout(False)
        Me.grpStatus.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdClear As System.Windows.Forms.Button
    Friend WithEvents prgScore As System.Windows.Forms.ProgressBar
    Friend WithEvents prgQuantity As System.Windows.Forms.ProgressBar
    Friend WithEvents lblEvaluation As System.Windows.Forms.Label
    Friend WithEvents lblQuantity As System.Windows.Forms.Label
    Friend WithEvents txtAnswer As System.Windows.Forms.TextBox
    Friend WithEvents lblAnswer As System.Windows.Forms.Label
    Friend WithEvents lblQuestion As System.Windows.Forms.Label
    Friend WithEvents grpInputBox As System.Windows.Forms.GroupBox
    Friend WithEvents grpStatus As System.Windows.Forms.GroupBox
End Class
