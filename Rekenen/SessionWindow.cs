﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Windows.Forms;
using GuiTools;
using Rekenen.Properties;
using Session;

namespace Rekenen
{
  public partial class SessionWindow : Form
  {
    private readonly float _exercises;
    private readonly IEnumerator _output;
    private readonly Stopwatch _sessionTime = new Stopwatch();
    private byte _correct;

    public SessionWindow(IEnumerator output, byte exercises)
    {
      _output = output;
      _exercises = exercises;
      InitializeComponent();
      CenterToParent();
      AttachEvents();
    }

    public long TimeSpent => _sessionTime.ElapsedMilliseconds;

    public float Accuracy
    {
      get
      {
        var value = _correct / _exercises;
        return value > 1 ? 1 : value;
      }
    }

    private void AttachEvents()
    {
      SizeChanged += (sender, e) => UpdateLabels();
      Closing += (sender, e) => _sessionTime.Stop();
      Cancel.Click += (sender, e) => Close();
      Answer.KeyDown += Answer_KeyDown;
      KeepToFormat.Initialize(Answer, new KeepToFormat.Number
      {
        LowerBound = -NumberRange.Limit,
        UpperBound = NumberRange.Limit,
        Precision = 5
      });
    }

    private void SessionWindow_Shown(object sender, EventArgs e)
    {
      UpdateLabels();
      Next.PerformClick();
      _sessionTime.Start();
    }

    private void Answer_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Alt && e.KeyCode == Keys.F4) Cancel.PerformClick();
      else if (e.KeyCode == Keys.Return)
      {
        Check.PerformClick();
        Next.Select();
      }
    }

    private void UpdateLabels()
    {
      Exercise.Height = Check.Top;
      Expected.Top = Check.Bottom;
      Expected.Height = Cancel.Top - Check.Bottom;
    }

    private void Next_Click(object sender, EventArgs e)
    {
      if (Exercise.Text != "") Check.PerformClick();
      Check.Enabled = true;
      Expected.Text = "";
      Answer.Select();
      Answer.SelectionStart = 0;
      Answer.SelectionLength = Answer.TextLength;
      if (_output.MoveNext())
      {
        var exercise = (string[])_output.Current;
        Exercise.Text = Resources.SessionWindow_Exercise + exercise[0];
      }
      else
      {
        _sessionTime.Stop();
        var values = Resources.RecordsWindow_Values.Split(';');
        MessageBox.Show(
          "" + values[0] + PersonalRecords.PersonalRecord.CalculateScore(Accuracy, TimeSpent) + '\n' + values[1] +
          Helper.MsToString(TimeSpent) + '\n' + values[2] + Accuracy.ToString("P"), Resources.SessionWindow_Result);
        Cancel.PerformClick();
      }
    }

    private void Check_Click(object sender, EventArgs e)
    {
      Check.Enabled = false;
      var exercise = (string[])_output.Current;
      if (exercise[1] == Answer.Text)
      {
        _correct++;
        Expected.Text = Resources.SessionWindow_Correct;
      }
      else Expected.Text = Resources.SessionWindow_Incorrect + exercise[0] + @"= " + exercise[1];
    }
  }
}