﻿Public Class frmCalcRules
    Private Sub frmCalcRules_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Icon = My.Resources.DefaultIcon
            Me.Size = New Size(Math.Round(SystemInformation.WorkingArea.Height * 0.707) + 20, SystemInformation.WorkingArea.Height)
            Me.MaximumSize = Me.Size
            Me.MinimumSize = Me.Size
            Me.CenterToScreen()
            webDisplayCalcRules.Navigate(clsAppInfo.AppDocuments & "Documenten\Rekenregels.html")
            webDisplayCalcRules.Select()
        Catch ex As Exception
            My.Settings.ErrorReport += clsAppInfo.ManageException(ex, "Er is een fout opgetreden tijdens het laden van de rekenregels.")
            frmMain.CreateLogs()
        End Try
    End Sub

    Private Sub frmCalcRules_FormClosed(sender As Object, e As EventArgs) Handles Me.FormClosed
        frmMain.cmdCalcRules.Enabled = True
    End Sub
End Class