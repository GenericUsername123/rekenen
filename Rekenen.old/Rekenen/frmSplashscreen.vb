﻿Public Class frmSplashscreen

    Private Sub frmSplashscreen_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        My.Computer.Audio.Play(My.Resources.SoundActivated, AudioPlayMode.Background)
    End Sub

    Private Sub frmSplashscreen_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = Keys.Enter Then
            End
        End If
    End Sub
    Private Sub frmSplashscreen_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = My.Resources.DefaultIcon
        lblPrgrmName.Text = My.Application.Info.ProductName
        If Mid(My.Application.Info.Version.ToString, 1, 1) = "0" Then
            lblBuild.Text = "Versie: " & My.Application.Info.Version.ToString & " (Bèta)"
        Else
            lblBuild.Text = "Versie: " & My.Application.Info.Version.ToString
        End If
        lblBuild.ForeColor = Color.FromArgb(125, 25, 30)
        lblPrgrmName.ForeColor = Color.FromArgb(125, 25, 30)
    End Sub
End Class