﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using GuiTools;
using Rekenen.Properties;
using Session;

namespace Rekenen
{
  public partial class OptionWindow : Form
  {
    private readonly List<Control[]> _exceptions = new List<Control[]>();
    private readonly List<double> _lastValues = new List<double>();
    private readonly bool _readOnly;
    private readonly Session.Session _session;

    public OptionWindow(Session.Session session)
    {
      _readOnly = false;
      _session = session;
      Init(Resources.OptionWindow_Settings);
    }

    public OptionWindow(Session.Session session, string key)
    {
      _readOnly = true;
      _session = session;
      Init(Helper.Translate(key));
    }

    private void Init(string title)
    {
      InitializeComponent();
      AdaptToCulture();
      CenterToParent();
      ReadFromSession();
      AttachEvents();
      if (_readOnly) MakeReadOnly(Controls);
      Text = title;
    }

    private void MakeReadOnly(IEnumerable controls)
    {
      foreach (Control control in controls)
      {
        control.TabStop = false;
        if (control is GroupBox)
        {
          MakeReadOnly(control.Controls);
          continue;
        }
        var txt = control as TextBox;
        if (txt != null)
        {
          txt.ReadOnly = true;
          continue;
        }
        var chck = control as CheckBox;
        if (chck != null)
        {
          chck.CheckedChanged += (sender, e) => ReadFromSession();
          continue;
        }
        var btn = control as Button;
        if (btn != null && btn.Name != Advanced.Name) btn.Enabled = false;
      }
    }

    private void AdaptToCulture()
    {
      SuspendLayout();
      var groups = Resources.SessionProperties.Split('\n');
      var groupBoxes = new[] { Basic, Symbols, Exceptions, Inclusion, Exclusion };
      var mutables = new Control[]
      {
        ExercisesLabel, PrecisionLabel, Factors, RangeNums, RangeResults, RangeLimits, Sum, Difference, Product,
        Division, IAbsolute, IPcntInfluencedExercises, IPcntInfluencedNums, EAbsolute, EPcntInfluencedExercises,
        EPcntInfluencedNums
      };
      int offset = 0, g = 0;
      for (var i = 0; i + g < groupBoxes.Length; i++)
      {
        var temp = groups[i].Split('{', '}', '=').ToList();
        if (temp.Count > 5)
        {
          groupBoxes[i + g++].Text = temp[0];
          temp.RemoveRange(0, 2);
        }
        groupBoxes[i + g].Text = temp[0];
        AlterGroupBox(ref offset, groupBoxes[i + g], mutables, temp[2].Split(','));
      }
      ResumeLayout();
    }

    private void AlterGroupBox(ref int offset, Control groupBox, IList<Control> mutables, IList<string> textNodes)
    {
      foreach (Control control in groupBox.Controls)
      {
        for (var j = offset; j < offset + textNodes.Count && j < mutables.Count; j++)
        {
          if (mutables[j].Name != control.Name) continue;
          mutables[j].Text = textNodes[j - offset];
          break;
        }
      }
      offset += textNodes.Count;
    }

    #region Events

    private void ObserveInteraction(IEnumerable controls)
    {
      foreach (Control control in controls)
      {
        control.KeyDown += (sender, e) =>
        {
          if (e.KeyCode == Keys.Escape) Close();
        };
        if (control is GroupBox)
        {
          ObserveInteraction(control.Controls);
          continue;
        }
        if (control is TextBox)
        {
          control.KeyDown += ListenForClosure;
          control.TextChanged += WriteToSession;
        }
        else
        {
          var checkBox = control as CheckBox;
          if (checkBox != null) checkBox.CheckStateChanged += WriteToSession;
        }
      }
    }

    private void ListenForClosure(object sender, KeyEventArgs e)
    {
      if (e.Alt && e.KeyCode == Keys.F4) Close();
    }

    private void AttachEvents()
    {
      ObserveInteraction(Controls);
      AddRecord.Click += (sender, e) => AddException();
      KeepToFormat.Initialize(Exercises, new KeepToFormat.Number
      {
        LowerBound = 1,
        UpperBound = 255,
        Precision = 0
      });
      KeepToFormat.Initialize(Precision, new KeepToFormat.Number
      {
        LowerBound = -5,
        UpperBound = 5,
        Precision = 0
      });
      var factorFormat = new KeepToFormat.Number
      {
        LowerBound = 2,
        UpperBound = 99,
        Precision = 0
      };
      var pcntFormat = new KeepToFormat.Number
      {
        LowerBound = 0,
        UpperBound = 100,
        Precision = 0
      };
      var numberFormat = new KeepToFormat.Number
      {
        LowerBound = -NumberRange.Limit,
        UpperBound = NumberRange.Limit,
        Precision = 5
      };
      KeepToFormat.Initialize(MinFactors, factorFormat);
      KeepToFormat.Initialize(MaxFactors, factorFormat);
      KeepToFormat.Initialize(IPcntExercises, pcntFormat);
      KeepToFormat.Initialize(IPcntFactors, pcntFormat);
      KeepToFormat.Initialize(EPcntExercises, pcntFormat);
      KeepToFormat.Initialize(EPcntFactors, pcntFormat);
      KeepToFormat.Initialize(MinNums, numberFormat);
      KeepToFormat.Initialize(MaxNums, numberFormat);
      KeepToFormat.Initialize(MinResult, numberFormat);
      KeepToFormat.Initialize(MaxResult, numberFormat);
      KeepToFormat.Initialize(MinLimit, numberFormat);
      KeepToFormat.Initialize(MaxLimit, numberFormat);
    }

    private void OptionWindow_FormClosed(object sender, FormClosedEventArgs e)
    {
      _session.Inclusions.Occurrences.Initialize();
      _session.Exclusions.Occurrences.Initialize();
    }

    private void Advanced_Click(object sender, EventArgs e)
    {
      Advanced.Enabled = false;
      var advancedWindow = new AdvancedWindow(_session, _readOnly);
      advancedWindow.Closed += (o, args) =>
      {
        Advanced.Enabled = true;
        Left += advancedWindow.Width / 2;
      };
      advancedWindow.Updated += (o, args) => ReadFromSession();
      advancedWindow.Show(this);
    }

    #endregion

    #region Exceptions

    private void AddException(double value = 0, bool isInclusion = true, bool readOnly = false)
    {
      var index = _exceptions.Count != 0 ? _exceptions[_exceptions.Count - 1][2].TabIndex - 2 : 0;
      var height = (_exceptions.Count != 0 ? _exceptions[_exceptions.Count - 1][0].Bottom : 0) + 3;
      var tabIndex = index + 1;
      var type = new ComboBox
      {
        Parent = ExceptionContainer,
        DropDownStyle = ComboBoxStyle.DropDownList,
        FormattingEnabled = true,
        Items =
        {
          Resources.OptionWindow_Inclusion,
          Resources.OptionWindow_Exclusion
        },
        Location = new Point(3, height),
        MaxDropDownItems = 2,
        Name = "Type" + index,
        Size = new Size(66, 21),
        SelectedIndex = isInclusion ? 0 : 1,
        TabIndex = tabIndex++
      };
      var exception = new TextBox
      {
        Parent = ExceptionContainer,
        Location = new Point(75, height),
        MaxLength = 19,
        Name = "Exception" + index,
        Size = new Size(88, 20),
        TabIndex = tabIndex++,
        Text = value + "",
        ReadOnly = _readOnly
      };
      var removeRecord = new Button
      {
        Parent = ExceptionContainer,
        Location = new Point(169, height),
        Name = "Record" + index,
        Size = new Size(27, 20),
        TabIndex = tabIndex,
        Text = '-' + "",
        UseVisualStyleBackColor = true,
        Enabled = !_readOnly
      };
      type.SelectedIndexChanged += SwitchException;
      KeepToFormat.Initialize(exception, new KeepToFormat.Number
      {
        LowerBound = -NumberRange.Limit,
        UpperBound = NumberRange.Limit,
        Precision = 5
      });
      if (_readOnly) type.Items.RemoveAt(isInclusion ? 1 : 0);
      exception.KeyDown += ListenForClosure;
      exception.TextChanged += WriteToSession;
      removeRecord.Click += RemoveException;
      _lastValues.Add(value);
      _exceptions.Add(new Control[] { type, exception, removeRecord });
      if (readOnly) return;
      if (isInclusion) _session.Inclusions.Add(value);
      else _session.Exclusions.Add(value);
    }

    private void RemoveException(object sender, EventArgs e)
    {
      var index = int.Parse(((Button)sender).Name.Substring("Record".Length));
      var exception = double.Parse(_exceptions[index][1].Text);
      if (((ComboBox)_exceptions[index][0]).SelectedIndex == 0) _session.Inclusions.Remove(exception);
      else _session.Exclusions.Remove(exception);
      _lastValues[index] = double.NaN;
      foreach (var control in _exceptions[index]) control.Dispose();
      for (var i = index; i < _exceptions.Count; i++)
      {
        _exceptions[i][0].Top -= _exceptions[i][0].Height + 3;
        _exceptions[i][1].Top -= _exceptions[i][0].Height + 3;
        _exceptions[i][2].Top -= _exceptions[i][0].Height + 3;
      }
    }

    private void SwitchException(object sender, EventArgs e)
    {
      var type = (ComboBox)sender;
      var index = int.Parse(type.Name.Substring("Type".Length));
      var val = double.Parse(_exceptions[index][1].Text);
      if (type.SelectedIndex == 0)
      {
        _session.Inclusions.Add(val);
        var i = 0;
        for (; i < _lastValues.Count; i++)
        {
          if (i == index) continue;
          if (_lastValues[i] == val && ((ComboBox)_exceptions[i][0]).SelectedIndex == 1) break;
        }
        if (i == _lastValues.Count) _session.Exclusions.Remove(val);
      }
      else
      {
        _session.Exclusions.Add(val);
        var i = 0;
        for (; i < _lastValues.Count; i++)
        {
          if (i == index) continue;
          if (_lastValues[i] == val && ((ComboBox)_exceptions[i][0]).SelectedIndex == 0) break;
        }
        if (i == _lastValues.Count) _session.Inclusions.Remove(val);
      }
    }

    #endregion

    #region Read/Write

    private void ReadFromSession()
    {
      AlterTextBox(_session.Exercises, ref Exercises);
      AlterTextBox(_session.Precision, ref Precision);
      AlterTextBox(_session.FactorRange.LowerBound, ref MinFactors);
      AlterTextBox(_session.Precision, ref Precision);
      AlterTextBox(_session.FactorRange.LowerBound, ref MinFactors);
      AlterTextBox(_session.FactorRange.UpperBound, ref MaxFactors);
      AlterTextBox(_session.NumberRange.LowerNumber, ref MinNums);
      AlterTextBox(_session.NumberRange.UpperNumber, ref MaxNums);
      AlterTextBox(_session.NumberRange.LowerResult, ref MinResult);
      AlterTextBox(_session.NumberRange.UpperResult, ref MaxResult);
      AlterTextBox(_session.NumberRange.LowerLimit, ref MinLimit);
      AlterTextBox(_session.NumberRange.UpperLimit, ref MaxLimit);
      Sum.Checked = _session.Symbols.Contains(Session.Symbols.Type.Sum);
      Difference.Checked = _session.Symbols.Contains(Session.Symbols.Type.Difference);
      Product.Checked = _session.Symbols.Contains(Session.Symbols.Type.Product);
      Division.Checked = _session.Symbols.Contains(Session.Symbols.Type.Division);
      IAbsolute.Checked = _session.Inclusions.Absolute;
      EAbsolute.Checked = _session.Exclusions.Absolute;
      AlterTextBox((int)(_session.Inclusions.Occurrences.PcntExercises * 100), ref IPcntExercises);
      AlterTextBox((int)(_session.Inclusions.Occurrences.PcntFactors * 100), ref IPcntFactors);
      AlterTextBox((int)(_session.Exclusions.Occurrences.PcntExercises * 100), ref EPcntExercises);
      AlterTextBox((int)(_session.Exclusions.Occurrences.PcntFactors * 100), ref EPcntFactors);
      if (_exceptions.Count != 0) return;
      foreach (var inclusion in _session.Inclusions) AddException(inclusion, true, true);
      foreach (var exclusion in _session.Exclusions) AddException(exclusion, false, true);
    }

    private void AlterTextBox(double value, ref TextBox textBox)
    {
      if (textBox.Text != "") if (double.Parse(textBox.Text) == value) return;
      textBox.Text = value.ToString("0." + new string('#', 339));
    }

    private void WriteToSession(object sender, EventArgs e)
    {
      RetrieveFromCheckBoxes(sender as CheckBox);
      RetrieveFromTextBoxes(sender as TextBox);
    }

    private void RetrieveFromCheckBoxes(CheckBox checkBox)
    {
      if (checkBox == null) return;
      var name = checkBox.Name;
      if (name == IAbsolute.Name) _session.Inclusions.Absolute = IAbsolute.Checked;
      else if (name == EAbsolute.Name) _session.Exclusions.Absolute = EAbsolute.Checked;
      else if (checkBox.Checked)
      {
        if (name == Sum.Name) _session.Symbols.Add(Session.Symbols.Type.Sum);
        else if (name == Difference.Name) _session.Symbols.Add(Session.Symbols.Type.Difference);
        else if (name == Product.Name) _session.Symbols.Add(Session.Symbols.Type.Product);
        else if (name == Division.Name) _session.Symbols.Add(Session.Symbols.Type.Division);
      }
      else
      {
        if (name == Sum.Name) _session.Symbols.Remove(Session.Symbols.Type.Sum);
        else if (name == Difference.Name) _session.Symbols.Remove(Session.Symbols.Type.Difference);
        else if (name == Product.Name) _session.Symbols.Remove(Session.Symbols.Type.Product);
        else if (name == Division.Name) _session.Symbols.Remove(Session.Symbols.Type.Division);
      }
    }

    private void RetrieveFromTextBoxes(Control textBox)
    {
      if (textBox == null) return;
      var name = textBox.Name;
      if (textBox.Text == '-' + "" ||
          textBox.Text == Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator) return;
      if (name == Exercises.Name) _session.Exercises = byte.Parse(Exercises.Text);
      else if (name == Precision.Name) _session.Precision = sbyte.Parse(Precision.Text);
      else if (name == MinFactors.Name) _session.FactorRange.LowerBound = byte.Parse(MinFactors.Text);
      else if (name == MaxFactors.Name) _session.FactorRange.UpperBound = byte.Parse(MaxFactors.Text);
      else if (name == MinNums.Name) _session.NumberRange.LowerNumber = double.Parse(MinNums.Text);
      else if (name == MaxNums.Name) _session.NumberRange.UpperNumber = double.Parse(MaxNums.Text);
      else if (name == MinResult.Name) _session.NumberRange.LowerResult = double.Parse(MinResult.Text);
      else if (name == MaxResult.Name) _session.NumberRange.UpperResult = double.Parse(MaxResult.Text);
      else if (name == MinLimit.Name) _session.NumberRange.LowerLimit = double.Parse(MinLimit.Text);
      else if (name == MaxLimit.Name) _session.NumberRange.UpperLimit = double.Parse(MaxLimit.Text);
      else if (name == IPcntExercises.Name)
        _session.Inclusions.Occurrences.PcntExercises = int.Parse(IPcntExercises.Text) / 100d;
      else if (name == IPcntFactors.Name)
        _session.Inclusions.Occurrences.PcntFactors = int.Parse(IPcntFactors.Text) / 100d;
      else if (name == EPcntExercises.Name)
        _session.Exclusions.Occurrences.PcntExercises = int.Parse(EPcntExercises.Text) / 100d;
      else if (name == EPcntFactors.Name)
        _session.Exclusions.Occurrences.PcntFactors = int.Parse(EPcntFactors.Text) / 100d;
      else if (name.Contains("Exception"))
      {
        var index = int.Parse(name.Substring("Exception".Length));
        var value = double.Parse(_exceptions[index][1].Text);
        if (((ComboBox)_exceptions[index][0]).SelectedIndex == 0)
        {
          _session.Inclusions.Remove(_lastValues[index]);
          _session.Inclusions.Add(value);
        }
        else
        {
          _session.Exclusions.Remove(_lastValues[index]);
          _session.Exclusions.Add(value);
        }
        _lastValues[index] = value;
      }
      ReadFromSession();
    }

    #endregion
  }
}