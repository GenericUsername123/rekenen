using System.Collections.Generic;
using System.Linq;

namespace Session
{
  public class Symbols : List<Symbols.Type>
  {
    public enum Type
    {
      Unknown = 0,
      Sum = 1,
      Difference = 3,
      Product = 2,
      Division = 4,
      Exponent = 10
    }

    // ReSharper disable once UnusedMember.Global
    public new Type this[int index]
    {
      get { return base[index]; }
      set { Add(value); }
    }

    public new void Add(Type item)
    {
      if ((int)item % 10 == 0) return;
      Remove(item);
      base.Add(item);
      Sort();
    }

    public new void AddRange(IEnumerable<Type> collection)
    {
      if (collection == null) return;
      var enumerator = collection.GetEnumerator();
      while (enumerator.MoveNext()) Add(enumerator.Current);
    }

    public static string TypeToString(Type type)
    {
      switch (type)
      {
        case Type.Unknown:
          return "";
        case Type.Sum:
          return " + ";
        case Type.Difference:
          return " - ";
        case Type.Product:
          return " x ";
        case Type.Division:
          return " : ";
        case Type.Exponent:
          return "^";
        default:
          return null;
      }
    }

    public override string ToString()
    {
      return this.Aggregate("", (current, symbol) => current + (int)symbol);
    }
  }
}