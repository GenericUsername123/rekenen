﻿using System;
using System.Runtime.CompilerServices;

namespace Session
{
  public class NumberRange
  {
    public const double Limit = 7.9228162514264343e19;
    private readonly double[] _rangeNums = new double[6];
    private readonly SessionSetup _sessionSetup;

    public NumberRange(SessionSetup sessionSetup)
    {
      _sessionSetup = sessionSetup;
      SetBoundaries(100);
      UpperNumber = 10;
    }

    public double LowerNumber
    {
      [MethodImpl(MethodImplOptions.NoInlining)]
      get { return _rangeNums[0]; }
      set
      {
        Cast(ref value);
        if (double.IsNaN(value)) value = 0;
        _rangeNums[0] = value;
        if (value > UpperNumber) UpperNumber = value;
        Update();
      }
    }

    public double UpperNumber
    {
      [MethodImpl(MethodImplOptions.NoInlining)]
      get { return _rangeNums[1]; }
      set
      {
        Cast(ref value);
        if (double.IsNaN(value)) value = 10;
        _rangeNums[1] = value;
        if (value < LowerNumber) LowerNumber = value;
        Update();
      }
    }

    public double LowerResult
    {
      [MethodImpl(MethodImplOptions.NoInlining)]
      get { return _rangeNums[2]; }
      set
      {
        Cast(ref value);
        if (double.IsNaN(value)) value = 0;
        _rangeNums[2] = value;
        if (value > UpperResult) UpperResult = value;
      }
    }

    public double UpperResult
    {
      [MethodImpl(MethodImplOptions.NoInlining)]
      get { return _rangeNums[3]; }
      set
      {
        Cast(ref value);
        if (double.IsNaN(value)) value = 100;
        _rangeNums[3] = value;
        if (value < LowerResult) LowerResult = value;
      }
    }

    public double LowerLimit
    {
      [MethodImpl(MethodImplOptions.NoInlining)]
      get { return _rangeNums[4]; }
      set
      {
        Cast(ref value);
        if (double.IsNaN(value)) value = -Math.Sqrt(Limit);
        _rangeNums[4] = value;
        if (value > UpperLimit) UpperLimit = value;
      }
    }

    public double UpperLimit
    {
      [MethodImpl(MethodImplOptions.NoInlining)]
      get { return _rangeNums[5]; }
      set
      {
        Cast(ref value);
        if (double.IsNaN(value)) value = Math.Sqrt(Limit);
        _rangeNums[5] = value;
        if (value < LowerLimit) LowerLimit = value;
      }
    }

    public void Update()
    {
      if (_sessionSetup?.Precision > -1) return;
      var epsilon = Math.Pow(10, -_sessionSetup.Precision);
      _rangeNums[0] = LowerNumber - LowerNumber % epsilon;
      _rangeNums[1] = UpperNumber - UpperNumber % epsilon;
    }

    // ReSharper disable once UnusedMethodReturnValue.Global
    public double Cast(ref double value)
    {
      value = value < -Limit ? -Limit : value > Limit ? Limit : value;
      return value;
    }

    public void SetNumbers(double value1, double value2)
    {
      UpperNumber = value1 > value2 ? value1 : value2;
      LowerNumber = value1 > value2 ? value2 : value1;
    }

    public void SetResults(double value1, double value2)
    {
      UpperResult = value1 > value2 ? value1 : value2;
      LowerResult = value1 > value2 ? value2 : value1;
    }

    public void SetLimits(double value1, double value2)
    {
      UpperLimit = value1 > value2 ? value1 : value2;
      LowerLimit = value1 > value2 ? value2 : value1;
    }

    public void SetBoundaries(double upperBound, double lowerBound = 0)
    {
      SetNumbers(lowerBound, upperBound);
      SetResults(lowerBound, upperBound);
      SetLimits(lowerBound, upperBound);
    }

    public void SetAll(double lowerNumber, double upperNumber, double lowerResult, double upperResult,
      double lowerLimit = double.NaN, double upperLimit = double.NaN)
    {
      SetNumbers(lowerNumber, upperNumber);
      SetResults(lowerResult, upperResult);
      SetLimits(lowerLimit, upperLimit);
    }

    public bool WithinBounds(bool isResult, double value)
    {
      if (isResult) return value >= LowerResult && value <= UpperResult;
      return value >= LowerNumber && value <= UpperNumber;
    }

    public bool WithinLimits(double value)
    {
      return value <= UpperLimit && value >= LowerLimit;
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      return obj.GetType() == GetType() && Equals((NumberRange)obj);
    }

    protected bool Equals(NumberRange other)
    {
      return Equals(_rangeNums, other._rangeNums) &&
             Equals(_sessionSetup, other._sessionSetup);
    }

    public override int GetHashCode()
    {
      unchecked
      {
        var hashCode = _rangeNums?.GetHashCode() ?? 0;
        hashCode = (hashCode * 397) ^ (_sessionSetup?.GetHashCode() ?? 0);
        return hashCode;
      }
    }
  }
}