﻿Public Class frmMain
#Region "Variables"
  Public _blnDocumentationAvailable As Boolean = True

  Friend _Random As New Random
  Friend _intValuePts As Integer
  Friend _strEasyToolTip As String
  Friend _strHardToolTip As String
  Friend _strMediumToolTip As String
  Friend _blnActivateToolTips As Boolean
  Friend _intQuantityOfExcercises As Integer
  Friend _Cryptography As New clsCryptography
  Dim _strSuggestion() As String = Split(My.Settings.Suggestions, ".")
  Dim _blnHasStoppedLoading As Boolean
  Dim _strPreviousKeyChar As String
  Dim _intHighestQuantity As Integer
  Dim _blnLessThanSix As Boolean
  Dim _strType As String
#End Region

#Region "Form load and show"
  Private Sub frmMain_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
    clsAppInfo.SetDocumentsInfo(My.Application.Info.ProductName)
    My.Computer.FileSystem.CreateDirectory(clsAppInfo.AppDocuments)
    Try
      CopyDocumentsAndNavigate()
      FormDesign()
      If My.Settings.Evaluations <> "" Then
        cmdShowEvaluations.Enabled = True
      End If
      My.Settings.IsFirstRun = False
    Catch ex As Exception
      My.Settings.ErrorReport += clsAppInfo.ManageException(ex, "Er is een fout opgetreden tijdens het laden van " & My.Application.Info.ProductName & ".")
      CreateLogs()
      Close()
    End Try
  End Sub

  Private Sub frmMain_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
    _blnHasStoppedLoading = True
    Me.WindowState = FormWindowState.Maximized
    If My.Settings.ShowDisclaimer Then
      If MsgBox("Het programma " & My.Application.Info.ProductName & " is nog in constructie." & vbCrLf & "Bepaalde functies werken mogelijk niet of niet volledig." & vbCrLf & vbCrLf & _
         "Wil je deze disclaimer bij het opstarten laten tevoorschijnkomen?", MsgBoxStyle.Information + MsgBoxStyle.YesNo, "Disclaimer") = MsgBoxResult.No Then
        My.Settings.ShowDisclaimer = False
      End If
    End If
  End Sub

#Region "Methods"
  Private Sub FormDesign()
    'Form
    Me.Icon = My.Resources.DefaultIcon
    'ComboBox
    cboExcerciseType.Text = My.Settings.LastExcerciseType
    LayoutForEachType()
    'CheckBox
    If chkHulp.Checked Then
      chkHulp.BackColor = Color.FromArgb(0, 150, 255)
    Else
      chkHulp.BackColor = Color.FromArgb(255, 200, 0)
    End If
    'Groupbox
    WriteVersion(True)
    'WebBrowser
    webInfo.WebBrowserShortcutsEnabled = True
    'Button
    If Not _blnDocumentationAvailable Then
      cmdCalcRules.Visible = False
    End If
    'TextBox
    If My.Settings.IsFirstRun Then
      txtUsername.Text = SystemInformation.UserName
      cmdNewUser.PerformClick()
    End If
    txtUsername.AutoCompleteCustomSource.Clear()
    For i = 0 To _strSuggestion.GetUpperBound(0)
      txtUsername.AutoCompleteCustomSource.Add(_strSuggestion(i))
    Next
  End Sub

  Private Sub CopyDocumentsAndNavigate()
    'Directory
    My.Computer.FileSystem.CreateDirectory(clsAppInfo.AppDocuments & "Documenten\")
    'File
    My.Computer.FileSystem.WriteAllText(clsAppInfo.AppDocuments & "Documenten\Info.html", My.Resources.Info, False)
    My.Computer.FileSystem.WriteAllText(clsAppInfo.AppDocuments & "Documenten\Rekenregels.html", My.Resources.Rekenregels, False)
    'Browser
    webInfo.Navigate(clsAppInfo.AppDocuments & "Documenten\Info.html")
  End Sub
#End Region
#End Region

#Region "Manage sessions"
  Private Sub cmdStart_Click(sender As System.Object, e As System.EventArgs) Handles cmdStart.Click
    Try
      Dim j As Integer
      'Noodzakelijk
      Dim intQuantityOfExcercises As Integer
      Dim intRandomExerciseNumber As Integer
      Dim intExerciseTimesOne() As Integer = Nothing
      Dim strDisplayEvaluation As String = Nothing
      Dim blnFirstLoop As Boolean = True
      Dim strDisplayExercise As String = Nothing
      Dim strValidation As String = Nothing
      Dim strTest As String = Nothing
      Dim sngSolution As Single
      Dim intAnswer As Integer
      'Instellingen
      Dim intMaxQuantity As Integer = 100
      Dim sngMaxOutput As Single
      Dim sngMinOutput As Single = -1000000
      Dim sngMaxOne As Single
      Dim sngMinOne As Single
      Dim intMaxZero As Integer
      Dim intQuantityOfNums As Integer
      'Extra's
      Dim intScore As Integer
      Dim intRightAnswers As Integer
      Dim intMinimumToRecommend As Integer = 10
      'Stopwatch
      Dim spwStopwatch As New Stopwatch()
      Dim intElapsedMinutes As Integer
      Dim intElapsedSeconds As Integer
      Dim intElapsedMilliseconds As Integer
      Dim strStopwatchToMessage As String
      Dim strLvlOfDifficulty As String
      'Opslaan van evaluatie
      Dim strEvaluation As String
      Dim strExerciseType As String = Nothing

      DisplayEvaluation()
      'Profiel
      My.Settings.Username = UCase(Mid(My.Settings.Username, 1, 1)) & Mid(My.Settings.Username, 2, Len(My.Settings.Username))
      strEvaluation = "- Prestaties van " & My.Settings.Username & ": " & vbCrLf & vbCrLf
      If My.Settings.Age = 0 Then
        My.Settings.Age = 18
      End If
      'Check instellingen
      Settings(sngMaxOutput, sngMinOutput, sngMaxOne, sngMinOne, intQuantityOfNums)
      If Advanced(sngMaxOutput, sngMinOutput, sngMaxOne, sngMinOne) Then
        Exit Sub
      End If
      'Bepaal intQuantity
      intQuantityOfExcercises = intMaxQuantity + 1
      While intQuantityOfExcercises > intMaxQuantity
        strTest = InputBox("Hoeveel oefeningen wil je doen?", "Aantal oefeningen", "Voer een getal in...")
        If strTest <> "" Then
          If IsNumeric(strTest) = True Then
            If strTest > intMaxQuantity Then
              intQuantityOfExcercises = strTest
              MsgBox("Je mag maximaal " & intMaxQuantity & " oefeningen maken in één keer.", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Aantal oefeningen")
            Else
              intQuantityOfExcercises = strTest
            End If
          Else
            intQuantityOfExcercises = intMaxQuantity + 1
            MsgBox("Geef een getal in.", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Aantal oefeningen")
          End If
        Else
          DisplayEvaluation(False)
          Exit Sub
        End If
      End While
      intMaxZero = intQuantityOfExcercises / 2
      'Start sessie
      _intQuantityOfExcercises = 100 / intQuantityOfExcercises
      If 100 / intQuantityOfExcercises <> _intQuantityOfExcercises Then
        frmSession.prgQuantity.Style = ProgressBarStyle.Marquee
      End If
      frmSession._blnFirstRun = True
      spwStopwatch.Start()
      For i = 1 To intQuantityOfExcercises
        'Ga uit loop als de sessie is geanulleerd
        If strTest = "Cancel" Then
          Continue For
        End If
        Try
          Select Case _strType
            Case Is = " x "
              UseWaitCursor = True
              Product(intQuantityOfNums, sngMaxOutput, sngMinOutput, sngMaxOne, sngMinOne, sngSolution, strDisplayExercise, _strType, strValidation, intExerciseTimesOne, _
                      intRandomExerciseNumber, intQuantityOfExcercises, intMaxQuantity, j, i, blnFirstLoop)
            Case Is = " : "
              UseWaitCursor = True
              Quotiënt(intQuantityOfNums, sngMaxOutput, sngMinOutput, sngSolution, strDisplayExercise, _strType, strValidation)
            Case Is = " - "
              UseWaitCursor = True
              Verschil(intQuantityOfNums, sngMaxOutput, sngMinOutput, intMaxZero, sngSolution, strDisplayExercise, _strType, strValidation)
            Case Is = " + "
              UseWaitCursor = True
              Som(intQuantityOfNums, sngMaxOutput, sngMinOutput, sngSolution, strDisplayExercise, _strType, strValidation)
          End Select
        Catch ex As Exception
          My.Settings.ErrorReport += clsAppInfo.ManageException(ex, "Er is een fout opgetreden tijdens het genereeren van de oefeningen.")
          CreateLogs()
        End Try
        'Werk met frmSession
        frmSession.lblQuestion.Text = "Wat is: " & Mid(strDisplayExercise, 1, strDisplayExercise.Length - 2)
        frmSession.ShowDialog()
        strTest = frmSession._strTest
        If strTest = "Cancel" Then
          If strDisplayEvaluation = Nothing Then
            strDisplayEvaluation += "Oefening gestopt op " & i & " van " & intQuantityOfExcercises & "."
          Else
            strDisplayEvaluation += vbCrLf & "Oefening gestopt op " & i & " van " & intQuantityOfExcercises & "."
          End If
          UseWaitCursor = False
          intQuantityOfExcercises = i - 1
          'Ga uit loop als de sessie is geanulleerd
          If strTest = "Cancel" Then
            Continue For
          End If
        Else
          intAnswer = Integer.MaxValue
          If IsNumeric(strTest) Then
            If strTest < intAnswer Then
              intAnswer = strTest
            Else
              intAnswer = 0
            End If
          Else
            intAnswer = 0
          End If
        End If
        'Vergelijk de 2 uitkomsten
        If sngSolution = intAnswer Then
          intRightAnswers += 1
          intScore = intRightAnswers / intQuantityOfExcercises * 100
        Else
          If i <> intQuantityOfExcercises Then
            strDisplayEvaluation += "Nr. " & i & ":  " & strDisplayExercise & sngSolution & "  →  " & "Antwoord: " & intAnswer & vbCrLf
          Else
            strDisplayEvaluation += "Nr. " & i & ":  " & strDisplayExercise & sngSolution & "  →  " & "Antwoord: " & intAnswer
          End If
        End If
        _intValuePts = intRightAnswers / i * 100
        'Zorgt ervoor dat while loops draaien bij de volgende loop
        sngSolution = sngMaxOutput + 1
        strDisplayExercise = ""
      Next
      'Verwerk de waarden uit de stopwatch
      spwStopwatch.Stop()
      intElapsedMinutes = Math.Round(spwStopwatch.ElapsedMilliseconds / 1000 / 60 - 0.5)
      intElapsedSeconds = Math.Round(spwStopwatch.ElapsedMilliseconds / 1000 - 0.5) - Convert.ToInt32(intElapsedMinutes) * 60
      intElapsedMilliseconds = spwStopwatch.ElapsedMilliseconds - Math.Round((spwStopwatch.ElapsedMilliseconds / 1000 - 0.5)) * 1000
      'Voeg de waarden toe in een bericht
      Select Case 0
        Case intElapsedMinutes
          strStopwatchToMessage = "Tijd nodig: " & intElapsedSeconds & "s en " & intElapsedMilliseconds & "ms"
        Case intElapsedSeconds
          strStopwatchToMessage = "Tijd nodig: " & intElapsedMilliseconds & "ms"
        Case Else
          strStopwatchToMessage = "Tijd nodig: " & intElapsedMinutes & "m, " & intElapsedSeconds & "s en " & intElapsedMilliseconds & "ms"
      End Select
      'Beveel aan
      If intRightAnswers > 0 Then
        Select Case intScore
          Case Is >= 60
            If intQuantityOfExcercises >= intMinimumToRecommend Then
              MsgBox("Je hebt " & intRightAnswers & " juiste antwoorden op de " & intQuantityOfExcercises & " oefeningen." & vbCrLf & "Je mag gerust een oefening starten die moeilijker is.", _
                     MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Aantal juist")
            Else
              If intRightAnswers <> 1 Then
                If intQuantityOfExcercises <> 1 Then
                  MsgBox("Je hebt " & intRightAnswers & " juiste antwoorden op de " & intQuantityOfExcercises & " oefeningen.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, _
                         "Aantal juist")
                Else
                  MsgBox("Je hebt " & intRightAnswers & " juiste antwoorden op " & intQuantityOfExcercises & " oefening.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Aantal juist")
                End If
              Else
                If intQuantityOfExcercises <> 1 Then
                  MsgBox("Je hebt " & intRightAnswers & " juiste antwoord op de " & intQuantityOfExcercises & " oefeningen.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Aantal juist")
                Else
                  MsgBox("Je hebt " & intRightAnswers & " juiste antwoord op " & intQuantityOfExcercises & " oefening.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Aantal juist")
                End If
              End If
            End If
          Case Is <= 30
            If intQuantityOfExcercises >= intMinimumToRecommend Then
              MsgBox("Je hebt " & intRightAnswers & " juiste antwoorden op de " & intQuantityOfExcercises & " oefeningen." & vbCrLf & _
                     "Ik raad je aan een oefening te starten die makkelijker is.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Aantal juist")
            Else
              If intRightAnswers <> 1 Then
                If intQuantityOfExcercises <> 1 Then
                  MsgBox("Je hebt " & intRightAnswers & " juiste antwoorden op de " & intQuantityOfExcercises & " oefeningen.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, _
                         "Aantal juist")
                Else
                  MsgBox("Je hebt " & intRightAnswers & " juiste antwoorden op " & intQuantityOfExcercises & " oefening.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Aantal juist")
                End If
              Else
                If intQuantityOfExcercises <> 1 Then
                  MsgBox("Je hebt " & intRightAnswers & " juiste antwoord op de " & intQuantityOfExcercises & " oefeningen.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Aantal juist")
                Else
                  MsgBox("Je hebt " & intRightAnswers & " juiste antwoord op " & intQuantityOfExcercises & " oefening.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Aantal juist")
                End If
              End If
            End If
          Case Else
            If intRightAnswers <> 1 Then
              If intQuantityOfExcercises <> 1 Then
                MsgBox("Je hebt " & intRightAnswers & " juiste antwoorden op de " & intQuantityOfExcercises & " oefeningen.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Aantal juist")
              Else
                MsgBox("Je hebt " & intRightAnswers & " juiste antwoorden op " & intQuantityOfExcercises & " oefening.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Aantal juist")
              End If
            Else
              If intQuantityOfExcercises <> 1 Then
                MsgBox("Je hebt " & intRightAnswers & " juiste antwoord op de " & intQuantityOfExcercises & " oefeningen.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Aantal juist")
              Else
                MsgBox("Je hebt " & intRightAnswers & " juiste antwoord op " & intQuantityOfExcercises & " oefening.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Aantal juist")
              End If
            End If
        End Select
      End If
      'Voeg gegevens uit stopwatch toe aan de evaluatie indien er nog geen evaluatie is
      If strDisplayEvaluation = Nothing Then
        rtbEvaluation.Text = strStopwatchToMessage
      Else
        rtbEvaluation.Text = strDisplayEvaluation & vbCrLf & vbCrLf & strStopwatchToMessage
      End If
      'Voeg de weergegeven evaluatie toe aan de werkelijke evaluatie
      strEvaluation += rtbEvaluation.Text
      'Maak duidelijk wat de bewerking is van de oefening
      Select Case _strType
        Case Is = " x "
          strExerciseType = "Type oefening: Product ( x )"
        Case Is = " : "
          strExerciseType = "Type oefening: Quotiënt ( : )"
        Case Is = " + "
          strExerciseType = "Type oefening: Som ( + )"
        Case Is = " - "
          strExerciseType = "Type oefening: Verschil ( - )"
      End Select
      strLvlOfDifficulty = CheckLvlOfDifficulty()
      strEvaluation += vbCrLf & vbCrLf & "Moeilijkheidsgraad: " & strLvlOfDifficulty & vbCrLf & strExerciseType & vbCrLf & vbCrLf & vbCrLf
      'Beheer evaluatie
      cmdShowEvaluations.Enabled = True
      cmdDeleteEvaluation.Enabled = False
      My.Settings.Evaluations += strEvaluation
      UseWaitCursor = False
    Catch ex As Exception
      My.Settings.ErrorReport += clsAppInfo.ManageException(ex, "Er is een fout opgetreden tijdens het verwerken van de oefeningen.")
      CreateLogs()
      Exit Sub
    End Try
  End Sub

#Region "Methods"
#Region "Settings"
  Function Advanced(ByRef sngMaxOutput As Single, ByRef sngMinOutput As Single, ByRef sngMaxOne As Single, ByRef sngMinOne As Single)
    If txtMaxOutput.Text <> "" Then
      sngMaxOutput = txtMaxOutput.Text
    End If
    If txtMinOutput.Text <> "" Then
      If txtMinOutput.Text > sngMaxOutput Then
        MsgBox("De maximale uitkomst is lager dan de minimale uitkomst. Controleer de instellingen onder ""Moeilijkheidsgraad"" en de maximale uitkomst onder ""Geavanceerd"".", _
               MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Maximum kleiner dan minimum")
        Return True
        Exit Function
      Else
        sngMinOutput = txtMinOutput.Text
      End If
    End If
    If txtMaxOne.Text <> "" Then
      sngMaxOne = txtMaxOne.Text / 100
    End If
    If txtMinOne.Text <> "" Then
      If txtMinOne.Text / 100 > sngMaxOne Then
        MsgBox("Maximaal aantal keer 1 is lager dan het minimale aantal keer 1. Controleer de instellingen onder ""Moeilijkheidsgraad"" en het maximale aantal keer 1 onder ""Geavanceerd"".", _
               MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Maximum kleiner dan minimum")
        Return True
        Exit Function
      End If
      sngMinOne = txtMinOne.Text / 100
    End If
    If chkNoZero.Checked = True And sngMinOutput = 0 Then
      sngMinOutput = 1
    End If
    Return False
  End Function

  Private Sub Settings(ByRef sngMaxOutput As Single, ByRef sngMinOutput As Single, ByRef sngMaxOne As Single, ByRef sngMinOne As Single, ByRef intQuantityOfNums As Integer)
    Select Case True
      Case rdoEasy.Checked
        sngMaxOutput = 10
        sngMinOutput = 0
        sngMaxOne = 0.2
        sngMinOne = 0.1
      Case rdoMedium.Checked
        sngMaxOutput = 100
        sngMinOutput = 10
        sngMaxOne = 0.1
        sngMinOne = 0.05
      Case rdoHard.Checked
        sngMaxOutput = 1000
        sngMinOutput = 100
        sngMaxOne = 0.05
        sngMinOne = 0
        '+ Tekens gemixt
    End Select
    If Not IsNumeric(txtQuantityOfNums.Text) Then
      txtQuantityOfNums.Text = 2
    End If
    If txtQuantityOfNums.Text < 2 Then
      txtQuantityOfNums.Text = 2
    End If

    intQuantityOfNums = txtQuantityOfNums.Text
  End Sub
#End Region

#Region "Session"
  Private Sub Product(intQuantityOfNums As Integer, ByRef sngMaxOutput As Single, ByRef sngMinOutput As Single, ByRef sngMaxOne As Single, ByRef sngMinOne As Single, ByRef sngSolution As Single, _
                      ByRef strDisplayExercise As String, ByRef _strType As String, ByRef strValidation As String, ByRef intExerciseTimesOne() As Integer, ByRef intRandomExerciseNumber As Integer, _
                      ByRef intQuantityOfExcercises As Integer, ByRef intMaxQuantity As Integer, ByRef i As Integer, ByRef j As Integer, ByRef blnFirstLoop As Boolean)
    Dim sngPercentTimesOne As Single = 1 / intQuantityOfExcercises
    Dim sngNumber(intQuantityOfNums - 1) As Single

    'Bepaal welke oefeningen maal 1 hebben.
    If blnFirstLoop = True Then
      ReDim intExerciseTimesOne(0 To _Random.Next((sngMinOne * intQuantityOfExcercises) - 1, (sngMaxOne * intQuantityOfExcercises)))
      For j = 0 To intExerciseTimesOne.GetUpperBound(0)
        blnFirstLoop = False
        Do Until intRandomExerciseNumber <> 0 And Array.IndexOf(intExerciseTimesOne, intRandomExerciseNumber) = -1
          If Array.IndexOf(intExerciseTimesOne, intRandomExerciseNumber) <> -1 Or intRandomExerciseNumber = 0 Then
            intRandomExerciseNumber = _Random.Next(intQuantityOfExcercises) + 1
          End If
        Loop
        intExerciseTimesOne(j) = intRandomExerciseNumber
      Next
      Array.Sort(intExerciseTimesOne)
      j = 0
    End If

    'Oefeningen worden gecreëerd.
    sngMaxOne += 1 / intMaxQuantity
    If sngMaxOne < sngPercentTimesOne Then
      Do While sngSolution > sngMaxOutput Or strValidation = strDisplayExercise Or sngSolution < sngMinOutput
        'Solution = 1 zodat de uitkomst niet telkens 0 is.
        sngSolution = 1
        strDisplayExercise = ""
        'Creëeren van inhoud en valideren.
        While intQuantityOfNums <> 0
          intQuantityOfNums -= 1
          sngNumber(intQuantityOfNums) = 1
          While sngNumber(intQuantityOfNums) = 1
            sngNumber(intQuantityOfNums) = _Random.Next(sngMaxOutput + 1)
          End While
          sngSolution *= sngNumber(intQuantityOfNums)
          If intQuantityOfNums <> 0 Then
            strDisplayExercise += sngNumber(intQuantityOfNums) & _strType
          Else
            strDisplayExercise += sngNumber(intQuantityOfNums) & " = "
          End If
        End While
        'Herdefiniëren van intQuantityOfNums.
        intQuantityOfNums = txtQuantityOfNums.Text
      Loop
    Else
      Do While sngSolution > sngMaxOutput Or strValidation = strDisplayExercise Or sngSolution < sngMinOutput
        'Solution = 1 zodat de uitkomst niet telkens 0 is.
        sngSolution = 1
        strDisplayExercise = ""
        'Creëeren van inhoud en valideren.
        While intQuantityOfNums <> 0
          intQuantityOfNums -= 1
          sngNumber(intQuantityOfNums) = _Random.Next(sngMaxOutput + 1)
          sngSolution *= sngNumber(intQuantityOfNums)
          If intQuantityOfNums <> 0 Then
            strDisplayExercise += sngNumber(intQuantityOfNums) & _strType
          Else
            strDisplayExercise += sngNumber(intQuantityOfNums) & " = "
          End If
        End While
        'Herdefiniëren van intQuantityOfNums.
        intQuantityOfNums = txtQuantityOfNums.Text
        'Er moet een 1 zijn.
        If i = intExerciseTimesOne(j) Then
          sngNumber(_Random.Next(0, intQuantityOfNums)) = 1
          'Creëeren van inhoud en valideren.
          sngSolution = 1
          strDisplayExercise = ""
          While intQuantityOfNums <> 0
            intQuantityOfNums -= 1
            sngSolution *= sngNumber(intQuantityOfNums)
            If intQuantityOfNums <> 0 Then
              strDisplayExercise += sngNumber(intQuantityOfNums) & _strType
            Else
              strDisplayExercise += sngNumber(intQuantityOfNums) & " = "
            End If
          End While
          'Herdefiniëren van intQuantityOfNums.
          intQuantityOfNums = txtQuantityOfNums.Text
        ElseIf Array.IndexOf(sngNumber, Convert.ToSingle(1)) <> -1 Then
          'Creëeren van inhoud en valideren.
          sngSolution = 1
          strDisplayExercise = ""
          While intQuantityOfNums <> 0
            intQuantityOfNums -= 1
            sngSolution *= sngNumber(intQuantityOfNums)
            If intQuantityOfNums <> 0 Then
              strDisplayExercise += sngNumber(intQuantityOfNums) & _strType
            Else
              strDisplayExercise += sngNumber(intQuantityOfNums) & " = "
            End If
          End While
          'Herdefiniëren van intQuantityOfNums.
          intQuantityOfNums = txtQuantityOfNums.Text
        End If
        If (sngSolution > sngMaxOutput Or strValidation = strDisplayExercise Or sngSolution < sngMinOutput) = False And Array.IndexOf(sngNumber, Convert.ToSingle(1)) <> -1 And _
            intExerciseTimesOne.GetUpperBound(0) <> j Then
          j += 1
          sngMaxOne -= sngPercentTimesOne
        End If
      Loop
      strValidation = strDisplayExercise
    End If
    'Sorteren voor strDisplay.
    Array.Reverse(sngNumber)
  End Sub

  Private Sub Quotiënt(intQuantityOfNums As Integer, ByRef sngMaxOutput As Single, ByRef sngMinOutput As Single, ByRef sngSolution As Single, ByRef strDisplayExercise As String, _
                       ByRef _strType As String, ByRef strValidation As String)
    Dim sngDivider As Single = _Random.Next(1, sngMaxOutput * 0.5 + 1)
    Dim sngNumber(intQuantityOfNums - 1) As Single
    Dim intPreviousPart As Integer = sngSolution
    Dim intHiLoRnd As Integer
    Dim intLooped As Integer

    sngSolution = sngMaxOutput + 1
    While sngSolution > sngMaxOutput Or (strValidation = strDisplayExercise And strValidation <> Nothing) Or sngSolution < sngMinOutput
      'Creëeren van inhoud en valideren.
      While intQuantityOfNums <> 0 And sngSolution = Math.Round(sngSolution)
        intQuantityOfNums -= 1
        If intQuantityOfNums = txtQuantityOfNums.Text - 1 Then
          'Genereer een eerste getal
          sngNumber(intQuantityOfNums) = sngMaxOutput + 1
          While sngNumber(intQuantityOfNums) > sngMaxOutput
            sngNumber(intQuantityOfNums) = _Random.Next(sngMaxOutput * (_Random.Next(50, 101) / 100), sngMaxOutput + 1) / sngDivider
            sngNumber(intQuantityOfNums) = Math.Round(Convert.ToDecimal(sngNumber(intQuantityOfNums))) * Math.Round(sngDivider)
            If sngNumber(intQuantityOfNums) = 0 Then
              sngDivider = sngDivider * sngMaxOutput
            End If
            sngDivider *= 0.9
          End While
          intPreviousPart = sngNumber(intQuantityOfNums)
        Else
          'Hi Lo of Rnd
          intHiLoRnd = _Random.Next(0, 3)
          If intHiLoRnd = 0 Then
            If intQuantityOfNums = 2 Then
              intHiLoRnd = _Random.Next(1, 3)
            Else
              intHiLoRnd = _Random.Next(0, 3)
            End If
          End If
          Select Case intHiLoRnd
            Case 0
              sngNumber(intQuantityOfNums) = intPreviousPart
            Case 1
              If sngNumber(intQuantityOfNums + 1) = 1 Then
                sngNumber(intQuantityOfNums) = 1
              Else
                For i = sngNumber(intQuantityOfNums + 1) - 1 To 1 Step -1
                  sngNumber(intQuantityOfNums) = i
                  If intPreviousPart Mod sngNumber(intQuantityOfNums) = 0 Then
                    i = 1
                  End If
                Next
              End If
            Case 2
              intLooped = 0
              While intPreviousPart Mod sngNumber(intQuantityOfNums) <> 0
                intLooped += 1
                If intPreviousPart = 1 Or intLooped / 2 = intPreviousPart Then
                  sngNumber(intQuantityOfNums) = 1
                Else
                  sngNumber(intQuantityOfNums) = _Random.Next(intPreviousPart / 9, intPreviousPart / 2 + 1)
                End If
              End While
          End Select
          'Dit zorgt ervoor dat er pas een 1 is op het eind.
          If intQuantityOfNums <> 0 And sngNumber(intQuantityOfNums) = 1 Then
            sngNumber(intQuantityOfNums) = sngNumber(intQuantityOfNums + 1)
          End If
          If Not sngNumber(intQuantityOfNums) > intPreviousPart Then
            intPreviousPart /= sngNumber(intQuantityOfNums)
          End If
        End If
      End While
      Array.Reverse(sngNumber)
      'Solution berekenen.
      intQuantityOfNums = txtQuantityOfNums.Text
      strDisplayExercise = ""
      For i = 0 To intQuantityOfNums - 1
        If i <> intQuantityOfNums - 1 Then
          strDisplayExercise += sngNumber(i) & _strType
        Else
          strDisplayExercise += sngNumber(i) & " = "
        End If
        If i = 0 Then
          sngSolution = sngNumber(i)
        Else
          sngSolution /= sngNumber(i)
        End If
      Next
    End While
    strValidation = strDisplayExercise
    Array.Reverse(sngNumber)
  End Sub

  Private Sub Verschil(intQuantityOfNums As Integer, ByRef sngMaxOutput As Single, ByRef sngMinOutput As Single, ByRef intMaxZero As Integer, ByRef sngSolution As Single, _
                       ByRef strDisplayExercise As String, ByRef _strType As String, ByRef strValidation As String)
    Dim sngNumber(intQuantityOfNums - 1) As Single
    Dim intDifference As Integer

    While sngSolution > sngMaxOutput Or strValidation = strDisplayExercise Or sngSolution < sngMinOutput Or sngSolution < 0
      intDifference = sngNumber(sngNumber.GetUpperBound(0)) - sngMaxOutput / 1.5
      sngSolution = 0
      'Creëeren van inhoud en valideren.
      While intQuantityOfNums <> 0
        intQuantityOfNums -= 1
        If intQuantityOfNums = txtQuantityOfNums.Text - 1 Then
          sngNumber(intQuantityOfNums) = _Random.Next(sngMaxOutput * 0.5, sngMaxOutput + 1)
        Else
          If intDifference < 0 Then
            intDifference = 0
          End If
          sngNumber(intQuantityOfNums) = _Random.Next(intDifference / 2, intDifference)
          If sngNumber(intQuantityOfNums) = 0 Then
            If intMaxZero > 0 Then
              intMaxZero -= 1
            Else
              sngNumber(intQuantityOfNums) = sngNumber(_Random.Next(intQuantityOfNums, sngNumber.Length))
            End If
          End If
        End If
      End While
      'Sorteren zodat er geen negatieve uikomsten zijn.
      Array.Sort(sngNumber)
      Array.Reverse(sngNumber)
      'Solution berekenen.
      intQuantityOfNums = txtQuantityOfNums.Text
      strDisplayExercise = ""
      For i = 0 To intQuantityOfNums - 1
        If i <> intQuantityOfNums - 1 Then
          strDisplayExercise += sngNumber(i) & _strType
        Else
          strDisplayExercise += sngNumber(i) & " = "
        End If
        If i = 0 Then
          sngSolution = sngNumber(i)
        Else
          sngSolution -= sngNumber(i)
        End If
      Next
      intQuantityOfNums = txtQuantityOfNums.Text
      'Sorteren voor strDisplay.
      Array.Sort(sngNumber)
    End While
    strValidation = strDisplayExercise
  End Sub

  Private Sub Som(intQuantityOfNums As Integer, ByRef sngMaxOutput As Single, ByRef sngMinOutput As Single, ByRef sngSolution As Single, ByRef strDisplayExercise As String, _strType As String, _
                  ByRef strValidation As String)
    Dim sngNumber(intQuantityOfNums - 1) As Single

    While sngSolution > sngMaxOutput Or strValidation = strDisplayExercise Or sngSolution < sngMinOutput
      sngSolution = 0
      strDisplayExercise = ""
      'Creëeren van inhoud en valideren
      While intQuantityOfNums <> 0
        intQuantityOfNums -= 1
        sngNumber(intQuantityOfNums) = _Random.Next(sngMaxOutput + 1)
        sngSolution += sngNumber(intQuantityOfNums)
        If intQuantityOfNums <> 0 Then
          strDisplayExercise += sngNumber(intQuantityOfNums) & _strType
        Else
          strDisplayExercise += sngNumber(intQuantityOfNums) & " = "
        End If
      End While
      'Herdefiniëren van intQuantityOfNums.
      intQuantityOfNums = txtQuantityOfNums.Text
    End While
    strValidation = strDisplayExercise
  End Sub
#End Region

  Private Function CheckLvlOfDifficulty()
    Dim strLvlOfDifficulty As String

    Select Case True
      Case rdoEasy.Checked
        strLvlOfDifficulty = "Makkelijk"
      Case rdoMedium.Checked
        strLvlOfDifficulty = "Gemiddeld"
      Case rdoHard.Checked
        strLvlOfDifficulty = "Moeilijk"
      Case Else
        strLvlOfDifficulty = "Aangepast"
    End Select

    Return strLvlOfDifficulty
  End Function
#End Region
#End Region

#Region "Manage rtbEvaluation"
  Private Sub rtbEvaluation_VisibleChanged(sender As Object, e As EventArgs) Handles rtbEvaluation.VisibleChanged
    If rtbEvaluation.Visible = True Then
      webInfo.Visible = False
      cmdCalcRules.Visible = False
    ElseIf _blnDocumentationAvailable = True Then
      webInfo.Visible = True
      cmdCalcRules.Visible = True
    Else
      webInfo.Visible = True
    End If
  End Sub

  Private Sub tmrRefreshForm_Tick(sender As Object, e As EventArgs) Handles tmrRefresh.Tick
    Try
      If My.Settings.Evaluations = "" Then
        cmdShowEvaluations.Enabled = False
        cmdDeleteEvaluation.Enabled = False
      Else
        cmdShowEvaluations.Enabled = True
      End If
    Catch ex As Exception
      tmrRefresh.Stop()
      My.Settings.ErrorReport += clsAppInfo.ManageException(ex, "Er is een fout opgetreden tijdens het verversen van " & My.Application.Info.ProductName & ".")
      CreateLogs()
      Close()
    End Try
  End Sub

  Private Sub cmdShowEvaluations_Click(sender As System.Object, e As System.EventArgs) Handles cmdShowEvaluations.Click
    rtbEvaluation.Text = My.Settings.Evaluations
    DisplayEvaluation()
    cmdDeleteEvaluation.Enabled = True
  End Sub

  Private Sub cmdDeleteEvaluation_Click(sender As System.Object, e As System.EventArgs) Handles cmdDeleteEvaluation.Click
    Try
      If MsgBox("Ben je zeker dat je alle prestaties wilt verwijderen?" & vbCrLf & "Eenmaal alles verwijdert is zijn je gegevens onherstelbaar.", MsgBoxStyle.YesNo + MsgBoxStyle.Question, _
                  "Prestaties verwijderen") = MsgBoxResult.Yes Then
        My.Settings.Evaluations = ""
        My.Computer.Audio.Play(My.Resources.SoundDeleting, AudioPlayMode.Background)
        DisplayEvaluation()
        rtbEvaluation.Text = "Correcties komen hier te staan..."
        cmdDeleteEvaluation.Enabled = False
        cmdShowEvaluations.Enabled = False
      End If
    Catch ex As Exception
      My.Settings.ErrorReport += clsAppInfo.ManageException(ex, "Er is een fout opgetreden tijdens het verwijderen van je evaluaties.")
      CreateLogs()
    End Try
  End Sub

  Private Sub DisplayEvaluation(Optional ByVal ShowEvaluation As Boolean = True)
    If ShowEvaluation Then
      rtbEvaluation.ScrollBars = ScrollBars.Vertical
      rtbEvaluation.Visible = True
    Else
      rtbEvaluation.ScrollBars = ScrollBars.None
      rtbEvaluation.Visible = False
      rtbEvaluation.Text = "Correcties komen hier te staan..."
    End If
  End Sub
#End Region

#Region "Form functionality"
  Private Sub TextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles txtMaxOutput.KeyDown, txtMinOutput.KeyDown, txtMaxOne.KeyDown, txtMinOne.KeyDown, txtQuantityOfNums.KeyDown
    If e.KeyCode = Keys.Enter Then
      cmdStart.PerformClick()
    End If
  End Sub

  Private Sub TextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMaxOutput.KeyPress, txtMinOutput.KeyPress, txtMaxOne.KeyPress, txtMinOne.KeyPress, txtQuantityOfNums.KeyPress
    'Algemeen
    If (IsNumeric(e.KeyChar) Or e.KeyChar = Convert.ToChar(Keys.Back)) = False Then
      e.Handled = True
    End If
    'Extra txtMaxOutput
    If sender.Name = "txtMaxOutput" Then
      If (e.KeyChar = "0" Or e.KeyChar = "1" Or e.KeyChar = "2" Or e.KeyChar = "3" Or e.KeyChar = "4" Or e.KeyChar = "5") And txtMaxOutput.Text = "" Then
        _strPreviousKeyChar = e.KeyChar
        e.KeyChar = "6"
        _blnLessThanSix = True
      ElseIf _blnLessThanSix And txtMaxOutput.TextLength = 1 And IsNumeric(e.KeyChar) Then
        _blnLessThanSix = False
        txtMaxOutput.Text = _strPreviousKeyChar
        txtMaxOutput.SelectionStart = 2
      End If
    End If
    'Extra txtQuantityOfNums
    If IsNumeric(e.KeyChar) Then
      If sender.Name = "txtQuantityOfNums" And (e.KeyChar > _intHighestQuantity.ToString Or e.KeyChar < "2") Then
        If e.KeyChar > _intHighestQuantity.ToString Then
          grpMessage.Text = "Het getal dat je wilt invoeren is te hoog." & vbCrLf & "Alles is mogelijk van 2 tot " & _intHighestQuantity & "."
        End If
        e.Handled = True
      End If
    ElseIf e.KeyChar <> Convert.ToChar(Keys.Back) Then
      e.Handled = True
    End If
  End Sub

  Private Sub TextBox_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtMaxOutput.TextChanged, txtMinOutput.TextChanged, txtMaxOne.TextChanged, txtMinOne.TextChanged, _
      txtQuantityOfNums.TextChanged
    cmdClear.Enabled = True
    If sender.Name = "txtMaxOne" And txtMaxOne.Text <> "" Then
      If txtMaxOne.Text > 100 Then
        txtMaxOne.Text = 100
        txtMaxOne.SelectAll()
      End If
    End If
    If sender.Name = "txtMinOne" And txtMinOne.Text <> "" Then
      If txtMinOne.Text > 100 Then
        txtMinOne.Text = 100
        txtMinOne.SelectAll()
      End If
    End If
  End Sub

  Private Sub RadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdoEasy.CheckedChanged, rdoMedium.CheckedChanged, rdoHard.CheckedChanged
    cmdClear.Enabled = True

    If _blnHasStoppedLoading And Not IsNumeric(txtQuantityOfNums.Text) Then
      Select Case True
        Case rdoEasy.Checked
          txtQuantityOfNums.Text = 2
        Case rdoMedium.Checked
          txtQuantityOfNums.Text = 3
        Case rdoHard.Checked
          txtQuantityOfNums.Text = 3
      End Select
    End If
  End Sub

  Private Sub CheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles chkNoZero.CheckedChanged
    cmdClear.Enabled = True
  End Sub

  Private Sub cmdClear_Click(sender As System.Object, e As System.EventArgs) Handles cmdClear.Click
    'RadioButton
    rdoEasy.Checked = True
    'CheckBox
    chkNoZero.Checked = False
    'TextBox
    txtMaxOutput.Clear()
    txtMinOutput.Clear()
    txtMaxOne.Clear()
    txtMinOne.Clear()
    txtQuantityOfNums.Clear()
    rtbEvaluation.Text = "Correcties komen hier te staan..."
    rtbEvaluation.Visible = False
    cmdClear.Enabled = False
  End Sub

  Private Sub cmdNewUser_Click(sender As System.Object, e As System.EventArgs) Handles cmdNewUser.Click
    If _blnHasStoppedLoading Then
      If txtUsername.Text = "" Then
        MsgBox("Vul een gebruikersnaam in.", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Vul gebruikersnaam in")
        Exit Sub
      ElseIf txtAge.Text = "" Then
        MsgBox("Vul je leeftijd in.", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Vul leeftijd in")
      Else
        If txtAge.Text < 3 Then
          txtAge.Text = "3"
        End If
        My.Settings.Age = txtAge.Text
        My.Settings.Username = txtUsername.Text
        MsgBox("Welkom " & txtUsername.Text & "!" & vbCrLf & "Je profiel met naam en leeftijd is aangemaakt.", MsgBoxStyle.Information, "Profiel aangemaakt")
        'Voeg naam toe aan suggesties
        Dim blnDoesNotPass As Boolean

        For i = 0 To _strSuggestion.GetUpperBound(0)
          If _strSuggestion(i) = txtUsername.Text Then
            blnDoesNotPass = True
          End If
        Next
        If blnDoesNotPass = False Then
          My.Settings.Suggestions += "." & txtUsername.Text
          Array.Resize(_strSuggestion, _strSuggestion.Length + 1)
          _strSuggestion(_strSuggestion.GetUpperBound(0)) = txtUsername.Text
          txtUsername.AutoCompleteCustomSource.Add(txtUsername.Text)
        End If
      End If
    End If
  End Sub

  Private Sub chkHulp_CheckedChanged(sender As Object, e As EventArgs) Handles chkHulp.CheckedChanged
    If chkHulp.Checked Then
      _blnActivateToolTips = True
      My.Settings.Hulp = True
      chkHulp.BackColor = Color.FromArgb(0, 150, 255)
      If _blnHasStoppedLoading Then
        MsgBox("Beweeg je muis over wat je niet begrijpt en een tooltip komt tevoorschijn", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Tooltips zijn actief")
      End If
    Else
      _blnActivateToolTips = False
      My.Settings.Hulp = False
      chkHulp.BackColor = Color.FromArgb(255, 200, 0)
      WriteVersion()
    End If
  End Sub

  Private Sub cmdManageMem_Click(sender As Object, e As EventArgs) Handles cmdManageMem.Click
    frmManageMem.ShowDialog()
  End Sub

  Private Sub WriteVersion(Optional AppJustStarted As Boolean = False)
    If AppJustStarted Then
      grpMessage.Text = "Welkom!" & vbCrLf & vbCrLf
    Else
      grpMessage.Text = ""
    End If
    If Mid(My.Application.Info.Version.ToString, 1, 1) <> 0 Then
      grpMessage.Text += "Dit is: NuCalc versie " & My.Application.Info.Version.ToString & "."
    Else
      grpMessage.Text += "Dit is: NuCalc Bèta versie " & My.Application.Info.Version.ToString & "."
    End If
  End Sub

  Public Sub CreateLogs()
    Dim bytKey() As Byte = _Cryptography.CreateKey(My.Resources.Password)
    Dim bytIV() As Byte = _Cryptography.CreateIV(My.Resources.Password)

    My.Computer.FileSystem.CreateDirectory(clsAppInfo.AppTemp)
    My.Computer.FileSystem.WriteAllText(clsAppInfo.AppTemp & "errorReport", My.Settings.ErrorReport, False)
    _Cryptography.EncryptDecryptFile(clsAppInfo.AppTemp & "errorReport", clsAppInfo.AppDocuments & "ErrorReport", bytKey, bytIV, clsCryptography.CryptoAction.ActionEncrypt)
    My.Computer.FileSystem.DeleteDirectory(clsAppInfo.AppTemp, FileIO.DeleteDirectoryOption.DeleteAllContents)
    UseWaitCursor = False
  End Sub
#End Region

#Region "Browser"
  Private Sub webInfo_DocumentCompleted(sender As Object, e As WebBrowserDocumentCompletedEventArgs) Handles webInfo.DocumentCompleted
    Me.webInfo.Document.Body.Style = "zoom: 80%"
  End Sub

  Private Sub cmdCalcRules_Click(sender As Object, e As EventArgs) Handles cmdCalcRules.Click
    cmdCalcRules.Enabled = False
    frmCalcRules.Show()
  End Sub
#End Region

#Region "Sounds"
  Private Sub PlayClickSound(sender As Object, e As EventArgs) Handles cmdCalcRules.Click, cmdClear.Click, cmdNewUser.Click, cmdShowEvaluations.Click, cmdStart.Click, _
      cboExcerciseType.SelectedIndexChanged
    If _blnHasStoppedLoading Then
      My.Computer.Audio.Play(My.Resources.SoundClick, AudioPlayMode.Background)
    End If
  End Sub

  Private Sub TuneWhenClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
    Me.Hide()
    My.Computer.Audio.Play(My.Resources.SoundClosing, AudioPlayMode.WaitToComplete)
    My.Settings.LastExcerciseType = cboExcerciseType.Text
  End Sub
#End Region

#Region "ComboBox"
  Private Sub cboExcerciseType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboExcerciseType.SelectedIndexChanged
    LayoutForEachType()
  End Sub

#Region "Methods"
  Private Sub LayoutForEachType()
    'Select Case cboExcerciseType.SelectedIndex
    '  Case 0
    '    MinMaxOne()
    '    cmdStart.Enabled = True
    '    txtMinOutput.Enabled = True
    '    chkNoZero.Enabled = False
    '    _intHighestQuantity = 5
    '    _strType = " + "
    '  Case 1
    '    MinMaxOne()
    '    cmdStart.Enabled = True
    '    txtMinOutput.Enabled = True
    '    chkNoZero.Enabled = False
    '    _intHighestQuantity = 3
    '    _strType = " - "
    '  Case 2
    '    MinMaxOne(True)
    '    cmdStart.Enabled = True
    '    txtMinOutput.Enabled = True
    '    chkNoZero.Enabled = True
    '    _intHighestQuantity = 4
    '    _strType = " x "
    '  Case 3
    '    MinMaxOne()
    '    cmdStart.Enabled = True
    '    txtMinOutput.Enabled = False
    '    chkNoZero.Enabled = False
    '    _intHighestQuantity = 3
    '    _strType = " : "
    '  Case 4
    '    MinMaxOne(True)
    '    cmdStart.Enabled = False
    '    txtMinOutput.Enabled = True
    '    txtMaxOutput.Enabled = True
    '    chkNoZero.Enabled = True
    '    _intHighestQuantity = 0
    '    _strType = " + en - "
    '  Case 5
    '    MinMaxOne(True)
    '    cmdStart.Enabled = False
    '    txtMinOutput.Enabled = True
    '    txtMaxOutput.Enabled = True
    '    chkNoZero.Enabled = True
    '    _intHighestQuantity = 0
    '    _strType = " x en : "
    '  Case 6
    '    MinMaxOne(True)
    '    cmdStart.Enabled = False
    '    txtMinOutput.Enabled = True
    '    txtMaxOutput.Enabled = True
    '    chkNoZero.Enabled = True
    '    _intHighestQuantity = 0
    '    _strType = " All "
    'End Select
  End Sub

  Private Sub MinMaxOne(Optional ByVal blnEnable As Boolean = False)
    If blnEnable = True Then
      lblMinOne.Enabled = True
      lblMaxOne.Enabled = True
      txtMinOne.Enabled = True
      txtMaxOne.Enabled = True
      _strEasyToolTip = "- Maximale uitkomst = 20" & vbCrLf & _
                      "- Minimale uitkomst = 0" & vbCrLf & _
                      "- Aantal keer 1 (maximum) = 20 (%)" & vbCrLf & _
                      "- Aantal keer 1 (minimum) = 10 (%)" & vbCrLf & _
                      "- Aantal getallen (of factoren) = 2"
      _strMediumToolTip = "- Maximale uitkomst = 100" & vbCrLf & _
                      "- Minimale uitkomst = 10" & vbCrLf & _
                      "- Aantal keer 1 (maximum) = 10 (%)" & vbCrLf & _
                      "- Aantal keer 1 (minimum) = 5 (%)" & vbCrLf & _
                      "- Aantal getallen (of factoren) = 3"
      _strHardToolTip = "- Maximale uitkomst = 1000" & vbCrLf & _
                      "- Minimale uitkomst = 100" & vbCrLf & _
                      "- Aantal keer 1 (maximum) = 5 (%)" & vbCrLf & _
                      "- Aantal keer 1 (minimum) = 0 (%)" & vbCrLf & _
                      "- Aantal getallen (of factoren) = 3"
    Else
      lblMinOne.Enabled = False
      lblMaxOne.Enabled = False
      txtMinOne.Enabled = False
      txtMaxOne.Enabled = False
      _strEasyToolTip = "- Maximale uitkomst = 20" & vbCrLf & _
                      "- Minimale uitkomst = 0" & vbCrLf & _
                      "- Aantal getallen (of factoren) = 2"
      _strMediumToolTip = "- Maximale uitkomst = 100" & vbCrLf & _
                      "- Minimale uitkomst = 10" & vbCrLf & _
                      "- Aantal getallen (of factoren) = 3"
      _strHardToolTip = "- Maximale uitkomst = 1000" & vbCrLf & _
                      "- Minimale uitkomst = 100" & vbCrLf & _
                      "- Aantal getallen (of factoren) = 3"
    End If
  End Sub
#End Region
#End Region

#Region "ToolTips"
  Private Sub Control_MouseEnter(sender As Object, e As EventArgs) Handles txtUsername.MouseEnter, txtQuantityOfNums.MouseEnter, txtMinZero.MouseEnter, txtMinOutput.MouseEnter, _
  txtMinOne.MouseEnter, txtMaxZero.MouseEnter, txtMaxOutput.MouseEnter, txtMaxOne.MouseEnter, txtAge.MouseEnter, rdoMedium.MouseEnter, rdoHard.MouseEnter, rdoEasy.MouseEnter, _
  lblUsername.MouseEnter, lblQuantityOfNums.MouseEnter, lblMinZero.MouseEnter, lblMinOutput.MouseEnter, lblMinOne.MouseEnter, lblMaxZero.MouseEnter, lblMaxOutput.MouseEnter, _
  lblMaxOne.MouseEnter, lblAge.MouseEnter, grpDifficulty.MouseEnter, grpAdvanced.MouseEnter, cmdStart.MouseEnter, cmdShowEvaluations.MouseEnter, cmdNewUser.MouseEnter, _
  cmdDeleteEvaluation.MouseEnter, cmdClear.MouseEnter, cmdCalcRules.MouseEnter, cmdManageMem.MouseEnter, chkNoZero.MouseEnter, chkHulp.MouseEnter, chkComma.MouseEnter, chkBrackets.MouseEnter
    If _blnActivateToolTips = True Then
      'Zoek op naam
      Select Case sender.Name
        'RadioButton
        Case rdoEasy.Name
          grpMessage.Text = _strEasyToolTip
        Case rdoMedium.Name
          grpMessage.Text = _strMediumToolTip
        Case rdoHard.Name
          grpMessage.Text = _strHardToolTip
          'Button
        Case cmdStart.Name
          grpMessage.Text = "Ik wens je veel succes!"
        Case cmdDeleteEvaluation.Name
          grpMessage.Text = "Dit verwijdert alle prestaties."
        Case cmdShowEvaluations.Name
          grpMessage.Text = "Dit toont alle prestaties sinds de eerste reeks oefeningen."
        Case cmdClear.Name
          grpMessage.Text = "Dit maakt alle instellingen ongedaan."
        Case cmdManageMem.Name
          grpMessage.Text = "Beheer het geheugen van " & My.Application.Info.ProductName & "."
        Case cmdCalcRules.Name
          grpMessage.Text = "Dit geeft een venster weer met alle rekenregels die gekend moeten zijn tot op het moeilijkste niveau."
        Case cmdNewUser.Name
          grpMessage.Text = "Als je een nieuwe gebruiker bent kan je registreren door naam en leeftijd in te vullen."
          'CheckBox
        Case chkHulp.Name
          grpMessage.Text = "Dit schakelt alle tooltips uit."
        Case chkNoZero.Name
          grpMessage.Text = "Dit zorgt ervoor dat er geen nullen voorkomen in de reeks oefeningen."
        Case chkBrackets.Name
          grpMessage.Text = "Dit laat het programma gebruik maken van haken."
        Case chkComma.Name
          grpMessage.Text = "Dit laat het programma gebruik maken van kommagetallen."
        Case Else
          'Zoek op eigenschap
          Select Case True
            Case InStr(sender.Name, "MaxOutput")
              grpMessage.Text = "De hoogste uitkomst mogelijk kan je hier invoeren."
            Case InStr(sender.Name, "MinOutput")
              grpMessage.Text = "De kleinste uitkomst mogelijk kan je hier invoeren."
            Case InStr(sender.Name, "MaxOne")
              grpMessage.Text = "Maximaal aantal keer 1 als getal (of factor) in een reeks oefeningen."
            Case InStr(sender.Name, "MinOne")
              grpMessage.Text = "Minimaal aantal keer 1 als getal (of factor) in een reeks oefeningen."
            Case InStr(sender.Name, "MaxZero")
              grpMessage.Text = "Maximaal aantal keer 0 als getal (of factor) in een reeks oefeningen."
            Case InStr(sender.Name, "MinZero")
              grpMessage.Text = "Minimaal aantal keer 0 als getal (of factor) in een reeks oefeningen."
            Case InStr(sender.Name, "QuantityOfNums")
              grpMessage.Text = "Het aantal getallen (of factoren) in een reeks oefeningen."
            Case InStr(sender.Name, "Username")
              grpMessage.Text = "Als je een nieuwe gebruiker bent kan je hier je naam invullen en druk je vervolgens op Nieuwe gebruiker."
            Case InStr(sender.Name, "Age")
              grpMessage.Text = "Als je een nieuwe gebruiker bent kan je hier je leeftijd  invullen en druk je vervolgens op Nieuwe gebruiker."
            Case Else
              WriteVersion()
          End Select
      End Select
    End If
  End Sub

  Private Sub Control_MouseLeave(sender As Object, e As EventArgs) Handles txtUsername.MouseLeave, txtQuantityOfNums.MouseLeave, txtMinZero.MouseLeave, txtMinOutput.MouseLeave, _
      txtMinOne.MouseLeave, txtMaxZero.MouseLeave, txtMaxOutput.MouseLeave, txtMaxOne.MouseLeave, txtAge.MouseLeave, rdoMedium.MouseLeave, rdoHard.MouseLeave, rdoEasy.MouseLeave, _
      lblUsername.MouseLeave, lblQuantityOfNums.MouseLeave, lblMinZero.MouseLeave, lblMinOutput.MouseLeave, lblMinOne.MouseLeave, lblMaxZero.MouseLeave, lblMaxOutput.MouseLeave, _
      lblMaxOne.MouseLeave, lblAge.MouseLeave, grpDifficulty.MouseLeave, grpAdvanced.MouseLeave, cmdStart.MouseLeave, cmdShowEvaluations.MouseLeave, cmdNewUser.MouseLeave, _
      cmdDeleteEvaluation.MouseLeave, cmdClear.MouseLeave, cmdCalcRules.MouseLeave, cmdManageMem.MouseLeave, chkNoZero.MouseLeave, chkHulp.MouseLeave, chkComma.MouseLeave, chkBrackets.MouseLeave
    WriteVersion()
  End Sub
#End Region
End Class