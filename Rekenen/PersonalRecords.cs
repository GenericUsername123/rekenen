﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using Rekenen.Properties;

namespace Rekenen
{
  public static class PersonalRecords
  {
    public static bool Abort;

    public static readonly Dictionary<string, PersonalRecord> Records = new Dictionary<string, PersonalRecord>();

    public static void LoadRecords()
    {
      if (!File.Exists(Helper.RecordFile)) return;
      try
      {
        using (var rijndael = new RijndaelManaged())
        {
          using (var hash = new Rfc2898DeriveBytes(Helper.Password, Encoding.Unicode.GetBytes(Helper.Salt)))
          {
            rijndael.Key = hash.GetBytes(rijndael.KeySize / 8);
            rijndael.IV = hash.GetBytes(rijndael.BlockSize / 8);
            var ms = new MemoryStream(File.ReadAllBytes(Helper.RecordFile));
            var cs = new CryptoStream(ms, rijndael.CreateDecryptor(), CryptoStreamMode.Read);
            using (var streamReader = new StreamReader(cs))
            {
              PersonalRecord.FromString(streamReader.ReadToEnd());
            }
          }
        }
      }
      catch
      {
        Abort = true;
        MessageBox.Show(Resources.PersonalRecords_LoadRecordsFailed.Replace("{0}", Helper.RecordFile),
          Resources.PersonalRecords_InvalidStats, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    public static void SaveRecords()
    {
      if (Abort) return;
      using (var rijndeal = new RijndaelManaged())
      {
        using (var hash = new Rfc2898DeriveBytes(Helper.Password, Encoding.Unicode.GetBytes(Helper.Salt)))
        {
          rijndeal.Key = hash.GetBytes(rijndeal.KeySize / 8);
          rijndeal.IV = hash.GetBytes(rijndeal.BlockSize / 8);
          var ms = new MemoryStream();
          var cs = new CryptoStream(ms, rijndeal.CreateEncryptor(), CryptoStreamMode.Write);
          using (var sw = new StreamWriter(cs))
          {
            sw.Write(Records.Aggregate("", (current, record) => current + (record.Key + '=' + record.Value + '\n')));
          }
          File.WriteAllBytes(Helper.RecordFile, ms.ToArray());
        }
      }
    }

    public sealed class PersonalRecord : IDisposable
    {
      public readonly Session.Session Setup = new Session.Session();
      private float _accuracy;
      public long TimeSpent;

      public PersonalRecord(string key)
      {
        if (key == null) return;
        var fileName = Helper.ConfigFolder + "\\" + key + ".cfg";
        if (!File.Exists(fileName))
        {
          Helper.Preset preset;
          if (Enum.TryParse(key, out preset)) Setup = Helper.GetSession(preset);
        }
        else Setup.LoadConfig(File.ReadAllLines(fileName));
      }

      public float Accuracy
      {
        get { return _accuracy; }
        set
        {
          value = Math.Abs(value);
          _accuracy = value > 1 ? 1 : value;
        }
      }

      public long Score => CalculateScore(Accuracy, TimeSpent);

      public void Dispose()
      {
        Setup.Dispose();
      }

      public static long CalculateScore(float accuracy, long timeSpent)
      {
        return (long)(accuracy / Math.Log10(timeSpent) * 1000);
      }

      public static void FromString(string contents)
      {
        var profiles = Helper.GetProfiles();
        foreach (var line in contents.Split('\n'))
        {
          if (line == "") break;
          var record = line.Split('=');
          if (Helper.Presets.All(config => config != record[0]) &&
              profiles.All(profile => profile != record[0])) continue;
          var values = record[1].Split(';');
          var value = new PersonalRecord(record[0]);
          float propVal;
          if (!long.TryParse(values[0], out value.TimeSpent) || !float.TryParse(values[1], out propVal))
          {
            Records.Clear();
            break;
          }
          value.Accuracy = propVal;
          if (Records.ContainsKey(record[0])) break;
          Records.Add(record[0], value);
        }

      }

      public override string ToString()
      {
        return TimeSpent + ";" + Accuracy;// + ";" + Setup;
      }
    }
  }
}