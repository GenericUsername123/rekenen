﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.cboExcerciseType = New System.Windows.Forms.ComboBox()
    Me.rdoHard = New System.Windows.Forms.RadioButton()
    Me.rdoMedium = New System.Windows.Forms.RadioButton()
    Me.rdoEasy = New System.Windows.Forms.RadioButton()
    Me.grpDifficulty = New System.Windows.Forms.GroupBox()
    Me.grpAdvanced = New System.Windows.Forms.GroupBox()
    Me.cmdManageMem = New System.Windows.Forms.Button()
    Me.chkBrackets = New System.Windows.Forms.CheckBox()
    Me.chkComma = New System.Windows.Forms.CheckBox()
    Me.txtMinZero = New System.Windows.Forms.TextBox()
    Me.lblMinZero = New System.Windows.Forms.Label()
    Me.txtMaxZero = New System.Windows.Forms.TextBox()
    Me.lblMaxZero = New System.Windows.Forms.Label()
    Me.chkHulp = New System.Windows.Forms.CheckBox()
    Me.txtQuantityOfNums = New System.Windows.Forms.TextBox()
    Me.lblQuantityOfNums = New System.Windows.Forms.Label()
    Me.chkNoZero = New System.Windows.Forms.CheckBox()
    Me.txtMinOne = New System.Windows.Forms.TextBox()
    Me.lblMinOne = New System.Windows.Forms.Label()
    Me.txtMaxOne = New System.Windows.Forms.TextBox()
    Me.lblMaxOne = New System.Windows.Forms.Label()
    Me.txtMinOutput = New System.Windows.Forms.TextBox()
    Me.lblMinOutput = New System.Windows.Forms.Label()
    Me.txtMaxOutput = New System.Windows.Forms.TextBox()
    Me.lblMaxOutput = New System.Windows.Forms.Label()
    Me.cmdClear = New System.Windows.Forms.Button()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.grpMessage = New System.Windows.Forms.GroupBox()
    Me.cmdNewUser = New System.Windows.Forms.Button()
    Me.lblUsername = New System.Windows.Forms.Label()
    Me.lblAge = New System.Windows.Forms.Label()
    Me.cmdDeleteEvaluation = New System.Windows.Forms.Button()
    Me.cmdShowEvaluations = New System.Windows.Forms.Button()
    Me.grpInfo = New System.Windows.Forms.GroupBox()
    Me.cmdCalcRules = New System.Windows.Forms.Button()
    Me.webInfo = New System.Windows.Forms.WebBrowser()
    Me.rtbEvaluation = New System.Windows.Forms.RichTextBox()
    Me.pnlChangeOfUser = New System.Windows.Forms.Panel()
    Me.txtAge = New System.Windows.Forms.TextBox()
    Me.txtUsername = New System.Windows.Forms.TextBox()
    Me.cmdStart = New System.Windows.Forms.Button()
    Me.grpConfig = New System.Windows.Forms.GroupBox()
    Me.tmrRefresh = New System.Windows.Forms.Timer(Me.components)
    Me.cmsNotifyStrip = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.BeheerHetGeheugenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.SluitenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.CheckBox1 = New System.Windows.Forms.CheckBox()
    Me.CheckBox2 = New System.Windows.Forms.CheckBox()
    Me.grpDifficulty.SuspendLayout()
    Me.grpAdvanced.SuspendLayout()
    Me.grpInfo.SuspendLayout()
    Me.pnlChangeOfUser.SuspendLayout()
    Me.grpConfig.SuspendLayout()
    Me.cmsNotifyStrip.SuspendLayout()
    Me.SuspendLayout()
    '
    'cboExcerciseType
    '
    Me.cboExcerciseType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
    Me.cboExcerciseType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboExcerciseType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cboExcerciseType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.cboExcerciseType.Items.AddRange(New Object() {"Som ( + )", "Verschil ( - )", "Product ( x )", "Quotiënt ( : )", "Mix som en verschil( + en - )", "Mix product en quotiënt ( x en : )", "Mix alles"})
    Me.cboExcerciseType.Location = New System.Drawing.Point(99, 26)
    Me.cboExcerciseType.Name = "cboExcerciseType"
    Me.cboExcerciseType.Size = New System.Drawing.Size(258, 21)
    Me.cboExcerciseType.TabIndex = 0
    '
    'rdoHard
    '
    Me.rdoHard.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.rdoHard.AutoSize = True
    Me.rdoHard.Cursor = System.Windows.Forms.Cursors.Hand
    Me.rdoHard.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.rdoHard.ForeColor = System.Drawing.SystemColors.ControlText
    Me.rdoHard.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.rdoHard.Location = New System.Drawing.Point(395, 21)
    Me.rdoHard.Name = "rdoHard"
    Me.rdoHard.Size = New System.Drawing.Size(60, 17)
    Me.rdoHard.TabIndex = 2
    Me.rdoHard.Text = "Moeilijk"
    Me.rdoHard.UseVisualStyleBackColor = True
    '
    'rdoMedium
    '
    Me.rdoMedium.Anchor = System.Windows.Forms.AnchorStyles.Top
    Me.rdoMedium.AutoSize = True
    Me.rdoMedium.Cursor = System.Windows.Forms.Cursors.Hand
    Me.rdoMedium.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.rdoMedium.ForeColor = System.Drawing.SystemColors.ControlText
    Me.rdoMedium.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.rdoMedium.Location = New System.Drawing.Point(202, 21)
    Me.rdoMedium.Name = "rdoMedium"
    Me.rdoMedium.Size = New System.Drawing.Size(75, 17)
    Me.rdoMedium.TabIndex = 1
    Me.rdoMedium.Text = "Gemiddeld"
    Me.rdoMedium.UseVisualStyleBackColor = True
    '
    'rdoEasy
    '
    Me.rdoEasy.AutoSize = True
    Me.rdoEasy.Checked = True
    Me.rdoEasy.Cursor = System.Windows.Forms.Cursors.Hand
    Me.rdoEasy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.rdoEasy.ForeColor = System.Drawing.SystemColors.ControlText
    Me.rdoEasy.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.rdoEasy.Location = New System.Drawing.Point(6, 21)
    Me.rdoEasy.Name = "rdoEasy"
    Me.rdoEasy.Size = New System.Drawing.Size(83, 17)
    Me.rdoEasy.TabIndex = 0
    Me.rdoEasy.TabStop = True
    Me.rdoEasy.Text = "Gemakkelijk"
    Me.rdoEasy.UseVisualStyleBackColor = True
    '
    'grpDifficulty
    '
    Me.grpDifficulty.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.grpDifficulty.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.grpDifficulty.BackColor = System.Drawing.Color.Transparent
    Me.grpDifficulty.Controls.Add(Me.rdoHard)
    Me.grpDifficulty.Controls.Add(Me.rdoMedium)
    Me.grpDifficulty.Controls.Add(Me.rdoEasy)
    Me.grpDifficulty.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.grpDifficulty.Location = New System.Drawing.Point(6, 60)
    Me.grpDifficulty.Name = "grpDifficulty"
    Me.grpDifficulty.Size = New System.Drawing.Size(461, 59)
    Me.grpDifficulty.TabIndex = 1
    Me.grpDifficulty.TabStop = False
    Me.grpDifficulty.Text = "Moeilijkheidsgraad"
    '
    'grpAdvanced
    '
    Me.grpAdvanced.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.grpAdvanced.BackColor = System.Drawing.Color.Transparent
    Me.grpAdvanced.Controls.Add(Me.CheckBox2)
    Me.grpAdvanced.Controls.Add(Me.CheckBox1)
    Me.grpAdvanced.Controls.Add(Me.cmdManageMem)
    Me.grpAdvanced.Controls.Add(Me.chkBrackets)
    Me.grpAdvanced.Controls.Add(Me.chkComma)
    Me.grpAdvanced.Controls.Add(Me.txtMinZero)
    Me.grpAdvanced.Controls.Add(Me.lblMinZero)
    Me.grpAdvanced.Controls.Add(Me.txtMaxZero)
    Me.grpAdvanced.Controls.Add(Me.lblMaxZero)
    Me.grpAdvanced.Controls.Add(Me.chkHulp)
    Me.grpAdvanced.Controls.Add(Me.txtQuantityOfNums)
    Me.grpAdvanced.Controls.Add(Me.lblQuantityOfNums)
    Me.grpAdvanced.Controls.Add(Me.chkNoZero)
    Me.grpAdvanced.Controls.Add(Me.txtMinOne)
    Me.grpAdvanced.Controls.Add(Me.lblMinOne)
    Me.grpAdvanced.Controls.Add(Me.txtMaxOne)
    Me.grpAdvanced.Controls.Add(Me.lblMaxOne)
    Me.grpAdvanced.Controls.Add(Me.txtMinOutput)
    Me.grpAdvanced.Controls.Add(Me.lblMinOutput)
    Me.grpAdvanced.Controls.Add(Me.txtMaxOutput)
    Me.grpAdvanced.Controls.Add(Me.lblMaxOutput)
    Me.grpAdvanced.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.grpAdvanced.Location = New System.Drawing.Point(6, 122)
    Me.grpAdvanced.Name = "grpAdvanced"
    Me.grpAdvanced.Size = New System.Drawing.Size(461, 236)
    Me.grpAdvanced.TabIndex = 2
    Me.grpAdvanced.TabStop = False
    Me.grpAdvanced.Text = "Geavanceerde instellingen"
    '
    'cmdManageMem
    '
    Me.cmdManageMem.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.cmdManageMem.AutoSize = True
    Me.cmdManageMem.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.cmdManageMem.BackColor = System.Drawing.Color.WhiteSmoke
    Me.cmdManageMem.Cursor = System.Windows.Forms.Cursors.Hand
    Me.cmdManageMem.FlatStyle = System.Windows.Forms.FlatStyle.Flat
    Me.cmdManageMem.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.cmdManageMem.ForeColor = System.Drawing.SystemColors.ControlText
    Me.cmdManageMem.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.cmdManageMem.Location = New System.Drawing.Point(288, 205)
    Me.cmdManageMem.Name = "cmdManageMem"
    Me.cmdManageMem.Size = New System.Drawing.Size(122, 25)
    Me.cmdManageMem.TabIndex = 49
    Me.cmdManageMem.Text = "Beheer het geheugen"
    Me.cmdManageMem.UseVisualStyleBackColor = False
    '
    'chkBrackets
    '
    Me.chkBrackets.AutoSize = True
    Me.chkBrackets.Cursor = System.Windows.Forms.Cursors.Hand
    Me.chkBrackets.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.chkBrackets.ForeColor = System.Drawing.SystemColors.ControlText
    Me.chkBrackets.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.chkBrackets.Location = New System.Drawing.Point(6, 145)
    Me.chkBrackets.Name = "chkBrackets"
    Me.chkBrackets.Size = New System.Drawing.Size(161, 17)
    Me.chkBrackets.TabIndex = 56
    Me.chkBrackets.Text = "Met haken in een bewerking"
    Me.chkBrackets.UseVisualStyleBackColor = True
    '
    'chkComma
    '
    Me.chkComma.AutoSize = True
    Me.chkComma.Cursor = System.Windows.Forms.Cursors.Hand
    Me.chkComma.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.chkComma.ForeColor = System.Drawing.SystemColors.ControlText
    Me.chkComma.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.chkComma.Location = New System.Drawing.Point(6, 190)
    Me.chkComma.Name = "chkComma"
    Me.chkComma.Size = New System.Drawing.Size(144, 17)
    Me.chkComma.TabIndex = 55
    Me.chkComma.Text = "Merkwaardige producten"
    Me.chkComma.UseVisualStyleBackColor = True
    '
    'txtMinZero
    '
    Me.txtMinZero.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.txtMinZero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.txtMinZero.Location = New System.Drawing.Point(430, 99)
    Me.txtMinZero.MaxLength = 3
    Me.txtMinZero.Name = "txtMinZero"
    Me.txtMinZero.Size = New System.Drawing.Size(25, 20)
    Me.txtMinZero.TabIndex = 52
    '
    'lblMinZero
    '
    Me.lblMinZero.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lblMinZero.AutoSize = True
    Me.lblMinZero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lblMinZero.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lblMinZero.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.lblMinZero.Location = New System.Drawing.Point(202, 102)
    Me.lblMinZero.Name = "lblMinZero"
    Me.lblMinZero.Size = New System.Drawing.Size(222, 13)
    Me.lblMinZero.TabIndex = 54
    Me.lblMinZero.Text = "Minimaal aantal keer 0 als getal in procent (%)"
    '
    'txtMaxZero
    '
    Me.txtMaxZero.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.txtMaxZero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.txtMaxZero.Location = New System.Drawing.Point(430, 73)
    Me.txtMaxZero.MaxLength = 3
    Me.txtMaxZero.Name = "txtMaxZero"
    Me.txtMaxZero.Size = New System.Drawing.Size(25, 20)
    Me.txtMaxZero.TabIndex = 51
    '
    'lblMaxZero
    '
    Me.lblMaxZero.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lblMaxZero.AutoSize = True
    Me.lblMaxZero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lblMaxZero.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lblMaxZero.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.lblMaxZero.Location = New System.Drawing.Point(199, 76)
    Me.lblMaxZero.Name = "lblMaxZero"
    Me.lblMaxZero.Size = New System.Drawing.Size(225, 13)
    Me.lblMaxZero.TabIndex = 53
    Me.lblMaxZero.Text = "Maximaal aantal keer 0 als getal in procent (%)"
    '
    'chkHulp
    '
    Me.chkHulp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.chkHulp.Appearance = System.Windows.Forms.Appearance.Button
    Me.chkHulp.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(150, Byte), Integer), CType(CType(255, Byte), Integer))
    Me.chkHulp.Checked = Global.RFP_VB.My.MySettings.Default.Hulp
    Me.chkHulp.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chkHulp.Cursor = System.Windows.Forms.Cursors.Hand
    Me.chkHulp.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Global.RFP_VB.My.MySettings.Default, "Hulp", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
    Me.chkHulp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
    Me.chkHulp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.chkHulp.ForeColor = System.Drawing.SystemColors.ControlText
    Me.chkHulp.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.chkHulp.Location = New System.Drawing.Point(416, 205)
    Me.chkHulp.Name = "chkHulp"
    Me.chkHulp.Size = New System.Drawing.Size(39, 25)
    Me.chkHulp.TabIndex = 50
    Me.chkHulp.Text = "Hulp"
    Me.chkHulp.UseVisualStyleBackColor = False
    '
    'txtQuantityOfNums
    '
    Me.txtQuantityOfNums.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.txtQuantityOfNums.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.txtQuantityOfNums.Location = New System.Drawing.Point(430, 165)
    Me.txtQuantityOfNums.MaxLength = 1
    Me.txtQuantityOfNums.Name = "txtQuantityOfNums"
    Me.txtQuantityOfNums.Size = New System.Drawing.Size(25, 20)
    Me.txtQuantityOfNums.TabIndex = 5
    Me.txtQuantityOfNums.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'lblQuantityOfNums
    '
    Me.lblQuantityOfNums.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lblQuantityOfNums.AutoSize = True
    Me.lblQuantityOfNums.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lblQuantityOfNums.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lblQuantityOfNums.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.lblQuantityOfNums.Location = New System.Drawing.Point(271, 168)
    Me.lblQuantityOfNums.Name = "lblQuantityOfNums"
    Me.lblQuantityOfNums.Size = New System.Drawing.Size(153, 13)
    Me.lblQuantityOfNums.TabIndex = 49
    Me.lblQuantityOfNums.Text = "Aantal getallen in een oefening"
    '
    'chkNoZero
    '
    Me.chkNoZero.AutoSize = True
    Me.chkNoZero.Cursor = System.Windows.Forms.Cursors.Hand
    Me.chkNoZero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.chkNoZero.ForeColor = System.Drawing.SystemColors.ControlText
    Me.chkNoZero.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.chkNoZero.Location = New System.Drawing.Point(6, 122)
    Me.chkNoZero.Name = "chkNoZero"
    Me.chkNoZero.Size = New System.Drawing.Size(154, 17)
    Me.chkNoZero.TabIndex = 4
    Me.chkNoZero.Text = "Alle oefeningen zonder nul."
    Me.chkNoZero.UseVisualStyleBackColor = True
    '
    'txtMinOne
    '
    Me.txtMinOne.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.txtMinOne.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.txtMinOne.Location = New System.Drawing.Point(430, 47)
    Me.txtMinOne.MaxLength = 3
    Me.txtMinOne.Name = "txtMinOne"
    Me.txtMinOne.Size = New System.Drawing.Size(25, 20)
    Me.txtMinOne.TabIndex = 3
    '
    'lblMinOne
    '
    Me.lblMinOne.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lblMinOne.AutoSize = True
    Me.lblMinOne.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lblMinOne.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lblMinOne.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.lblMinOne.Location = New System.Drawing.Point(202, 50)
    Me.lblMinOne.Name = "lblMinOne"
    Me.lblMinOne.Size = New System.Drawing.Size(222, 13)
    Me.lblMinOne.TabIndex = 48
    Me.lblMinOne.Text = "Minimaal aantal keer 1 als getal in procent (%)"
    '
    'txtMaxOne
    '
    Me.txtMaxOne.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.txtMaxOne.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.txtMaxOne.Location = New System.Drawing.Point(430, 21)
    Me.txtMaxOne.MaxLength = 3
    Me.txtMaxOne.Name = "txtMaxOne"
    Me.txtMaxOne.Size = New System.Drawing.Size(25, 20)
    Me.txtMaxOne.TabIndex = 2
    '
    'lblMaxOne
    '
    Me.lblMaxOne.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lblMaxOne.AutoSize = True
    Me.lblMaxOne.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lblMaxOne.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lblMaxOne.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.lblMaxOne.Location = New System.Drawing.Point(202, 28)
    Me.lblMaxOne.Name = "lblMaxOne"
    Me.lblMaxOne.Size = New System.Drawing.Size(225, 13)
    Me.lblMaxOne.TabIndex = 47
    Me.lblMaxOne.Text = "Maximaal aantal keer 1 als getal in procent (%)"
    '
    'txtMinOutput
    '
    Me.txtMinOutput.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.txtMinOutput.Location = New System.Drawing.Point(105, 47)
    Me.txtMinOutput.MaxLength = 4
    Me.txtMinOutput.Name = "txtMinOutput"
    Me.txtMinOutput.Size = New System.Drawing.Size(31, 20)
    Me.txtMinOutput.TabIndex = 1
    '
    'lblMinOutput
    '
    Me.lblMinOutput.AutoSize = True
    Me.lblMinOutput.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lblMinOutput.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lblMinOutput.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.lblMinOutput.Location = New System.Drawing.Point(9, 50)
    Me.lblMinOutput.Name = "lblMinOutput"
    Me.lblMinOutput.Size = New System.Drawing.Size(90, 13)
    Me.lblMinOutput.TabIndex = 45
    Me.lblMinOutput.Text = "Minimale uitkomst"
    '
    'txtMaxOutput
    '
    Me.txtMaxOutput.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.txtMaxOutput.Location = New System.Drawing.Point(105, 21)
    Me.txtMaxOutput.MaxLength = 4
    Me.txtMaxOutput.Name = "txtMaxOutput"
    Me.txtMaxOutput.Size = New System.Drawing.Size(31, 20)
    Me.txtMaxOutput.TabIndex = 0
    '
    'lblMaxOutput
    '
    Me.lblMaxOutput.AutoSize = True
    Me.lblMaxOutput.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lblMaxOutput.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lblMaxOutput.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.lblMaxOutput.Location = New System.Drawing.Point(6, 24)
    Me.lblMaxOutput.Name = "lblMaxOutput"
    Me.lblMaxOutput.Size = New System.Drawing.Size(93, 13)
    Me.lblMaxOutput.TabIndex = 42
    Me.lblMaxOutput.Text = "Maximale uitkomst"
    '
    'cmdClear
    '
    Me.cmdClear.AutoSize = True
    Me.cmdClear.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.cmdClear.BackColor = System.Drawing.Color.Transparent
    Me.cmdClear.Cursor = System.Windows.Forms.Cursors.Hand
    Me.cmdClear.Enabled = False
    Me.cmdClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
    Me.cmdClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.cmdClear.ForeColor = System.Drawing.SystemColors.ControlText
    Me.cmdClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.cmdClear.Location = New System.Drawing.Point(363, 23)
    Me.cmdClear.Name = "cmdClear"
    Me.cmdClear.Size = New System.Drawing.Size(104, 25)
    Me.cmdClear.TabIndex = 3
    Me.cmdClear.Text = "Ongedaan maken"
    Me.cmdClear.UseVisualStyleBackColor = False
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.75!)
    Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
    Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.Label1.Location = New System.Drawing.Point(6, 27)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(87, 15)
    Me.Label1.TabIndex = 37
    Me.Label1.Text = "Type oefening:"
    Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'grpMessage
    '
    Me.grpMessage.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.grpMessage.BackColor = System.Drawing.Color.Transparent
    Me.grpMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.grpMessage.Location = New System.Drawing.Point(12, 43)
    Me.grpMessage.MaximumSize = New System.Drawing.Size(500, 40)
    Me.grpMessage.MinimumSize = New System.Drawing.Size(185, 40)
    Me.grpMessage.Name = "grpMessage"
    Me.grpMessage.Size = New System.Drawing.Size(266, 40)
    Me.grpMessage.TabIndex = 999
    Me.grpMessage.TabStop = False
    Me.grpMessage.Text = "Welkom!"
    '
    'cmdNewUser
    '
    Me.cmdNewUser.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.cmdNewUser.AutoSize = True
    Me.cmdNewUser.BackColor = System.Drawing.Color.WhiteSmoke
    Me.cmdNewUser.Cursor = System.Windows.Forms.Cursors.Hand
    Me.cmdNewUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat
    Me.cmdNewUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.cmdNewUser.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.cmdNewUser.Location = New System.Drawing.Point(366, 3)
    Me.cmdNewUser.Name = "cmdNewUser"
    Me.cmdNewUser.Size = New System.Drawing.Size(104, 25)
    Me.cmdNewUser.TabIndex = 2
    Me.cmdNewUser.Text = "Nieuwe gebruiker"
    Me.cmdNewUser.UseVisualStyleBackColor = False
    '
    'lblUsername
    '
    Me.lblUsername.AutoSize = True
    Me.lblUsername.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.75!)
    Me.lblUsername.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lblUsername.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.lblUsername.Location = New System.Drawing.Point(4, 7)
    Me.lblUsername.Name = "lblUsername"
    Me.lblUsername.Size = New System.Drawing.Size(102, 15)
    Me.lblUsername.TabIndex = 32
    Me.lblUsername.Text = "Gebruikersnaam:"
    Me.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lblAge
    '
    Me.lblAge.Anchor = System.Windows.Forms.AnchorStyles.Bottom
    Me.lblAge.AutoSize = True
    Me.lblAge.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.75!)
    Me.lblAge.ForeColor = System.Drawing.SystemColors.ControlText
    Me.lblAge.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.lblAge.Location = New System.Drawing.Point(272, 7)
    Me.lblAge.Name = "lblAge"
    Me.lblAge.Size = New System.Drawing.Size(50, 15)
    Me.lblAge.TabIndex = 36
    Me.lblAge.Text = "Leeftijd:"
    Me.lblAge.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'cmdDeleteEvaluation
    '
    Me.cmdDeleteEvaluation.AutoSize = True
    Me.cmdDeleteEvaluation.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.cmdDeleteEvaluation.BackColor = System.Drawing.Color.WhiteSmoke
    Me.cmdDeleteEvaluation.Cursor = System.Windows.Forms.Cursors.Hand
    Me.cmdDeleteEvaluation.Enabled = False
    Me.cmdDeleteEvaluation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
    Me.cmdDeleteEvaluation.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.cmdDeleteEvaluation.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.cmdDeleteEvaluation.Location = New System.Drawing.Point(284, 58)
    Me.cmdDeleteEvaluation.Name = "cmdDeleteEvaluation"
    Me.cmdDeleteEvaluation.Size = New System.Drawing.Size(100, 25)
    Me.cmdDeleteEvaluation.TabIndex = 4
    Me.cmdDeleteEvaluation.Text = "Prestaties wissen"
    Me.cmdDeleteEvaluation.UseVisualStyleBackColor = False
    '
    'cmdShowEvaluations
    '
    Me.cmdShowEvaluations.AutoSize = True
    Me.cmdShowEvaluations.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.cmdShowEvaluations.BackColor = System.Drawing.Color.WhiteSmoke
    Me.cmdShowEvaluations.Cursor = System.Windows.Forms.Cursors.Hand
    Me.cmdShowEvaluations.Enabled = False
    Me.cmdShowEvaluations.FlatStyle = System.Windows.Forms.FlatStyle.Flat
    Me.cmdShowEvaluations.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.cmdShowEvaluations.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.cmdShowEvaluations.Location = New System.Drawing.Point(390, 58)
    Me.cmdShowEvaluations.Name = "cmdShowEvaluations"
    Me.cmdShowEvaluations.Size = New System.Drawing.Size(95, 25)
    Me.cmdShowEvaluations.TabIndex = 3
    Me.cmdShowEvaluations.Text = "Prestaties tonen"
    Me.cmdShowEvaluations.UseVisualStyleBackColor = False
    '
    'grpInfo
    '
    Me.grpInfo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.grpInfo.BackColor = System.Drawing.Color.Transparent
    Me.grpInfo.Controls.Add(Me.cmdCalcRules)
    Me.grpInfo.Controls.Add(Me.webInfo)
    Me.grpInfo.Controls.Add(Me.rtbEvaluation)
    Me.grpInfo.Cursor = System.Windows.Forms.Cursors.Default
    Me.grpInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
    Me.grpInfo.ForeColor = System.Drawing.SystemColors.ControlDarkDark
    Me.grpInfo.Location = New System.Drawing.Point(491, 12)
    Me.grpInfo.Name = "grpInfo"
    Me.grpInfo.Size = New System.Drawing.Size(441, 478)
    Me.grpInfo.TabIndex = 5
    Me.grpInfo.TabStop = False
    Me.grpInfo.Text = "Info"
    '
    'cmdCalcRules
    '
    Me.cmdCalcRules.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.cmdCalcRules.BackColor = System.Drawing.Color.WhiteSmoke
    Me.cmdCalcRules.Cursor = System.Windows.Forms.Cursors.Hand
    Me.cmdCalcRules.FlatStyle = System.Windows.Forms.FlatStyle.Flat
    Me.cmdCalcRules.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.cmdCalcRules.ForeColor = System.Drawing.SystemColors.ControlText
    Me.cmdCalcRules.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.cmdCalcRules.Location = New System.Drawing.Point(359, 20)
    Me.cmdCalcRules.Name = "cmdCalcRules"
    Me.cmdCalcRules.Size = New System.Drawing.Size(79, 24)
    Me.cmdCalcRules.TabIndex = 0
    Me.cmdCalcRules.Text = "Rekenregels"
    Me.cmdCalcRules.UseVisualStyleBackColor = False
    '
    'webInfo
    '
    Me.webInfo.Dock = System.Windows.Forms.DockStyle.Fill
    Me.webInfo.Location = New System.Drawing.Point(3, 20)
    Me.webInfo.MinimumSize = New System.Drawing.Size(20, 20)
    Me.webInfo.Name = "webInfo"
    Me.webInfo.ScrollBarsEnabled = False
    Me.webInfo.Size = New System.Drawing.Size(435, 455)
    Me.webInfo.TabIndex = 19
    Me.webInfo.Url = New System.Uri("", System.UriKind.Relative)
    '
    'rtbEvaluation
    '
    Me.rtbEvaluation.BackColor = System.Drawing.Color.AliceBlue
    Me.rtbEvaluation.Dock = System.Windows.Forms.DockStyle.Fill
    Me.rtbEvaluation.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.rtbEvaluation.ForeColor = System.Drawing.Color.Black
    Me.rtbEvaluation.Location = New System.Drawing.Point(3, 20)
    Me.rtbEvaluation.Name = "rtbEvaluation"
    Me.rtbEvaluation.ReadOnly = True
    Me.rtbEvaluation.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
    Me.rtbEvaluation.Size = New System.Drawing.Size(435, 455)
    Me.rtbEvaluation.TabIndex = 18
    Me.rtbEvaluation.TabStop = False
    Me.rtbEvaluation.Text = "Correcties komen hier te staan..."
    Me.rtbEvaluation.Visible = False
    '
    'pnlChangeOfUser
    '
    Me.pnlChangeOfUser.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.pnlChangeOfUser.BackColor = System.Drawing.Color.Transparent
    Me.pnlChangeOfUser.Controls.Add(Me.txtAge)
    Me.pnlChangeOfUser.Controls.Add(Me.cmdNewUser)
    Me.pnlChangeOfUser.Controls.Add(Me.txtUsername)
    Me.pnlChangeOfUser.Controls.Add(Me.lblUsername)
    Me.pnlChangeOfUser.Controls.Add(Me.lblAge)
    Me.pnlChangeOfUser.Location = New System.Drawing.Point(12, 459)
    Me.pnlChangeOfUser.Name = "pnlChangeOfUser"
    Me.pnlChangeOfUser.Size = New System.Drawing.Size(473, 31)
    Me.pnlChangeOfUser.TabIndex = 2
    '
    'txtAge
    '
    Me.txtAge.Anchor = System.Windows.Forms.AnchorStyles.Bottom
    Me.txtAge.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.RFP_VB.My.MySettings.Default, "Age", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
    Me.txtAge.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.txtAge.Location = New System.Drawing.Point(328, 6)
    Me.txtAge.MaxLength = 2
    Me.txtAge.Name = "txtAge"
    Me.txtAge.Size = New System.Drawing.Size(21, 20)
    Me.txtAge.TabIndex = 1
    Me.txtAge.Text = "0"
    '
    'txtUsername
    '
    Me.txtUsername.AutoCompleteCustomSource.AddRange(New String() {"Anoniem"})
    Me.txtUsername.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
    Me.txtUsername.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
    Me.txtUsername.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.RFP_VB.My.MySettings.Default, "Username", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
    Me.txtUsername.ImeMode = System.Windows.Forms.ImeMode.Off
    Me.txtUsername.Location = New System.Drawing.Point(112, 6)
    Me.txtUsername.MaxLength = 20
    Me.txtUsername.Name = "txtUsername"
    Me.txtUsername.Size = New System.Drawing.Size(154, 20)
    Me.txtUsername.TabIndex = 0
    Me.txtUsername.Text = Global.RFP_VB.My.MySettings.Default.Username
    '
    'cmdStart
    '
    Me.cmdStart.AccessibleName = ""
    Me.cmdStart.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.cmdStart.BackColor = System.Drawing.Color.WhiteSmoke
    Me.cmdStart.Cursor = System.Windows.Forms.Cursors.Hand
    Me.cmdStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat
    Me.cmdStart.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.cmdStart.ForeColor = System.Drawing.SystemColors.ControlText
    Me.cmdStart.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.cmdStart.Location = New System.Drawing.Point(12, 12)
    Me.cmdStart.Name = "cmdStart"
    Me.cmdStart.Size = New System.Drawing.Size(473, 25)
    Me.cmdStart.TabIndex = 0
    Me.cmdStart.Text = "Start"
    Me.cmdStart.UseVisualStyleBackColor = False
    '
    'grpConfig
    '
    Me.grpConfig.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.grpConfig.BackColor = System.Drawing.Color.Transparent
    Me.grpConfig.Controls.Add(Me.grpAdvanced)
    Me.grpConfig.Controls.Add(Me.grpDifficulty)
    Me.grpConfig.Controls.Add(Me.cmdClear)
    Me.grpConfig.Controls.Add(Me.Label1)
    Me.grpConfig.Controls.Add(Me.cboExcerciseType)
    Me.grpConfig.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.grpConfig.ForeColor = System.Drawing.SystemColors.ControlDarkDark
    Me.grpConfig.Location = New System.Drawing.Point(12, 89)
    Me.grpConfig.Name = "grpConfig"
    Me.grpConfig.Size = New System.Drawing.Size(473, 364)
    Me.grpConfig.TabIndex = 1
    Me.grpConfig.TabStop = False
    Me.grpConfig.Text = "Configuratie"
    '
    'tmrRefresh
    '
    Me.tmrRefresh.Enabled = True
    Me.tmrRefresh.Interval = 500
    '
    'cmsNotifyStrip
    '
    Me.cmsNotifyStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BeheerHetGeheugenToolStripMenuItem, Me.SluitenToolStripMenuItem})
    Me.cmsNotifyStrip.Name = "cmsNotifyStrip"
    Me.cmsNotifyStrip.Size = New System.Drawing.Size(187, 48)
    '
    'BeheerHetGeheugenToolStripMenuItem
    '
    Me.BeheerHetGeheugenToolStripMenuItem.Name = "BeheerHetGeheugenToolStripMenuItem"
    Me.BeheerHetGeheugenToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
    Me.BeheerHetGeheugenToolStripMenuItem.Text = "Beheer het geheugen"
    '
    'SluitenToolStripMenuItem
    '
    Me.SluitenToolStripMenuItem.Name = "SluitenToolStripMenuItem"
    Me.SluitenToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
    Me.SluitenToolStripMenuItem.Text = "Sluiten"
    '
    'CheckBox1
    '
    Me.CheckBox1.AutoSize = True
    Me.CheckBox1.Cursor = System.Windows.Forms.Cursors.Hand
    Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.CheckBox1.ForeColor = System.Drawing.SystemColors.ControlText
    Me.CheckBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.CheckBox1.Location = New System.Drawing.Point(6, 167)
    Me.CheckBox1.Name = "CheckBox1"
    Me.CheckBox1.Size = New System.Drawing.Size(116, 17)
    Me.CheckBox1.TabIndex = 57
    Me.CheckBox1.Text = "Met kommagetalen"
    Me.CheckBox1.UseVisualStyleBackColor = True
    '
    'CheckBox2
    '
    Me.CheckBox2.AutoSize = True
    Me.CheckBox2.Cursor = System.Windows.Forms.Cursors.Hand
    Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.CheckBox2.ForeColor = System.Drawing.SystemColors.ControlText
    Me.CheckBox2.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.CheckBox2.Location = New System.Drawing.Point(6, 213)
    Me.CheckBox2.Name = "CheckBox2"
    Me.CheckBox2.Size = New System.Drawing.Size(68, 17)
    Me.CheckBox2.TabIndex = 58
    Me.CheckBox2.Text = "Machten"
    Me.CheckBox2.UseVisualStyleBackColor = True
    '
    'frmMain
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
    Me.ClientSize = New System.Drawing.Size(952, 510)
    Me.ContextMenuStrip = Me.cmsNotifyStrip
    Me.Controls.Add(Me.grpMessage)
    Me.Controls.Add(Me.cmdDeleteEvaluation)
    Me.Controls.Add(Me.cmdShowEvaluations)
    Me.Controls.Add(Me.grpInfo)
    Me.Controls.Add(Me.pnlChangeOfUser)
    Me.Controls.Add(Me.cmdStart)
    Me.Controls.Add(Me.grpConfig)
    Me.DoubleBuffered = True
    Me.MinimumSize = New System.Drawing.Size(960, 540)
    Me.Name = "frmMain"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "NuCalc"
    Me.grpDifficulty.ResumeLayout(False)
    Me.grpDifficulty.PerformLayout()
    Me.grpAdvanced.ResumeLayout(False)
    Me.grpAdvanced.PerformLayout()
    Me.grpInfo.ResumeLayout(False)
    Me.pnlChangeOfUser.ResumeLayout(False)
    Me.pnlChangeOfUser.PerformLayout()
    Me.grpConfig.ResumeLayout(False)
    Me.grpConfig.PerformLayout()
    Me.cmsNotifyStrip.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
    Friend WithEvents txtUsername As System.Windows.Forms.TextBox
    Friend WithEvents cboExcerciseType As System.Windows.Forms.ComboBox
    Friend WithEvents rdoHard As System.Windows.Forms.RadioButton
    Friend WithEvents rdoMedium As System.Windows.Forms.RadioButton
    Friend WithEvents rdoEasy As System.Windows.Forms.RadioButton
    Friend WithEvents grpDifficulty As System.Windows.Forms.GroupBox
    Friend WithEvents txtAge As System.Windows.Forms.TextBox
    Friend WithEvents grpAdvanced As System.Windows.Forms.GroupBox
    Friend WithEvents txtQuantityOfNums As System.Windows.Forms.TextBox
    Friend WithEvents lblQuantityOfNums As System.Windows.Forms.Label
    Friend WithEvents chkNoZero As System.Windows.Forms.CheckBox
    Friend WithEvents txtMinOne As System.Windows.Forms.TextBox
    Friend WithEvents lblMinOne As System.Windows.Forms.Label
    Friend WithEvents txtMaxOne As System.Windows.Forms.TextBox
    Friend WithEvents lblMaxOne As System.Windows.Forms.Label
    Friend WithEvents txtMinOutput As System.Windows.Forms.TextBox
    Friend WithEvents lblMinOutput As System.Windows.Forms.Label
    Friend WithEvents txtMaxOutput As System.Windows.Forms.TextBox
    Friend WithEvents lblMaxOutput As System.Windows.Forms.Label
    Friend WithEvents cmdClear As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents grpMessage As System.Windows.Forms.GroupBox
    Friend WithEvents cmdNewUser As System.Windows.Forms.Button
    Friend WithEvents lblUsername As System.Windows.Forms.Label
    Friend WithEvents lblAge As System.Windows.Forms.Label
    Friend WithEvents cmdDeleteEvaluation As System.Windows.Forms.Button
    Friend WithEvents cmdShowEvaluations As System.Windows.Forms.Button
    Friend WithEvents grpInfo As System.Windows.Forms.GroupBox
    Friend WithEvents cmdCalcRules As System.Windows.Forms.Button
    Friend WithEvents webInfo As System.Windows.Forms.WebBrowser
    Friend WithEvents rtbEvaluation As System.Windows.Forms.RichTextBox
    Friend WithEvents pnlChangeOfUser As System.Windows.Forms.Panel
    Friend WithEvents cmdStart As System.Windows.Forms.Button
    Friend WithEvents grpConfig As System.Windows.Forms.GroupBox
    Friend WithEvents tmrRefresh As System.Windows.Forms.Timer
    Friend WithEvents cmsNotifyStrip As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SluitenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkHulp As System.Windows.Forms.CheckBox
    Friend WithEvents BeheerHetGeheugenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtMinZero As System.Windows.Forms.TextBox
    Friend WithEvents lblMinZero As System.Windows.Forms.Label
    Friend WithEvents txtMaxZero As System.Windows.Forms.TextBox
    Friend WithEvents lblMaxZero As System.Windows.Forms.Label
    Friend WithEvents chkBrackets As System.Windows.Forms.CheckBox
    Friend WithEvents chkComma As System.Windows.Forms.CheckBox
  Friend WithEvents cmdManageMem As System.Windows.Forms.Button
  Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
  Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
End Class
