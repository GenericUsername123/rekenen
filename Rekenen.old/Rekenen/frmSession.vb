﻿Public Class frmSession
#Region "Variables"
    Public _strTest As String
    Public _blnFirstRun As Boolean = True
#End Region

#Region "Form"
    Private Sub frmSession_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Icon = My.Resources.DefaultIcon
        txtAnswer.Focus()
        If _blnFirstRun = True Then
            _blnFirstRun = False
            prgQuantity.Value = 0
            prgScore.Value = 0
        Else
            prgScore.Value = frmMain._intValuePts
            prgQuantity.Value += frmMain._intQuantityOfExcercises
        End If
    End Sub

    Private Sub frmSession_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        txtAnswer.Select()
    End Sub
#End Region

#Region "Button_Click"
    Private Sub cmdClear_Click(sender As System.Object, e As System.EventArgs) Handles cmdClear.Click
        txtAnswer.Text = ""
        txtAnswer.Focus()
    End Sub

    Private Sub cmdCancel_Click(sender As System.Object, e As System.EventArgs) Handles cmdCancel.Click
        _strTest = "Cancel"
        Me.Close()
    End Sub

    Private Sub cmdOK_Click(sender As System.Object, e As System.EventArgs) Handles cmdOK.Click
        _strTest = txtAnswer.Text
        txtAnswer.Text = "Getal..."
        txtAnswer.Focus()
        Me.Close()
    End Sub
#End Region

#Region "TextBox_KeyPress"
    Private Sub txtAnswer_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtAnswer.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            cmdOK.PerformClick()
        End If
        If (IsNumeric(e.KeyChar) Or e.KeyChar = Convert.ToChar(Keys.Back)) = False Then
            e.Handled = True
        End If
    End Sub
#End Region
End Class