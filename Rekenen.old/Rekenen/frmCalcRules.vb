﻿Public Class frmCalcRules

    Private Sub frmCalcRules_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = My.Resources.DefaultIcon
        Me.Size = New Size(Math.Round(SystemInformation.WorkingArea.Height * 0.707) + 20, SystemInformation.WorkingArea.Height)
        Me.MaximumSize = Me.Size
        Me.MinimumSize = Me.Size
        Me.CenterToScreen()

        webDisplayCalcRules.Navigate(frmMain._strDir & "docs\Rekenregels.htm")
        webDisplayCalcRules.Select()
    End Sub
End Class