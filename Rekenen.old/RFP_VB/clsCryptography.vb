﻿Imports System.IO
Imports System.Threading
Imports System.Security.Cryptography

''' <summary>
''' Deze dll bevat Rijndeal encryptie, één van de beste opties van vandaag.
''' De beste manier om deze dll te gebruiken is door via een apparte thread de methods te laten lopen zodat alles kan updaten via clsCryptography.Progress.
''' (Versie: 0.9.3.1)
''' </summary>
''' <remarks></remarks>
Public Class clsCryptography
    Private _fsInput As FileStream
    Private _fsOutput As FileStream
    Public Progress As Integer

#Region "Create key and IV"
    ''' <summary>
    ''' De key die gebruikt word in encryptie of decryptie (256 bits).
    ''' </summary>
    ''' <param name="Password">Het wachtwoord dat nodig is om een key te genereeren.</param>
    ''' <returns>Returns de key.</returns>
    ''' <remarks></remarks>

    Public Function CreateKey(Password As String) As Byte()
        Dim chrData() As Char = Password.ToCharArray
        Dim intLength As Integer = chrData.GetUpperBound(0)
        Dim bytDataToHash(intLength) As Byte

        For i = 0 To intLength
            bytDataToHash(i) = Convert.ToByte(chrData(i))
        Next

        Dim SHA512 As New SHA512Managed
        Dim bytDataInHash() As Byte = SHA512.ComputeHash(bytDataToHash)
        Dim bytKey(31) As Byte

        For i = 0 To 31
            bytKey(i) = bytDataInHash(i)
        Next
        Return bytKey
    End Function

    ''' <summary>
    ''' De Intitialisatie Vector die gebruikt word in encryptie of decryptie. (128 bits)
    ''' </summary>
    ''' <param name="Password">Het wachtwoord dat nodig is om een IV te genereeren.</param>
    ''' <returns>Returns de IV</returns>
    ''' <remarks></remarks>
    Public Function CreateIV(Password As String) As Byte()
        Dim chrData() As Char = Password.ToCharArray
        Dim intLength As Integer = chrData.GetUpperBound(0)
        Dim bytDataToHash(intLength) As Byte

        For i = 0 To intLength
            bytDataToHash(i) = Convert.ToByte(chrData(i))
        Next

        Dim SHA512 As New SHA512Managed
        Dim bytDataInHash() As Byte = SHA512.ComputeHash(bytDataToHash)
        Dim bytIV(15) As Byte

        'Het volgende stuk hash moet gebruikt worden (dus sla de 32 bits over).
        For i = 32 To 32 + 15
            bytIV(i - 32) = bytDataInHash(i)
        Next
        Return bytIV
    End Function
#End Region

#Region "Action"
    ''' <summary>
    ''' De actie die je wilt ondernemen.
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum CryptoAction
        ActionEncrypt = 1
        ActionDecrypt = 2
    End Enum
#End Region

#Region "Encrypt/Decrypt"
    ''' <summary>
    ''' Eender welk bestand onleesbaar maken of weer leesbaar maken (Rijndael).
    ''' </summary>
    ''' <param name="InputFile">Het bestand die de data bevat.</param>
    ''' <param name="OutputFile">Het bestand waar de bewerkte data in verwerkt word.</param>
    ''' <param name="Key">De key dat gebruikt word om data te encrypteren of decrypteren.</param>
    ''' <param name="IV">Initialisatie Vector: Dit word gebruikt om het eerste gedeelte van de data te encrypteren.</param>
    ''' <param name="Action">De actie encrypteren of decrypteren.</param>
    ''' <remarks></remarks>
    Public Sub EncryptDecryptFile(InputFile As String, OutputFile As String, Key() As Byte, IV() As Byte, Action As CryptoAction)
        _fsInput = New FileStream(InputFile, FileMode.Open, FileAccess.Read)
        _fsOutput = New FileStream(OutputFile, FileMode.OpenOrCreate, FileAccess.Write)
        _fsOutput.SetLength(0)
        Progress = 0

        Dim intBufferSize As Integer = 4096
        Dim bytBuffer(intBufferSize) As Byte
        Dim lngBytesProcessed As Long
        Dim lngFileLength As Long = _fsInput.Length
        Dim intBytesInCurrentBlock As Integer
        Dim strMessageWhenCompleted As String
        Dim csCryptoStream As CryptoStream
        Dim csRijndael As New RijndaelManaged

        Select Case Action
            Case CryptoAction.ActionEncrypt
                csCryptoStream = New CryptoStream(_fsOutput, csRijndael.CreateEncryptor(Key, IV), CryptoStreamMode.Write)
                strMessageWhenCompleted = "Encryptie voltooid."
            Case Else
                csCryptoStream = New CryptoStream(_fsOutput, csRijndael.CreateDecryptor(Key, IV), CryptoStreamMode.Write)
                strMessageWhenCompleted = "Decryptie voltooid."
        End Select
        While lngBytesProcessed < lngFileLength
            intBytesInCurrentBlock = _fsInput.Read(bytBuffer, 0, intBufferSize)
            csCryptoStream.Write(bytBuffer, 0, intBytesInCurrentBlock)
            lngBytesProcessed += intBytesInCurrentBlock
            Progress = lngBytesProcessed / lngFileLength * 100
        End While

        csCryptoStream.Close()
        _fsInput.Close()
        _fsOutput.Close()
    End Sub
#End Region

#Region "Generate password"
    Public Function GeneratePassword(LengthOfPassword As Integer)
        Dim strPassword As String = Nothing
        Dim Rnd As New Random

        For i = 1 To LengthOfPassword
            If Rnd.Next(2) = 0 Then
                strPassword = strPassword & Rnd.Next(10)
            Else
                If Rnd.Next(2) = 0 Then
                    strPassword = strPassword & Convert.ToChar(Rnd.Next(97, 123))
                Else
                    strPassword = strPassword & UCase(Convert.ToChar(Rnd.Next(97, 123)))
                End If
            End If
        Next
        Return strPassword
    End Function
#End Region
End Class
