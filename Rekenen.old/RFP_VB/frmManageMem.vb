﻿Public Class frmManageMem
    Dim _blnConfirmed As Boolean

    Private Sub frmSettings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = My.Resources.DefaultIcon
        Me.Text += " " & My.Application.Info.ProductName
        If My.Settings.ErrorReport = "" Then
            chkLogs.Enabled = False
        End If
        If My.Settings.Evaluations = "" Then
            chkEvaluations.Enabled = False
        End If
    End Sub

    Private Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub cmdOK_Click(sender As Object, e As EventArgs) Handles cmdOK.Click
        cmdApply.PerformClick()
        cmdCancel.PerformClick()
    End Sub

    Private Sub cmdApply_Click(sender As Object, e As EventArgs) Handles cmdApply.Click
        If chkLogs.Checked = True Then
            If MsgBox("Ben je zeker dat je de logs van " & My.Application.Info.ProductName & " wilt verwijderen?" & vbCrLf & _
                   "Als je deze logs verwijdert wordt er mogelijk geen rekening gehouden met fouten in " & My.Application.Info.ProductName & " in de toekomst.", _
                   MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo, "Verwijderen van logs") = MsgBoxResult.Yes Then
                _blnConfirmed = True
            End If
        ElseIf chkEvaluations.Checked Or chkPreferences.Checked Or chkProfile.Checked Then
            If MsgBox("Ben je zeker dat je de geselecteerde items wilt verwijderen?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo, "Verwijderen van logs") = MsgBoxResult.Yes Then
                _blnConfirmed = True
            End If
        End If

        If _blnConfirmed Then
            If chkLogs.Checked Then
                My.Settings.ErrorReport = ""
                chkLogs.Checked = False
            End If
            If chkProfile.Checked Then
                My.Settings.Username = SystemInformation.UserName
                My.Settings.Age = 0
                My.Settings.Suggestions = SystemInformation.UserName
                frmMain.txtUsername.AutoCompleteCustomSource.Clear()
                frmMain.txtUsername.AutoCompleteCustomSource.Add("Anoniem")
                frmMain.txtUsername.AutoCompleteCustomSource.Add(SystemInformation.UserName)
                chkProfile.Checked = False
            End If
            If chkPreferences.Checked Then
                My.Settings.Hulp = True
                My.Settings.LastExcerciseType = "Som ( + )"
                My.Settings.ShowDisclaimer = True
                chkPreferences.Checked = False
            End If
            If chkEvaluations.Checked Then
                My.Settings.Evaluations = ""
                chkEvaluations.Checked = False
            End If
        End If
        If My.Settings.ErrorReport = "" Then
            chkLogs.Enabled = False
        End If
        If My.Settings.Evaluations = "" Then
            chkEvaluations.Enabled = False
        End If
    End Sub

    Private Sub cmdDeleteAll_Click(sender As Object, e As EventArgs) Handles cmdDeleteAll.Click
        chkLogs.Checked = True
        chkProfile.Checked = True
        chkPreferences.Checked = True
        chkEvaluations.Checked = True
    End Sub
End Class