﻿Public Class frmMain
#Region "Variables"
    Public _intValuePts As Integer
    Public _intQuantityOfExcercises As Integer
    Public _strDoc As String = My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & My.Application.Info.CompanyName & "\" & My.Application.Info.ProductName & "\"
    Public _strDir As String = Mid(Process.GetCurrentProcess.Modules(0).FileName, 1, InStrRev(Process.GetCurrentProcess.Modules(0).FileName, "\"))

    Dim _Random As New Random
    Dim _strLogin As String = SystemInformation.UserName
    Dim _strVersion As String = My.Application.Info.Version.ToString
    Dim _intTimesClick As Integer
    Dim _intTimesTick As Integer
    Dim _blnDevMode As Boolean
    Dim _blnTesting As Boolean
    Dim _blnLessThanSix As Boolean
    Dim _strPreviousKeyChar As String
    Dim _strType As String
#End Region

#Region "Preparing frmMain"
    Private Sub frmMain_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            Dim intWorkingAreaHeight As Integer = SystemInformation.WorkingArea.Height
            Dim intWorkingAreaWidth As Integer = SystemInformation.WorkingArea.Width
            Dim intFormHeight As Integer = Me.Height
            Dim intFormWidth As Integer = Me.Width

            If intFormHeight > intWorkingAreaHeight Or intFormWidth > intWorkingAreaWidth Then
                frmError.ShowDialog()
            End If
            If My.Settings.DevMode = True Then
                _blnDevMode = True
                cmdTestMode.Visible = True
                cmdTestMode.BackColor = Color.Red
            End If

            My.Computer.FileSystem.CreateDirectory(_strDoc)
            FormDesign()
            If My.Computer.FileSystem.FileExists(_strDoc & "Evaluation.txt") = False Then
                My.Computer.FileSystem.WriteAllText(_strDoc & "Evaluation.txt", "", False)
            End If
            If My.Computer.FileSystem.ReadAllText(_strDoc & "Evaluation.txt") = "" Then
                cmdShowEvaluations.Visible = False
                cmdDeleteEvaluation.Visible = False
            Else
                cmdShowEvaluations.Visible = True
                cmdDeleteEvaluation.Visible = False
            End If
            webInfo.Navigate(_strDir & "docs\Info.htm")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Interne fout")
            My.Computer.FileSystem.WriteAllText(_strDir & "\logs.txt", My.Computer.Clock.LocalTime & vbCrLf & "Message: " & ex.Message & vbCrLf & "Stack trace: " & ex.StackTrace & vbCrLf & "Version: " & _
                                                My.Application.Info.Version.ToString & vbCrLf & vbCrLf & "_" & vbCrLf, True)
            Me.Close()
        End Try
    End Sub

    Private Sub frmMain_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        MsgBox("Het programma Rekenen is nog in constructie." & vbCrLf & "Bepaalde functies werken mogelijk niet of niet volledig." & vbCrLf & vbCrLf & "Veel succes!", _
                MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Disclaimer")
    End Sub

    Function FormDesign()
        'Form
        Me.Text = My.Application.Info.ProductName & " (v" & _strVersion & ")"
        Me.Icon = My.Resources.DefaultIcon
        'TabControl
        tctrlType.SelectedIndex = 0
        LayoutForEachType()
        'Groupbox
        If Mid(_strVersion, 1, 1) <> 0 Then
            grpCurrentVersion.Text = "Welkom!" & vbCrLf & "Rekenen versie " & _strVersion & "."
        Else
            grpCurrentVersion.Text = "Welkom!" & vbCrLf & "Rekenen Bèta versie " & _strVersion & "."
        End If
        Hulp.SetToolTip(grpCurrentVersion, "Build " & _strVersion)
        'Button
        cmdClear.Text = "Ongedaan maken"
        cmdClear.AutoSize = True
        cmdClear.Visible = False
        'CheckBox
        Hulp.SetToolTip(chkNoZero, "Geen nullen te zien in een volledige sessie.")
        'WebBrowser
        webInfo.WebBrowserShortcutsEnabled = True
        'Label
        Hulp.SetToolTip(lblMaxOutput, "De hoogste uitkomst mogelijk kan je hier invoeren.")
        Hulp.SetToolTip(lblMinOutput, "De kleinste uitkomst mogelijk kan je hier invoeren.")
        Hulp.SetToolTip(lblMaxOne, "Maximaal aantal keer 1 als getal (of factor) in een sessie.")
        Hulp.SetToolTip(lblMinOne, "Minimaal aantal keer 1 als getal (of factor) in een sessie.")
        Hulp.SetToolTip(lblQuantityOfNums, "Het aantal getallen (of factoren) in een sessie.")
        'TextBox
        rtbCorrection.ScrollBars = ScrollBars.None
        rtbCorrection.Visible = False
        rtbCorrection.Text = "Correcties komen hier te staan..."
        txtUsername.Text = My.Settings.Username
        txtAge.Text = My.Settings.Age
        Return 0
    End Function
#End Region

#Region "Form control"
    Private Sub cmdStart_Click(sender As System.Object, e As System.EventArgs) Handles cmdStart.Click
        Dim strLogInfo As String = Nothing
        Try
            'Functionality
            Dim j As Integer
            Dim sngNumber() As Single = Nothing
            Dim intDifference As Integer
            Dim intAnswer As Integer
            Dim sngSolution As Single
            Dim intQuantityOfExcercises As Integer
            Dim strWrong As String = Nothing
            Dim strTest As String
            Dim sngPercentTimesOne As Single
            Dim strDisplayExercise As String = Nothing
            Dim strValidation As String = Nothing
            Dim intExerciseTimesOne() As Integer = Nothing
            Dim strShowExerciseTimeOne As String = Nothing
            Dim blnFirstLoop As Boolean = True
            Dim intRandomExerciseNumber As Integer = 0
            Dim blnExit As Boolean
            Dim intHiLoRnd As Integer
            Dim intLooped As Integer
            Dim intMinOutputNeeded As Integer = 10
            'Settings
            Dim intMaxQuantity As Integer = 100
            Dim sngMaxOutput As Single
            Dim sngMinOutput As Single = -1000000
            Dim sngMaxOne As Single
            Dim sngMinOne As Single
            Dim intQuantityOfNums As Integer
            'Extra's
            Dim intScore As Integer
            Dim intJudgeAndRecomend As Integer = 10
            Dim intRight As Integer
            Dim intCurrency As Integer
            Dim intAge As Integer
            Dim sngCurrencyMultiplier As Single = 1
            'Stopwatch
            Dim spwStopwatch As New Stopwatch()
            Dim strElapsedMinutes As String
            Dim strElapsedSeconds As String
            Dim strElapsedMilliseconds As String
            Dim intLvlOfDifficulty As Integer
            'Save data
            Dim strUsername As String = My.Settings.Username
            Dim strEvaluation As String
            Dim strChr As String = Nothing
            Dim strExerciseType As String = Nothing
            'Test
            Dim intTest As Integer

            intAge = My.Settings.Age
            rtbCorrection.Visible = True
            rtbCorrection.ScrollBars = ScrollBars.Vertical

            If IsNumeric(txtQuantityOfNums.Text) = False Then
                txtQuantityOfNums.Text = 2
            Else
                If txtQuantityOfNums.Text < 2 Then
                    txtQuantityOfNums.Text = 2
                End If
            End If
            intQuantityOfNums = txtQuantityOfNums.Text
            ReDim sngNumber(0 To intQuantityOfNums - 1)

            strUsername = UCase(Mid(strUsername, 1, 1)) & Mid(strUsername, 2, Len(strUsername))
            strEvaluation = "- Prestaties van " & strUsername & ": " & vbCrLf & vbCrLf
            If intAge = 0 Then
                intAge = 18
            End If

            'Check instellingen
            Settings(sngMaxOutput:=sngMaxOutput, sngMinOutput:=sngMinOutput, sngMaxOne:=sngMaxOne, sngMinOne:=sngMinOne)
            Advanced(sngMaxOutput:=sngMaxOutput, sngMinOutput:=sngMinOutput, sngMaxOne:=sngMaxOne, sngMinOne:=sngMinOne, blnExit:=blnExit)

            If sngMaxOutput <> 0 And blnExit = False Then
                'Bepaal intQuantity
                Do
                    strTest = InputBox("Hoeveel oefeningen wil je doen?", "Aantal oefeningen", "Voer een getal in...")
                    If strTest <> "" Then
                        If IsNumeric(strTest) = True Then
                            If strTest > intMaxQuantity Then
                                intQuantityOfExcercises = strTest
                                MsgBox("Je mag maximaal " & intMaxQuantity & " oefeningen maken in één keer.", , "Aantal oefeningen")
                            Else
                                intQuantityOfExcercises = strTest
                            End If
                        Else
                            intQuantityOfExcercises = intMaxQuantity + 1
                            MsgBox("Geef een getal in.", , "Aantal oefeningen")
                        End If
                    Else
                        blnExit = True
                        rtbCorrection.ScrollBars = ScrollBars.None
                        rtbCorrection.Visible = False
                        rtbCorrection.Text = "Correcties komen hier te staan..."
                    End If
                Loop While intQuantityOfExcercises > intMaxQuantity And blnExit = False
                'End

                If blnExit = False Then
                    'Currency
                    Select Case _strType
                        Case Is = " x "
                            intCurrency = Math.Round(((sngMaxOutput * 4) / 3) + ((sngMinOutput * 4) / 3) - (sngMaxOne * ((sngMaxOutput * 4) / 3) + sngMaxOne * 10) - (sngMinOne * ((sngMinOutput * 4) / 3) + _
                                                                                                                                                                      sngMinOne * 10), 0)
                        Case Is = " : "
                            intCurrency = Math.Round((((sngMaxOutput * 4) / 3) + ((sngMinOutput * 4) / 3)) * (1 + 0.3), 0)
                        Case Is = " + "
                            intCurrency += sngMinOutput * 0.5 + sngMaxOutput * 0.5
                        Case Is = " - "
                            intCurrency += sngMinOutput * 0.5 + sngMaxOutput * 0.5
                    End Select
                    Select Case intCurrency
                        Case Is <= 10
                            intLvlOfDifficulty = 1
                        Case Is <= 20
                            intLvlOfDifficulty = 2
                        Case Is <= 40
                            intLvlOfDifficulty = 3
                        Case Is <= 65
                            intLvlOfDifficulty = 4
                        Case Is <= 90
                            intLvlOfDifficulty = 5
                        Case Is <= 115
                            intLvlOfDifficulty = 6
                        Case Is <= 140
                            intLvlOfDifficulty = 7
                        Case Is <= 165
                            intLvlOfDifficulty = 8
                        Case Is <= 190
                            intLvlOfDifficulty = 9
                        Case Is > 190
                            intLvlOfDifficulty = 10
                    End Select
                    Select Case intAge
                        Case Is < 6
                            sngCurrencyMultiplier = 10
                        Case Is = 6
                            sngCurrencyMultiplier = 3
                        Case Is = 7
                            sngCurrencyMultiplier = 2
                        Case Is = 8
                            sngCurrencyMultiplier = 1.7
                        Case Is = 9
                            sngCurrencyMultiplier = 1.4
                        Case Is = 10
                            sngCurrencyMultiplier = 1.2
                        Case Is = 11
                            sngCurrencyMultiplier = 1.1
                        Case Is = 12
                            sngCurrencyMultiplier = 1.05
                    End Select
                    intCurrency = intCurrency * intQuantityOfExcercises * sngCurrencyMultiplier
                    If _blnDevMode = True Then
                        MsgBox("intCurrency: " & intCurrency)
                        MsgBox("intLvlOfDifficulty: " & intLvlOfDifficulty)
                    End If
                    'End

                    If intQuantityOfExcercises > 0 And blnExit = False Then
                        _intQuantityOfExcercises = 100 / intQuantityOfExcercises
                        frmSession._blnFirstRun = True
                        spwStopwatch.Start()
                        For i = 1 To intQuantityOfExcercises
                            Select Case _strType
                                Case Is = " x "
                                    UseWaitCursor = True
                                    Product(sngNumber:=sngNumber, intMaxQuantity:=intMaxQuantity, sngMaxOutput:=sngMaxOutput, sngMinOutput:=sngMinOutput, sngMaxOne:=sngMaxOne, sngMinOne:=sngMinOne, _
                                            intQuantityOfNums:=intQuantityOfNums, blnFirstLoop:=blnFirstLoop, intExerciseTimesOne:=intExerciseTimesOne, intQuantityOfExcercises:=intQuantityOfExcercises, _
                                            intRandomExerciseNumber:=intRandomExerciseNumber, j:=j, blnDevMode:=_blnDevMode, i:=i, _
                                            sngPercentTimesOne:=sngPercentTimesOne, sngSolution:=sngSolution, strDisplayExercise:=strDisplayExercise, strShowExerciseTimeOne:=strShowExerciseTimeOne, _
                                            _strType:=_strType, strValidation:=strValidation)
                                Case Is = " : "
                                    UseWaitCursor = True
                                    Quotiënt(sngNumber:=sngNumber, intHiLoRnd:=intHiLoRnd, intLooped:=intLooped, intQuantityOfNums:=intQuantityOfNums, sngMaxOutput:=sngMaxOutput, sngMinOutput:=sngMinOutput, _
                                             sngSolution:=sngSolution, strDisplayExercise:=strDisplayExercise, _strType:=_strType, strValidation:=strValidation)

                                    If Convert.ToInt16(Array.IndexOf(sngNumber, 0)) <> -1 Then
                                        MsgBox("Er is een fout opgetreden bij het genereeren van een quotiënt." & vbCrLf & "Het wordt aangeraden geen quotiënt meer te gebruiken.", MsgBoxStyle.Critical, _
                                               "Fout bij het genereren")
                                        strLogInfo = "Fout opgetreden bij het genereeren van een quotiënt." & vbCrLf & "_" & vbCrLf
                                        My.Computer.FileSystem.WriteAllText(_strDir & "\logs.txt", strLogInfo, True)
                                        blnExit = True
                                    End If
                                Case Is = " - "
                                    UseWaitCursor = True
                                    Verschil(sngNumber:=sngNumber, intDifference:=intDifference, intQuantityOfNums:=intQuantityOfNums, sngMaxOutput:=sngMaxOutput, sngMinOutput:=sngMinOutput, _
                                             sngSolution:=sngSolution, strDisplayExercise:=strDisplayExercise, _strType:=_strType, strValidation:=strValidation)
                                Case Is = " + "
                                    UseWaitCursor = True
                                    Som(sngNumber:=sngNumber, intQuantityOfNums:=intQuantityOfNums, sngMaxOutput:=sngMaxOutput, sngMinOutput:=sngMinOutput, _
                                             sngSolution:=sngSolution, strDisplayExercise:=strDisplayExercise, _strType:=_strType, strValidation:=strValidation)
                            End Select
                            'Herdefiniëren van intQuantityOfNums.
                            intQuantityOfNums = txtQuantityOfNums.Text

                            frmSession.lblQuestion.Text = "Wat is: " & Mid(strDisplayExercise, 1, strDisplayExercise.Length - 2)
                            frmSession.ShowDialog()
                            strTest = frmSession._strTest

                            If strTest = "Cancel" Then
                                blnExit = True
                                If strWrong = "" Then
                                    strWrong += "Oefening gestopt op " & i & " van " & intQuantityOfExcercises & "."
                                Else
                                    strWrong += vbCrLf & "Oefening gestopt op " & i & " van " & intQuantityOfExcercises & "."
                                End If
                                i = intQuantityOfExcercises
                            Else
                                intAnswer = 2000000
                                If IsNumeric(strTest) Then
                                    If strTest < 2000000 Then
                                        intAnswer = strTest
                                    Else
                                        intAnswer = 0
                                    End If
                                Else
                                    intAnswer = 0
                                End If
                            End If

                            If blnExit = False Then
                                If sngSolution = intAnswer Then
                                    intRight += 1
                                    intScore = intRight / intQuantityOfExcercises * 100
                                Else
                                    If i <> intQuantityOfExcercises Then
                                        strWrong += "Nr. " & i & ":  " & strDisplayExercise & sngSolution & "  →  " & "Antwoord: " & intAnswer & vbCrLf
                                    Else
                                        strWrong += "Nr. " & i & ":  " & strDisplayExercise & sngSolution & "  →  " & "Antwoord: " & intAnswer
                                    End If
                                End If
                                _intValuePts = Math.Round(intRight / i * 100, 0)
                            End If

                            sngSolution = sngMaxOutput + 1
                            strDisplayExercise = ""
                        Next

                        spwStopwatch.Stop()
                        'Minuten
                        strElapsedMinutes = Math.Round(spwStopwatch.ElapsedMilliseconds / 1000 / 60 - 0.5)
                        'Seconden
                        strElapsedSeconds = Math.Round(spwStopwatch.ElapsedMilliseconds / 1000 - 0.5) - Convert.ToInt32(strElapsedMinutes) * 60
                        'Milliseconden
                        strElapsedMilliseconds = spwStopwatch.ElapsedMilliseconds - Math.Round((spwStopwatch.ElapsedMilliseconds / 1000 - 0.5)) * 1000

                        If intRight >= 1 Then
                            'JudgeAndRecomend
                            Select Case intScore
                                Case Is >= 60
                                    If intQuantityOfExcercises >= intJudgeAndRecomend Then
                                        MsgBox("Je hebt " & intRight & " juiste antwoorden op de " & intQuantityOfExcercises & " oefeningen." & vbCrLf & _
                                               "Je mag gerust een oefening starten die moeilijker is.", , "Aantal juist")
                                    Else
                                        If intRight <> 1 Then
                                            If intQuantityOfExcercises <> 1 Then
                                                MsgBox("Je hebt " & intRight & " juiste antwoorden op de " & intQuantityOfExcercises & " oefeningen.", , "Aantal juist")
                                            Else
                                                MsgBox("Je hebt " & intRight & " juiste antwoorden op " & intQuantityOfExcercises & " oefening.", , "Aantal juist")
                                            End If
                                        Else
                                            If intQuantityOfExcercises <> 1 Then
                                                MsgBox("Je hebt " & intRight & " juiste antwoord op de " & intQuantityOfExcercises & " oefeningen.", , "Aantal juist")
                                            Else
                                                MsgBox("Je hebt " & intRight & " juiste antwoord op " & intQuantityOfExcercises & " oefening.", , "Aantal juist")
                                            End If
                                        End If
                                    End If
                                Case Is <= 30
                                    If intQuantityOfExcercises >= intJudgeAndRecomend Then
                                        MsgBox("Je hebt " & intRight & " juiste antwoorden op de " & intQuantityOfExcercises & " oefeningen." & vbCrLf & _
                                               "Ik raad je aan een oefening te starten die makkelijker is.", , "Aantal juist")
                                    Else
                                        If intRight <> 1 Then
                                            If intQuantityOfExcercises <> 1 Then
                                                MsgBox("Je hebt " & intRight & " juiste antwoorden op de " & intQuantityOfExcercises & " oefeningen.", , "Aantal juist")
                                            Else
                                                MsgBox("Je hebt " & intRight & " juiste antwoorden op " & intQuantityOfExcercises & " oefening.", , "Aantal juist")
                                            End If
                                        Else
                                            If intQuantityOfExcercises <> 1 Then
                                                MsgBox("Je hebt " & intRight & " juiste antwoord op de " & intQuantityOfExcercises & " oefeningen.", , "Aantal juist")
                                            Else
                                                MsgBox("Je hebt " & intRight & " juiste antwoord op " & intQuantityOfExcercises & " oefening.", , "Aantal juist")
                                            End If
                                        End If
                                    End If
                                Case Else
                                    If intRight <> 1 Then
                                        If intQuantityOfExcercises <> 1 Then
                                            MsgBox("Je hebt " & intRight & " juiste antwoorden op de " & intQuantityOfExcercises & " oefeningen.", , "Aantal juist")
                                        Else
                                            MsgBox("Je hebt " & intRight & " juiste antwoorden op " & intQuantityOfExcercises & " oefening.", , "Aantal juist")
                                        End If
                                    Else
                                        If intQuantityOfExcercises <> 1 Then
                                            MsgBox("Je hebt " & intRight & " juiste antwoord op de " & intQuantityOfExcercises & " oefeningen.", , "Aantal juist")
                                        Else
                                            MsgBox("Je hebt " & intRight & " juiste antwoord op " & intQuantityOfExcercises & " oefening.", , "Aantal juist")
                                        End If
                                    End If
                            End Select
                        End If
                        'End

                        If strWrong = "" Then
                            rtbCorrection.Text = "Tijd nodig: " & strElapsedSeconds & "s en " & strElapsedMilliseconds & "ms"
                        Else
                            rtbCorrection.Text = strWrong & vbCrLf & vbCrLf & "Tijd nodig: " & strElapsedMinutes & "m, " & strElapsedSeconds & "s en " & strElapsedMilliseconds & "ms"
                        End If
                    Else
                        If blnExit = False Then
                            MsgBox("Het aantal oefeningen moet groter zijn dan 0.", , "Foute invoer")
                        Else
                            rtbCorrection.ScrollBars = ScrollBars.None
                            rtbCorrection.Visible = False
                            rtbCorrection.Text = "Correcties komen hier te staan..."
                        End If
                    End If
                End If
            ElseIf sngMaxOutput = 0 Then
                MsgBox("De maximale uitkomst kan niet 0 zijn. Vul het tekstvak ""Maximale uitkomst"" in of gebruik een moeilijkheidsniveau.", MsgBoxStyle.Critical, "Maximale uitkomst is nul.")
            End If

            intCurrency *= intScore / 100

            strEvaluation += rtbCorrection.Text

            'strExerciseType
            Select Case _strType
                Case Is = " x "
                    strExerciseType = "Type oefening: product of maal"
                Case Is = " : "
                    strExerciseType = "Type oefening: quotiënt of gedeelt door"
                Case Is = " + "
                    strExerciseType = "Type oefening: som of plus"
                Case Is = " - "
                    strExerciseType = "Type oefening: verschil of min"
            End Select
            strEvaluation += vbCrLf & vbCrLf & "Moeilijkheidsgraad: " & intLvlOfDifficulty & vbCrLf & strExerciseType & vbCrLf & vbCrLf & vbCrLf

            If blnExit = False Then
                cmdShowEvaluations.Visible = True
                cmdDeleteEvaluation.Visible = False

                My.Computer.FileSystem.WriteAllText(_strDoc & "Evaluation.txt", strEvaluation, True)
            End If

            If _blnTesting = True Then
                MsgBox("Percentage van aantal keer 1: " & intTest / intQuantityOfExcercises * 100 & "%")
            End If

            UseWaitCursor = False
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Interne fout")
            strLogInfo += My.Computer.Clock.LocalTime & vbCrLf & "Message: " & ex.Message & vbCrLf & "Stack trace: " & ex.StackTrace & vbCrLf & "Version: " & My.Application.Info.Version.ToString & vbCrLf & _
                vbCrLf & "_" & vbCrLf
            My.Computer.FileSystem.WriteAllText(_strDir & "\logs.txt", strLogInfo, True)
        End Try
    End Sub

    Private Sub cmdClear_Click(sender As System.Object, e As System.EventArgs) Handles cmdClear.Click
        'RadioButton
        rdoEasy.Checked = True
        rdoMedium.Checked = False
        rdoHard.Checked = False
        'CheckBox
        chkNoZero.Checked = False
        'TextBox
        txtMaxOutput.Text = ""
        txtMinOutput.Text = ""
        txtMaxOne.Text = ""
        txtMinOne.Text = ""
        txtQuantityOfNums.Text = "2"
        rtbCorrection.Text = "Correcties komen hier te staan..."
        rtbCorrection.Visible = False
        cmdClear.Visible = False
    End Sub

    Private Sub rtbCorrection_VisibleChanged(sender As Object, e As EventArgs) Handles rtbCorrection.VisibleChanged
        If rtbCorrection.Visible = True Then
            webInfo.Visible = False
            cmdCalcRules.Visible = False
        Else
            webInfo.Visible = True
            cmdCalcRules.Visible = True
        End If
    End Sub

    Private Sub TuneWhenClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        My.Computer.Audio.Play(My.Resources.SoundClosing, AudioPlayMode.Background)
        Threading.Thread.Sleep(200)
        Me.Hide()
        frmSession.Hide()
        frmError.Hide()
        frmSplashscreen.Hide()
        Threading.Thread.Sleep(800)
    End Sub

    Private Sub tmrRefreshForm_Tick(sender As Object, e As EventArgs) Handles tmrRefreshForm.Tick
        Try
            If My.Computer.FileSystem.FileExists(_strDoc & "Evaluation.txt") = False Or _
                My.Computer.FileSystem.ReadAllText(_strDoc & "Evaluation.txt") = "" Then
                cmdShowEvaluations.Visible = False
                cmdDeleteEvaluation.Visible = False
            Else
                cmdShowEvaluations.Visible = True
                cmdDeleteEvaluation.Visible = False
            End If
            tmrRefreshForm.Enabled = False
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Interne fout")
            My.Computer.FileSystem.WriteAllText(_strDir & "\logs.txt", My.Computer.Clock.LocalTime & vbCrLf & "Message: " & ex.Message & vbCrLf & "Stack trace: " & ex.StackTrace & vbCrLf & "Version: " & _
                                                My.Application.Info.Version.ToString & vbCrLf & vbCrLf & "_" & vbCrLf, True)
        End Try
    End Sub

#Region "TextBox Control"
    Private Sub TextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMaxOutput.KeyPress, txtMinOutput.KeyPress, txtMaxOne.KeyPress, txtMinOne.KeyPress, _
        txtQuantityOfNums.KeyPress, txtAge.KeyPress
        'Algemeen
        If (IsNumeric(e.KeyChar) Or e.KeyChar = Convert.ToChar(Keys.Back)) = False Then
            e.Handled = True
        End If
        'Extra txtMaxOutput
        If sender.Name = "txtMaxOutput" Then
            If (e.KeyChar = "0" Or e.KeyChar = "1" Or e.KeyChar = "2" Or e.KeyChar = "3" Or e.KeyChar = "4" Or e.KeyChar = "5") And txtMaxOutput.Text = "" Then
                _strPreviousKeyChar = e.KeyChar
                e.KeyChar = "6"
                _blnLessThanSix = True
            ElseIf _blnLessThanSix And txtMaxOutput.TextLength = 1 And IsNumeric(e.KeyChar) Then
                _blnLessThanSix = False
                txtMaxOutput.Text = _strPreviousKeyChar
                txtMaxOutput.SelectionStart = 2
            End If
        End If
        'Extra txtQuantityOfNums
        If sender.Name = "txtQuantityOfNums" And (e.KeyChar = "9" Or e.KeyChar = "8" Or e.KeyChar = "7" Or e.KeyChar = "6") Then
            e.Handled = True
        End If
        'Extra txtAge
        If sender.Name = "txtAge" Then
            If e.KeyChar = "0" Or e.KeyChar = "1" Or e.KeyChar = "2" Then
                e.Handled = True
            End If
            If txtAge.Text = "" Then
                txtAge.Text = 0
                txtAge.SelectAll()
            End If
        End If
    End Sub

    Private Sub txtMaxOutput_KeyDown(sender As Object, e As KeyEventArgs) Handles txtMaxOutput.KeyDown, txtMinOutput.KeyDown, txtMaxOne.KeyDown, txtMinOne.KeyDown, _
        txtQuantityOfNums.KeyDown
        If e.KeyCode = Keys.Enter Then
            cmdStart.PerformClick()
        End If
    End Sub
#End Region
#End Region

#Region "Instellingen"
    Function Advanced(ByRef sngMaxOutput As Single, ByRef sngMinOutput As Single, ByRef sngMaxOne As Single, ByRef sngMinOne As Single, ByRef blnExit As Boolean)
        If txtMaxOutput.Text <> "" Then
            sngMaxOutput = txtMaxOutput.Text
        End If
        If txtMinOutput.Text <> "" Then
            If txtMinOutput.Text > sngMaxOutput Then
                MsgBox("De maximale uitkomst is lager dan de minimale uitkomst. Controleer de instellingen onder ""Ingesteld"" en de maximale uitkomst onder ""Geavanceerd"".", MsgBoxStyle.Exclamation, _
                       "Maximum kleiner dan minimum")
                blnExit = True
                Return 0
                Exit Function
            Else
                sngMinOutput = txtMinOutput.Text
            End If
        End If
        If txtMaxOne.Text <> "" Then
            If txtMaxOne.Text > 100 Then
                MsgBox("Maximum aantal keer 1 kan niet meer dan 100% van je aantal oefeningen zijn.", MsgBoxStyle.Exclamation, "Maximum aantal keer 1")
                blnExit = True
                txtMaxOne.Text = 100
            End If
            sngMaxOne = txtMaxOne.Text / 100
        End If
        If txtMinOne.Text <> "" Then
            If txtMinOne.Text / 100 > sngMaxOne Then
                MsgBox("Maximaal aantal keer 1 is lager dan het minimale aantal keer 1. Controleer de instellingen onder ""Ingesteld"" en het maximale aantal keer 1 onder ""Geavanceerd"".", _
                        MsgBoxStyle.Exclamation, "Maximum kleiner dan minimum")
                blnExit = True
                Return 0
                Exit Function
            Else
                If txtMinOne.Text > 100 Then
                    MsgBox("Minimum aantal keer 1 kan niet meer dan 100% van je aantal oefeningen zijn.", MsgBoxStyle.Exclamation, "Minimum aantal keer 1")
                    blnExit = True
                    txtMinOne.Text = 100
                End If
            End If
            sngMinOne = txtMinOne.Text / 100
        End If
        If chkNoZero.Checked = True And sngMinOutput = 0 Then
            sngMinOutput = 1
        End If
        Return 0
    End Function

    Function Settings(ByRef sngMaxOutput As Single, ByRef sngMinOutput As Single, ByRef sngMaxOne As Single, ByRef sngMinOne As Single)
        Select Case True
            Case rdoEasy.Checked
                sngMaxOutput = 10
                sngMinOutput = 0
                sngMaxOne = 0.2
                sngMinOne = 0.1
            Case rdoMedium.Checked
                sngMaxOutput = 60
                sngMinOutput = 10
                sngMaxOne = 0.1
                sngMinOne = 0.05
            Case rdoHard.Checked
                sngMaxOutput = 100
                sngMinOutput = 60
                sngMaxOne = 0.05
                sngMinOne = 0
            Case Else
                rdoEasy.Checked = True
                sngMaxOutput = 10
                sngMinOutput = 0
                sngMaxOne = 0.2
                sngMinOne = 0.1
        End Select
        Return 0
    End Function
#End Region

#Region "Session"
    Function Product(ByRef sngNumber() As Single, ByRef sngMaxOutput As Single, ByRef sngMinOutput As Single, ByRef sngMinOne As Single, ByRef sngMaxOne As Single, ByRef sngSolution As Single, _
                     ByRef intQuantityOfNums As Integer, ByRef _strType As String, ByRef strDisplayExercise As String, ByRef strValidation As String, ByRef intExerciseTimesOne() As Integer, _
                     ByRef strShowExerciseTimeOne As String, ByRef sngPercentTimesOne As Single, ByRef intRandomExerciseNumber As Integer, ByRef intQuantityOfExcercises As Integer, _
                     ByRef intMaxQuantity As Integer, ByRef i As Integer, ByRef j As Integer, ByRef blnFirstLoop As Boolean, ByRef blnDevMode As Boolean)
        'Bepaal welke oefeningen maal 1 hebben.
        If blnFirstLoop = True Then
            ReDim intExerciseTimesOne(0 To _Random.Next((sngMinOne * intQuantityOfExcercises) - 1, (sngMaxOne * intQuantityOfExcercises)))
            For j = 0 To intExerciseTimesOne.Length - 1
                blnFirstLoop = False
                Do Until intRandomExerciseNumber <> 0 And Array.IndexOf(intExerciseTimesOne, intRandomExerciseNumber) = -1
                    If Array.IndexOf(intExerciseTimesOne, intRandomExerciseNumber) <> -1 Or intRandomExerciseNumber = 0 Then
                        intRandomExerciseNumber = _Random.Next(intQuantityOfExcercises) + 1
                    End If
                Loop
                intExerciseTimesOne(j) = intRandomExerciseNumber
            Next
            Array.Sort(intExerciseTimesOne)
            j = 0
            If blnDevMode = True Then
                For j = 0 To intExerciseTimesOne.Length - 1
                    If j = intExerciseTimesOne.Length - 1 Then
                        strShowExerciseTimeOne += intExerciseTimesOne(j).ToString
                    Else
                        strShowExerciseTimeOne += intExerciseTimesOne(j).ToString & ", "
                    End If
                Next
                j = 0
                MsgBox("Volgende oefeningen hebben de nummer 1: " & strShowExerciseTimeOne)
            End If
        End If
        'End

        'Oefeningen worden gecreëerd.
        sngMaxOne += 1 / intMaxQuantity
        sngPercentTimesOne = 1 / intQuantityOfExcercises
        If sngMaxOne < sngPercentTimesOne Then
            Do While sngSolution > sngMaxOutput Or strValidation = strDisplayExercise Or sngSolution < sngMinOutput
                'Solution = 1 zodat de uitkomst niet telkens 0 is.
                sngSolution = 1
                strDisplayExercise = ""
                'Creëeren van inhoud en valideren.
                While intQuantityOfNums <> 0
                    intQuantityOfNums -= 1
                    sngNumber(intQuantityOfNums) = 1
                    While sngNumber(intQuantityOfNums) = 1
                        sngNumber(intQuantityOfNums) = _Random.Next(sngMaxOutput + 1)
                    End While
                    sngSolution *= sngNumber(intQuantityOfNums)
                    If intQuantityOfNums <> 0 Then
                        strDisplayExercise += sngNumber(intQuantityOfNums) & _strType
                    Else
                        strDisplayExercise += sngNumber(intQuantityOfNums) & " = "
                    End If
                End While
                'Herdefiniëren van intQuantityOfNums.
                intQuantityOfNums = txtQuantityOfNums.Text
            Loop
        Else
            Do While sngSolution > sngMaxOutput Or strValidation = strDisplayExercise Or sngSolution < sngMinOutput
                'Solution = 1 zodat de uitkomst niet telkens 0 is.
                sngSolution = 1
                strDisplayExercise = ""
                'Creëeren van inhoud en valideren.
                While intQuantityOfNums <> 0
                    intQuantityOfNums -= 1
                    sngNumber(intQuantityOfNums) = _Random.Next(sngMaxOutput + 1)
                    sngSolution *= sngNumber(intQuantityOfNums)
                    If intQuantityOfNums <> 0 Then
                        strDisplayExercise += sngNumber(intQuantityOfNums) & _strType
                    Else
                        strDisplayExercise += sngNumber(intQuantityOfNums) & " = "
                    End If
                End While
                'Herdefiniëren van intQuantityOfNums.
                intQuantityOfNums = txtQuantityOfNums.Text
                'Er moet een 1 zijn.
                If i = intExerciseTimesOne(j) Then
                    sngNumber(_Random.Next(0, intQuantityOfNums)) = 1
                    'Creëeren van inhoud en valideren.
                    sngSolution = 1
                    strDisplayExercise = ""
                    While intQuantityOfNums <> 0
                        intQuantityOfNums -= 1
                        sngSolution *= sngNumber(intQuantityOfNums)
                        If intQuantityOfNums <> 0 Then
                            strDisplayExercise += sngNumber(intQuantityOfNums) & _strType
                        Else
                            strDisplayExercise += sngNumber(intQuantityOfNums) & " = "
                        End If
                    End While
                    'Herdefiniëren van intQuantityOfNums.
                    intQuantityOfNums = txtQuantityOfNums.Text
                ElseIf Array.IndexOf(sngNumber, Convert.ToSingle(1)) <> -1 Then
                    'Creëeren van inhoud en valideren.
                    sngSolution = 1
                    strDisplayExercise = ""
                    While intQuantityOfNums <> 0
                        intQuantityOfNums -= 1
                        sngSolution *= sngNumber(intQuantityOfNums)
                        If intQuantityOfNums <> 0 Then
                            strDisplayExercise += sngNumber(intQuantityOfNums) & _strType
                        Else
                            strDisplayExercise += sngNumber(intQuantityOfNums) & " = "
                        End If
                    End While
                    'Herdefiniëren van intQuantityOfNums.
                    intQuantityOfNums = txtQuantityOfNums.Text
                End If
                If (sngSolution > sngMaxOutput Or strValidation = strDisplayExercise Or sngSolution < sngMinOutput) = False And Array.IndexOf(sngNumber, Convert.ToSingle(1)) <> -1 And _
                    intExerciseTimesOne.Length - 1 <> j Then
                    j += 1
                    sngMaxOne -= sngPercentTimesOne
                End If
            Loop
            strValidation = strDisplayExercise
        End If
        'Sorteren voor strDisplay.
        Array.Reverse(sngNumber)
        'End
        Return 0
    End Function

    Function Quotiënt(ByRef sngNumber() As Single, ByRef sngMaxOutput As Single, ByRef sngMinOutput As Single, ByRef sngSolution As Single, ByRef intQuantityOfNums As Integer, _
                      ByRef _strType As String, ByRef strDisplayExercise As String, ByRef strValidation As String, ByRef intLooped As Integer, ByRef intHiLoRnd As Integer)
        Dim sngDivider As Single = _Random.Next(1, sngMaxOutput * 0.5 + 1)

        While sngSolution > sngMaxOutput Or strValidation = strDisplayExercise 'Or sngSolution < sngMinOutput
            sngSolution = 0
            'Creëeren van inhoud en valideren.
            While intQuantityOfNums <> 0
                intQuantityOfNums -= 1
                If intQuantityOfNums = txtQuantityOfNums.Text - 1 Then
                    'Genereer een eerste getal
                    'sngNumber(intQuantityOfNums) = _Random.Next(Math.Round(sngMaxOutput * (_Random.Next(50, 101) / 100)), sngMaxOutput + 1)
                    sngNumber(intQuantityOfNums) = sngMaxOutput + 1
                    While sngNumber(intQuantityOfNums) > sngMaxOutput
                        sngNumber(intQuantityOfNums) = _Random.Next(sngMaxOutput * (_Random.Next(50, 101) / 100), sngMaxOutput + 1) / sngDivider
                        sngNumber(intQuantityOfNums) = Math.Round(Convert.ToDecimal(sngNumber(intQuantityOfNums))) * Math.Round(sngDivider)
                        If sngNumber(intQuantityOfNums) = 0 Then
                            sngDivider = sngDivider * sngMaxOutput
                        End If
                        sngDivider *= 0.9
                    End While
                Else
                    'Hi Lo of Rnd
                    intHiLoRnd = _Random.Next(0, 3)
                    'Zorg voor 50% minder kans op Hi en geen kans bij intQuantityOfNums = 2
                    If intHiLoRnd = 0 Then
                        If intQuantityOfNums = 2 Then
                            intHiLoRnd = _Random.Next(1, 3)
                        Else
                            intHiLoRnd = _Random.Next(0, 3)
                        End If
                    End If
                    Select Case intHiLoRnd
                        Case 0
                            sngNumber(intQuantityOfNums) = sngNumber(intQuantityOfNums + 1)
                        Case 1
                            If sngNumber(intQuantityOfNums + 1) = 1 Then
                                sngNumber(intQuantityOfNums) = 1
                            Else
                                For i = sngNumber(intQuantityOfNums + 1) - 1 To 1 Step -1
                                    sngNumber(intQuantityOfNums) = i
                                    If sngNumber(intQuantityOfNums + 1) Mod sngNumber(intQuantityOfNums) = 0 Then
                                        i = 1
                                    End If
                                Next
                            End If
                        Case 2
                            intLooped = 0
                            While (sngNumber(intQuantityOfNums + 1) Mod sngNumber(intQuantityOfNums)) <> 0
                                intLooped += 1
                                If sngNumber(intQuantityOfNums + 1) = 1 Or intLooped / 2 = sngNumber(intQuantityOfNums + 1) Then
                                    sngNumber(intQuantityOfNums) = 1
                                Else
                                    sngNumber(intQuantityOfNums) = _Random.Next(sngNumber(intQuantityOfNums + 1) / 9, sngNumber(intQuantityOfNums + 1) / 2 + 1)
                                End If
                            End While
                    End Select
                    'Dit zorgt ervoor dat er pas een 1 is op het eind.
                    If intQuantityOfNums <> 0 And sngNumber(intQuantityOfNums) = 1 Then
                        sngNumber(intQuantityOfNums) = sngNumber(intQuantityOfNums + 1)
                    End If
                End If
            End While
            'Sorteren zodat er geen te kleine uikomsten zijn.
            Array.Sort(sngNumber)
            Array.Reverse(sngNumber)
            'Solution berekenen.
            intQuantityOfNums = txtQuantityOfNums.Text
            strDisplayExercise = ""
            For i = 0 To intQuantityOfNums - 1
                If i <> intQuantityOfNums - 1 Then
                    strDisplayExercise += sngNumber(i) & _strType
                Else
                    strDisplayExercise += sngNumber(i) & " = "
                End If
                If i = 0 Then
                    sngSolution = sngNumber(i)
                Else
                    sngSolution /= sngNumber(i)
                End If
            Next
        End While
        strValidation = strDisplayExercise
        'Sorteren voor strDisplay.
        Array.Reverse(sngNumber)
        'End
        Return 0
    End Function

    Function Verschil(ByRef sngNumber() As Single, ByRef sngMaxOutput As Single, ByRef sngMinOutput As Single, ByRef sngSolution As Single, ByRef intQuantityOfNums As Integer, _
                      ByRef _strType As String, ByRef strDisplayExercise As String, ByRef strValidation As String, ByRef intDifference As Integer)
        Do While sngSolution > sngMaxOutput Or strValidation = strDisplayExercise Or sngSolution < sngMinOutput
            sngSolution = 0
            'Creëeren van inhoud en valideren.
            While intQuantityOfNums <> 0
                intQuantityOfNums -= 1
                If intQuantityOfNums = txtQuantityOfNums.Text - 1 Then
                    sngNumber(intQuantityOfNums) = _Random.Next(sngMaxOutput * 8, sngMaxOutput * 10 + 1)
                Else
                    intDifference = Math.Round(sngNumber(intQuantityOfNums + 1) - sngMaxOutput / 1.5, 0)
                    If intDifference < 0 Then
                        intDifference = 0
                    End If
                    sngNumber(intQuantityOfNums) = _Random.Next(intDifference / 2, intDifference)
                End If
            End While
            'Sorteren zodat er geen negatieve uikomsten zijn.
            Array.Sort(sngNumber)
            Array.Reverse(sngNumber)
            'Solution berekenen.
            intQuantityOfNums = txtQuantityOfNums.Text
            strDisplayExercise = ""
            For i = 0 To intQuantityOfNums - 1
                If i <> intQuantityOfNums - 1 Then
                    strDisplayExercise += sngNumber(i) & _strType
                Else
                    strDisplayExercise += sngNumber(i) & " = "
                End If
                If i = 0 Then
                    sngSolution = sngNumber(i)
                Else
                    sngSolution -= sngNumber(i)
                End If
            Next
            intQuantityOfNums = txtQuantityOfNums.Text
            'Sorteren voor strDisplay.
            Array.Sort(sngNumber)
        Loop
        strValidation = strDisplayExercise
        Return 0
    End Function

    Function Som(ByRef sngNumber() As Single, ByRef sngMaxOutput As Single, ByRef sngMinOutput As Single, ByRef sngSolution As Single, ByRef intQuantityOfNums As Integer, _
                      ByRef _strType As String, ByRef strDisplayExercise As String, ByRef strValidation As String)
        Do While sngSolution > sngMaxOutput Or strValidation = strDisplayExercise Or sngSolution < sngMinOutput
            sngSolution = 0
            'Creëeren van inhoud en valideren.
            strDisplayExercise = ""
            While intQuantityOfNums <> 0
                intQuantityOfNums -= 1
                sngNumber(intQuantityOfNums) = _Random.Next(sngMaxOutput + 1)
                sngSolution += sngNumber(intQuantityOfNums)
                If intQuantityOfNums <> 0 Then
                    strDisplayExercise += sngNumber(intQuantityOfNums) & _strType
                Else
                    strDisplayExercise += sngNumber(intQuantityOfNums) & " = "
                End If
            End While
            'Herdefiniëren van intQuantityOfNums.
            intQuantityOfNums = txtQuantityOfNums.Text
        Loop
        strValidation = strDisplayExercise
        Return 0
    End Function
#End Region

#Region "Activate cmdClear"
    Private Sub RadioButton_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdoEasy.CheckedChanged, rdoMedium.CheckedChanged, rdoHard.CheckedChanged
        cmdClear.Visible = True
    End Sub

    Private Sub CheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles chkNoZero.CheckedChanged
        cmdClear.Visible = True
    End Sub

    Private Sub TextBox_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtMaxOutput.TextChanged, txtMinOutput.TextChanged, txtMaxOne.TextChanged, txtMinOne.TextChanged, _
        txtQuantityOfNums.TextChanged
        cmdClear.Visible = True
    End Sub
#End Region

#Region "Manage Evaluation.txt"
    Private Sub cmdShowEvaluations_Click(sender As System.Object, e As System.EventArgs) Handles cmdShowEvaluations.Click
        Try
            rtbCorrection.Text = My.Computer.FileSystem.ReadAllText(_strDoc & "Evaluation.txt")
            rtbCorrection.ScrollBars = ScrollBars.Vertical
            rtbCorrection.Visible = True
            cmdDeleteEvaluation.Visible = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Interne fout")
            My.Computer.FileSystem.WriteAllText(_strDir & "\logs.txt", My.Computer.Clock.LocalTime & vbCrLf & "Message: " & ex.Message & vbCrLf & "Stack trace: " & ex.StackTrace & vbCrLf & "Version: " & _
                                                My.Application.Info.Version.ToString & vbCrLf & vbCrLf & "_" & vbCrLf, True)
        End Try
    End Sub

    Private Sub cmdDeleteEvaluation_Click(sender As System.Object, e As System.EventArgs) Handles cmdDeleteEvaluation.Click
        Try
            If MsgBox("Ben je zeker dat je alle prestaties wilt verwijderen?" & vbCrLf & "Eenmaal alles verwijdert is zijn je gegevens onherstelbaar.", MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation, _
                      "Prestaties verwijderen") = MsgBoxResult.Yes Then
                My.Computer.FileSystem.WriteAllText(_strDoc & "Evaluation.txt", "", False)
                My.Computer.Audio.Play(My.Resources.SoundDeleting, AudioPlayMode.Background)
                rtbCorrection.ScrollBars = ScrollBars.None
                rtbCorrection.Visible = False
                rtbCorrection.Text = "Correcties komen hier te staan..."
                cmdDeleteEvaluation.Visible = False
                cmdShowEvaluations.Visible = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Interne fout")
            My.Computer.FileSystem.WriteAllText(_strDir & "\logs.txt", My.Computer.Clock.LocalTime & vbCrLf & "Message: " & ex.Message & vbCrLf & "Stack trace: " & ex.StackTrace & vbCrLf & "Version: " & _
                                                My.Application.Info.Version.ToString & vbCrLf & vbCrLf & "_" & vbCrLf, True)
        End Try
    End Sub
#End Region

#Region "Profile"
    Private Sub cmdNewUser_Click(sender As System.Object, e As System.EventArgs) Handles cmdNewUser.Click
        If txtUsername.Text = "" Then
            MsgBox("Vul een gebruikersnaam in.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Vul gebruikersnaam in")
            Exit Sub
        ElseIf txtAge.Text = "" Then
            MsgBox("Vul je leeftijd in.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Vul leeftijd in")
        End If

        My.Settings.Age = txtAge.Text
        My.Settings.Username = txtUsername.Text
    End Sub
#End Region

#Region "Dev tools"
    Private Sub tmrClicks_Tick(sender As Object, e As EventArgs) Handles tmrClicks.Tick
        _intTimesTick += 1
        If _intTimesClick >= 3 Then
            _intTimesClick = 0
            tmrClicks.Stop()
            If _blnDevMode = False Then
                _blnDevMode = True
                My.Settings.DevMode = True
                cmdTestMode.BackColor = Color.Red
                cmdTestMode.Visible = True
            Else
                _blnDevMode = False
                My.Settings.DevMode = False
                cmdTestMode.Visible = False
            End If
        End If
        If _intTimesTick = 3 Then
            tmrClicks.Stop()
            _intTimesTick = 0
            _intTimesClick = 0
        End If
    End Sub

    Private Sub cmdTestMode_Click(sender As Object, e As EventArgs) Handles cmdTestMode.Click
        If _blnTesting = False Then
            cmdTestMode.BackColor = Color.Green
            _blnTesting = True
        Else
            cmdTestMode.BackColor = Color.Red
            _blnTesting = False
        End If
    End Sub
#End Region

#Region "Browser"
    Private Sub webInfo_DocumentCompleted(sender As Object, e As WebBrowserDocumentCompletedEventArgs) Handles webInfo.DocumentCompleted
        Me.webInfo.Document.Body.Style = "zoom: 80%"
    End Sub

    Private Sub cmdCalcRules_Click(sender As Object, e As EventArgs) Handles cmdCalcRules.Click
        Try
            cmdCalcRules.Enabled = False
            frmCalcRules.ShowDialog()
            cmdCalcRules.Enabled = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Interne fout")
            My.Computer.FileSystem.WriteAllText(_strDir & "\logs.txt", My.Computer.Clock.LocalTime & vbCrLf & "Message: " & ex.Message & vbCrLf & "Stack trace: " & ex.StackTrace & vbCrLf & "Version: " & _
                                                My.Application.Info.Version.ToString & vbCrLf & vbCrLf & "_" & vbCrLf, True)
        End Try
    End Sub
#End Region

#Region "PlayClickSound"
    Private Sub PlayClickSound(sender As Object, e As EventArgs) Handles cmdCalcRules.Click, cmdClear.Click, cmdDeleteEvaluation.Click, cmdNewUser.Click, cmdShowEvaluations.Click, cmdStart.Click, _
        cmdTestMode.Click
        My.Computer.Audio.Play(My.Resources.SoundClick, AudioPlayMode.Background)
    End Sub
#End Region

#Region "TabControl"
    Private Sub tctrlType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tctrlType.SelectedIndexChanged
        My.Computer.Audio.Play(My.Resources.SoundClick, AudioPlayMode.Background)
        LayoutForEachType()
    End Sub

    Function LayoutForEachType()
        lblSelected.Parent = tctrlType.TabPages(tctrlType.SelectedIndex)
        lblShowType.Parent = tctrlType.TabPages(tctrlType.SelectedIndex)
        lblSymbol.Parent = tctrlType.TabPages(tctrlType.SelectedIndex)
        grpDifficulty.Parent = tctrlType.TabPages(tctrlType.SelectedIndex)
        grpAdvanced.Parent = tctrlType.TabPages(tctrlType.SelectedIndex)
        'cmdClear
        cmdClear.Parent = tctrlType.TabPages(tctrlType.SelectedIndex)
        'cmdTestMode
        cmdTestMode.Parent = tctrlType.TabPages(tctrlType.SelectedIndex)

        Select Case tctrlType.SelectedTab.Name
            Case "tpgSum"
                MinMaxOne(False)
                txtQuantityOfNums.Enabled = True
                lblQuantityOfNums.Enabled = True
                _strType = " + "
                lblSymbol.Text = "+"
                lblShowType.Text = "oefeningen op plus"
            Case "tpgDifference"
                MinMaxOne(False)
                txtQuantityOfNums.Enabled = True
                lblQuantityOfNums.Enabled = True
                _strType = " - "
                lblSymbol.Text = "-"
                lblShowType.Text = "oefeningen op min"
            Case "tpgProduct"
                MinMaxOne(True)
                txtQuantityOfNums.Enabled = True
                lblQuantityOfNums.Enabled = True
                _strType = " x "
                lblSymbol.Text = "x"
                lblShowType.Text = "oefeningen op maal"
            Case "tpgQuotient"
                MinMaxOne(False)
                txtQuantityOfNums.Enabled = False
                lblQuantityOfNums.Enabled = False
                _strType = " : "
                lblSymbol.Text = ":"
                lblShowType.Text = "oefeningen op gedeelt door"
        End Select

        Return 0
    End Function

    Function MinMaxOne(ByVal blnEnable As Boolean)
        If blnEnable = True Then
            lblMinOne.Enabled = True
            lblMaxOne.Enabled = True
            txtMinOne.Enabled = True
            txtMaxOne.Enabled = True
            Hulp.SetToolTip(rdoEasy, _
                            "- Maximale uitkomst = 20" & vbCrLf & _
                            "- Minimale uitkomst = 0" & vbCrLf & _
                            "- Aantal keer 1 (maximum) = 20 (%)" & vbCrLf & _
                            "- Aantal keer 1 (minimum) = 10 (%)")
            Hulp.SetToolTip(rdoMedium, _
                            "- Maximale uitkomst = 60" & vbCrLf & _
                            "- Minimale uitkomst = 10" & vbCrLf & _
                            "- Aantal keer 1 (maximum) = 10 (%)" & vbCrLf & _
                            "- Aantal keer 1 (minimum) = 5 (%)")
            Hulp.SetToolTip(rdoHard, _
                            "- Maximale uitkomst = 100" & vbCrLf & _
                            "- Minimale uitkomst = 60" & vbCrLf & _
                            "- Aantal keer 1 (maximum) = 5 (%)" & vbCrLf & _
                            "- Aantal keer 1 (minimum) = 0 (%)")
        Else
            lblMinOne.Enabled = False
            lblMaxOne.Enabled = False
            txtMinOne.Enabled = False
            txtMaxOne.Enabled = False
            Hulp.SetToolTip(rdoEasy, _
                            "- Maximale uitkomst = 20" & vbCrLf & _
                            "- Minimale uitkomst = 0")
            Hulp.SetToolTip(rdoMedium, _
                            "- Maximale uitkomst = 60" & vbCrLf & _
                            "- Minimale uitkomst = 10")
            Hulp.SetToolTip(rdoHard, _
                            "- Maximale uitkomst = 100" & vbCrLf & _
                            "- Minimale uitkomst = 60")
        End If

        Return 0
    End Function
#End Region
End Class

#Region "ToDo V1"
'locaal in quotient
'LoHi
'intLooped

'!!!!!!!!!!!!!!!!!!!!!!!!
'!!! PAS Info.htm AAN !!!
'!!! PAS TABINDEX AAN !!!
'!!!!!!!!!!!!!!!!!!!!!!!!

'Gepland v1:

'verander groupbox geavanceerd in tabs, maal, plus, ... zodat vast staat over wat voor type oefening het gaat (dan moet er niet voortdeurend aan aanpassingen gedaan worden)

'Gemiddeld: max 100 min 10  3 getallen                  (geen minimum bij quotiënt)
'Moeilijk: max 1000 min 100 3 getallen  tekens gemixt   (geen minimum bij quotiënt)

'session cancelt op alt f4
'Aantal keer 0.
'alle oefeningen zonder nul op alles laten werken
'extra txtbox' zodat bij bevoorbeeld nooit 0 niet 2 kaar een waarde 0 hoeft ingetypt worden
'txtbox'
''1ste: min factoren
''2de: max factoren
'tekens gemixt in 1 oefening
'met komma of zonder
'haakjes
'Betere documentatie voor rekenregels, dus op niveau in plaats van alles in 1 en afgewerkt
'currency herbekijken
'GUI verbeteren, positie van knoppen, achtergronden, ...

'!!!Betere code!!!
'benamingen bekijken
'functie voor rtbCorrection
'bekijk functies in session voor tijdelijke variabelen die nergens anders worden gebruikt.

'!!!Bugs!!!
'quotiënt: zorg voor een betere divider en check voor 0 als deler
'quotiënt: er wordt enkel rekenening gehouden met vorig getal modulo getal en niet alles daarvoor
#End Region

#Region "Optioneel"
'Optioneel:

'- Nog geavanceerder gaan qua validatie. I.p.v. een string van de display van de vorige oefening te gebruiken om te valideren, een array gebruiken en in een loop alles valideren.
'Als alles is gebruikt de array legen om uit de loop te springen zodat de oefening niet vast zit.

'- Maximaliseren toelaten.
'- Records bijhouden bij naam of niet (map aanmaken of niet) als *Apllication.Settings.CreateMap*.
'- Achtergrond met doorzichtige tools.
'- Muziek wanneer het programma inactief is.
'- Geluiden afspelen bij bepaalde events zoals 100% behalen.
'- Eigen GUI.
'- Opties voor GUI in een control panel zoals opacity background colors en images, misschien geluiden.
'- Kwadraat, logaritme en vierkantswortel toevoegen in de documentatie.
#End Region