﻿using System.Drawing;

namespace Rekenen
{
  partial class RecordsWindow
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.Body = new System.Windows.Forms.Panel();
      this._toolTip = new System.Windows.Forms.ToolTip(this.components);
      this.SuspendLayout();
      // 
      // Body
      // 
      this.Body.AutoScroll = true;
      this.Body.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.Body.Dock = System.Windows.Forms.DockStyle.Fill;
      this.Body.Location = new System.Drawing.Point(0, 0);
      this.Body.Name = "Body";
      this.Body.Size = new System.Drawing.Size(392, 272);
      this.Body.TabIndex = 0;
      // 
      // _toolTip
      // 
      this._toolTip.AutomaticDelay = 0;
      this._toolTip.ShowAlways = true;
      this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
      this._toolTip.ToolTipTitle = "Extra";
      this._toolTip.UseAnimation = false;
      this._toolTip.UseFading = false;
      // 
      // RecordsWindow
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(392, 272);
      this.Controls.Add(this.Body);
      this.Icon = global::Rekenen.Properties.Resources.DefaultIcon;
      this.MinimumSize = new System.Drawing.Size(400, 28);
      this.Name = "RecordsWindow";
      this.Text = "Eigen records";
      this.Shown += new System.EventHandler(this.RecordsWindow_Shown);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel Body;
    private System.Windows.Forms.ToolTip _toolTip;
  }
}