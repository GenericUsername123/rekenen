﻿// ReSharper disable MemberCanBeProtected.Global
// ReSharper disable UnusedMember.Global

namespace Session
{
  public class DefinedTable<T>
  {
    private readonly byte _dim2;
    private readonly SessionSetup _sessionSetup;
    private T[][] _table;

    public DefinedTable(SessionSetup sessionSetup, byte dim2)
    {
      _sessionSetup = sessionSetup;
      _table = new T[sessionSetup.Exercises][];
      _dim2 = dim2;
      for (byte e = 0; e < sessionSetup.Exercises; e++) _table[e] = new T[dim2];
    }

    public int Length => _table.Length;
    public T[] this[byte index] => _table[index];

    public void SetAll(byte startIndex, T item)
    {
      for (byte e = 1; e <= _table.Length && e != 0; e++) SetRanges(startIndex, item, e);
    }

    public void SetAll(byte startIndex, params T[] items)
    {
      for (byte e = 1; e <= _table.Length && e != 0; e++) SetRanges(startIndex, items, e);
    }

    public void SetRanges(byte startIndex, T item, params byte[] exercises)
    {
      var items = new T[_dim2];
      for (byte i = 0; i < _dim2; i++) items[i] = item;
      SetRanges(startIndex, items, exercises);
    }

    public void SetRanges(byte startIndex, T[] items, params byte[] exercises)
    {
      for (var e = 0; e < exercises?.Length; e++)
      {
        if (exercises[e] > _table.Length || exercises[e] == 0) continue;
        ClearRanges(exercises[e]);
        for (var i = 0; i < items?.Length; i++)
        {
          if (IsIgnored(items[i])) continue;
          _table[exercises[e] - 1][startIndex + i] = items[i];
        }
      }
    }

    protected virtual bool IsIgnored(T item)
    {
      return item.Equals(default(T));
    }

    public void ClearRanges(params byte[] exercises)
    {
      for (var e = 0; e < exercises?.Length; e++)
      {
        if (exercises[e] > _table.Length || exercises[e] == 0) continue;
        for (byte i = 0; i < _table[exercises[e] - 1].Length; i++) _table[exercises[e] - 1][i] = default(T);
      }
    }

    public void Update()
    {
      var table = new T[_sessionSetup.Exercises][];
      for (byte e = 0; e < _sessionSetup.Exercises; e++)
      {
        table[e] = new T[_dim2];
        if (e >= _table.Length) continue;
        for (byte i = 0; i < _table[e].Length; i++) table[e][i] = _table[e][i];
      }
      _table = table;
    }
  }
}