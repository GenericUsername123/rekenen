﻿using System;
using System.Threading;
using System.Windows.Forms;
using Session;

namespace GuiTools
{
  public static class KeepToFormat
  {
    public static void Initialize(TextBox textBox, object format)
    {
      var asNumber = format as Number;
      var asText = format as Text;
      if (asNumber == null && asText == null) throw new ArgumentException();
      if (asNumber != null) textBox.KeyDown += (sender, e) => textBox_KeyDown(textBox, e, asNumber);
      else textBox.KeyDown += (sender, e) => textBox_KeyDown(e, asText);
    }

    private static void textBox_KeyDown(TextBox tb, KeyEventArgs e, Number asNumber)
    {
      if (Ignore(e)) return;
      if (e.KeyCode == Keys.V && e.Control) Paste(tb, e, asNumber);
      if (e.KeyValue > 36 && e.KeyValue < 41) return;
      if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
      {
        Delete(tb, e, asNumber);
        return;
      }
      var comma = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
      bool isComma;
      if (comma == ',')
      {
        if (e.KeyData == Keys.Decimal)
        {
          SendKeys.Send(",");
          e.SuppressKeyPress = true;
          return;
        }
        isComma = e.KeyValue == 188;
      }
      else
      {
        if (e.KeyValue == 188)
        {
          SendKeys.Send(".");
          e.SuppressKeyPress = true;
          return;
        }
        isComma = e.KeyData == Keys.Decimal;
      }
      var text = tb.Text == tb.SelectedText ? "" : tb.Text;
      var commaIndex = text.IndexOf(comma);
      var isNegative = e.KeyValue == 189 || e.KeyValue == 109;
      var isCapital = Control.IsKeyLocked(Keys.CapsLock) || e.Modifiers == Keys.Shift;
      var isDigit = e.KeyValue <= 105 && e.KeyValue >= 96 || (e.KeyValue >= 48 && e.KeyValue <= 57 && isCapital);
      if (!(isDigit || isComma || isNegative) || isComma && asNumber.Precision == 0 ||
          isNegative && asNumber.LowerBound > 0 || isComma && commaIndex != -1 || tb.SelectionStart != 0 && isNegative)
      {
        e.SuppressKeyPress = true;
        return;
      }
      double value;
      if (asNumber.Precision != 0 && double.TryParse(text, out value))
      {
        if (NumericalOccurrences.HasPrecision(value) >= asNumber.Precision && tb.SelectionStart > commaIndex)
        {
          e.SuppressKeyPress = true;
          return;
        }
      }
      char temp;
      NativeMethods.TryParse(e.KeyCode, out temp);
      text = tb.Text.Remove(tb.SelectionStart, tb.SelectionLength) + temp;
      if (!double.TryParse(text, out value)) return;
      if (!(value < asNumber.LowerBound) && !(value > asNumber.UpperBound)) return;
      var selectPosition = tb.SelectionStart + 1;
      value = value < asNumber.LowerBound ? asNumber.LowerBound : asNumber.UpperBound;
      tb.Text = value + "";
      tb.SelectionStart = selectPosition;
      e.SuppressKeyPress = true;
    }

    private static void textBox_KeyDown(KeyEventArgs e, Text asText)
    {
      if (Ignore(e) || e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete) return;
      var isCapital = Control.IsKeyLocked(Keys.CapsLock) || e.Modifiers == Keys.Shift;
      var isLower = e.KeyValue > 64 && e.KeyValue < 91 && !isCapital;
      var isUpper = e.KeyValue > 64 && e.KeyValue < 91 && isCapital;
      var isDigit = e.KeyValue <= 105 && e.KeyValue >= 96 || (e.KeyValue >= 48 && e.KeyValue <= 57 && isCapital);
      e.SuppressKeyPress = isDigit && !asText.Numbers || isLower && !asText.LowerCase || isUpper && !asText.UpperCase;
      e.SuppressKeyPress = !isDigit && !isLower && !isUpper || e.SuppressKeyPress;
    }

    private static bool Ignore(KeyEventArgs e)
    {
      if (e.KeyData == (Keys.Menu | Keys.Alt) || e.Alt && e.KeyCode == Keys.F4 ||
          e.Control && (e.KeyCode == Keys.C || e.KeyCode == Keys.A)) return true;
      return e.KeyCode == Keys.Home || e.KeyCode == Keys.End || e.KeyCode == Keys.Insert;
    }

    private static void Paste(TextBox tb, KeyEventArgs e, Number asNumber)
    {
      e.SuppressKeyPress = true;
      double val;
      if (!double.TryParse(Clipboard.GetText(), out val)) return;
      val = Math.Round(val, asNumber.Precision);
      if (val < asNumber.LowerBound || asNumber.UpperBound < val) return;
      var tempStr = tb.SelectionLength == 0 ? tb.Text : tb.Text.Replace(tb.SelectedText, "");
      var str = tempStr.Substring(0, tb.SelectionStart) + val.ToString("0." + new string('#', 339));
      tempStr = str + tempStr.Substring(tb.SelectionStart);
      if (!double.TryParse(tempStr, out val)) return;
      if (val < asNumber.LowerBound || asNumber.UpperBound < val) return;
      tb.Text = tempStr;
      tb.SelectionStart = str.Length;
    }

    private static void Delete(TextBox tb, KeyEventArgs e, Number asNumber)
    {
      if (tb.SelectedText != tb.Text && tb.TextLength > 1) return;
      tb.Text = (asNumber.LowerBound > 0 ? asNumber.LowerBound : 0) + "";
      tb.SelectAll();
      e.SuppressKeyPress = true;
    }

    public class Number
    {
      public double LowerBound = long.MinValue;
      public byte Precision = byte.MaxValue;
      public double UpperBound = long.MaxValue;
    }

    // ReSharper disable once ClassNeverInstantiated.Global
    public class Text
    {
      public bool LowerCase = true;
      public bool Numbers = true;
      public bool UpperCase = true;
    }
  }
}