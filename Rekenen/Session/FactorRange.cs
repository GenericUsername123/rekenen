﻿using System.Runtime.CompilerServices;

namespace Session
{
  public class FactorRange
  {
    public const byte Limit = 99;
    private readonly byte[] _rangeFactors = new byte[2];

    public FactorRange()
    {
      SetAll(2, 2);
    }

    public byte LowerBound
    {
      [MethodImpl(MethodImplOptions.NoInlining)]
      get { return _rangeFactors[0]; }
      set
      {
        Cast(ref value);
        _rangeFactors[0] = value;
        if (value > UpperBound) UpperBound = value;
      }
    }

    public byte UpperBound
    {
      [MethodImpl(MethodImplOptions.NoInlining)]
      get { return _rangeFactors[1]; }
      set
      {
        Cast(ref value);
        _rangeFactors[1] = value;
        if (value < LowerBound) LowerBound = value;
      }
    }

    public void SetAll(byte value1, byte value2)
    {
      UpperBound = value1 > value2 ? value1 : value2;
      LowerBound = value1 > value2 ? value1 : value2;
    }

    private void Cast(ref byte value)
    {
      if (value < 2) value = 2;
      if (value > Limit) value = Limit;
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      return obj.GetType() == GetType() && Equals((FactorRange)obj);
    }

    protected bool Equals(FactorRange other)
    {
      return _rangeFactors.Equals(other._rangeFactors);
    }

    public override int GetHashCode()
    {
      return _rangeFactors.GetHashCode();
    }
  }
}