﻿Public Class frmError

    Private Sub frmError_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim intSupportedWidth As Integer = frmMain.Width
        Dim intSupportedHeight As Integer = frmMain.Height

        Me.Icon = My.Resources.DefaultIcon
        lblErrorMessage.Text += vbCrLf & "Ondersteund: " & intSupportedWidth & " x " & intSupportedHeight
        frmMain.Close()
    End Sub
End Class