﻿namespace Rekenen
{
  partial class SessionWindow
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.Next = new System.Windows.Forms.Button();
      this.Cancel = new System.Windows.Forms.Button();
      this.Exercise = new System.Windows.Forms.Label();
      this.AnswerLabel = new System.Windows.Forms.Label();
      this.Expected = new System.Windows.Forms.Label();
      this.Check = new System.Windows.Forms.Button();
      this.Answer = new System.Windows.Forms.TextBox();
      this.SuspendLayout();
      // 
      // Next
      // 
      this.Next.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.Next.Location = new System.Drawing.Point(151, 137);
      this.Next.Name = "Next";
      this.Next.Size = new System.Drawing.Size(75, 23);
      this.Next.TabIndex = 2;
      this.Next.Text = "Volgende";
      this.Next.UseVisualStyleBackColor = true;
      this.Next.Click += new System.EventHandler(this.Next_Click);
      // 
      // Cancel
      // 
      this.Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.Cancel.Location = new System.Drawing.Point(70, 137);
      this.Cancel.Name = "Cancel";
      this.Cancel.Size = new System.Drawing.Size(75, 23);
      this.Cancel.TabIndex = 3;
      this.Cancel.Text = "Stoppen";
      this.Cancel.UseVisualStyleBackColor = true;
      // 
      // Exercise
      // 
      this.Exercise.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.Exercise.Location = new System.Drawing.Point(0, 0);
      this.Exercise.Name = "Exercise";
      this.Exercise.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
      this.Exercise.Size = new System.Drawing.Size(292, 60);
      this.Exercise.TabIndex = 2;
      this.Exercise.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // AnswerLabel
      // 
      this.AnswerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.AnswerLabel.AutoSize = true;
      this.AnswerLabel.Location = new System.Drawing.Point(12, 61);
      this.AnswerLabel.Name = "AnswerLabel";
      this.AnswerLabel.Size = new System.Drawing.Size(55, 13);
      this.AnswerLabel.TabIndex = 4;
      this.AnswerLabel.Text = "Antwoord:";
      // 
      // Expected
      // 
      this.Expected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.Expected.Location = new System.Drawing.Point(0, 81);
      this.Expected.Name = "Expected";
      this.Expected.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
      this.Expected.Size = new System.Drawing.Size(292, 60);
      this.Expected.TabIndex = 5;
      this.Expected.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // Check
      // 
      this.Check.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.Check.Location = new System.Drawing.Point(205, 56);
      this.Check.Name = "Check";
      this.Check.Size = new System.Drawing.Size(75, 23);
      this.Check.TabIndex = 1;
      this.Check.Text = "Controleer";
      this.Check.UseVisualStyleBackColor = true;
      this.Check.Click += new System.EventHandler(this.Check_Click);
      // 
      // Answer
      // 
      this.Answer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.Answer.Location = new System.Drawing.Point(73, 58);
      this.Answer.MaxLength = 19;
      this.Answer.Name = "Answer";
      this.Answer.Size = new System.Drawing.Size(126, 20);
      this.Answer.TabIndex = 0;
      // 
      // SessionWindow
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(292, 172);
      this.Controls.Add(this.Answer);
      this.Controls.Add(this.Check);
      this.Controls.Add(this.Expected);
      this.Controls.Add(this.AnswerLabel);
      this.Controls.Add(this.Exercise);
      this.Controls.Add(this.Cancel);
      this.Controls.Add(this.Next);
      this.Icon = global::Rekenen.Properties.Resources.DefaultIcon;
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(500, 500);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(300, 200);
      this.Name = "SessionWindow";
      this.Text = "Oefeningen";
      this.Shown += new System.EventHandler(this.SessionWindow_Shown);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button Next;
    private System.Windows.Forms.Button Cancel;
    private System.Windows.Forms.Label Exercise;
    private System.Windows.Forms.Label AnswerLabel;
    private System.Windows.Forms.Label Expected;
    private System.Windows.Forms.Button Check;
    private System.Windows.Forms.TextBox Answer;
  }
}