﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using GuiTools;
using Rekenen.Properties;

namespace Rekenen
{
  public partial class AdvancedWindow : Form
  {
    private readonly bool _readOnly;
    private readonly Session.Session _session;

    public AdvancedWindow(Session.Session session, bool readOnly)
    {
      _readOnly = readOnly;
      _session = session;
      InitializeComponent();
      SuspendLayout();
      AdaptToCulture();
      var profiles = Helper.GetProfiles();
      if (profiles.Count != 0)
      {
        foreach (var profile in profiles) Profiles.Items.Add(profile);
        Profiles.SelectedIndex = 0;
      }
      ReadFromSession();
      AttachEvents();
    }

    private void AdaptToCulture()
    {
      var mutables = new Control[] { label1, label2, label3, SingleThread, Debug };
      var controls = Resources.SessionProperties.Substring(Resources.SessionProperties.LastIndexOf('\n') + 1)
        .Split('{', '}', '=')[2].Split(',');
      foreach (Control control in Controls)
      {
        for (var i = 0; i < mutables.Length; i++)
        {
          if (mutables[i].Name != control.Name) continue;
          mutables[i].Text = controls[i];
          break;
        }
      }
    }

    public event EventHandler Updated;

    #region Events

    private void ObserveInteraction(Control.ControlCollection controls)
    {
      for (var c = 0; c < controls.Count; c++)
      {
        controls[c].KeyDown += (sender, e) =>
        {
          if (e.KeyCode == Keys.Escape) Close();
        };
        if (controls[c] is GroupBox)
        {
          ObserveInteraction(controls[c].Controls);
          continue;
        }
        if (controls[c] is TextBox)
        {
          controls[c].KeyDown += ListenForClosure;
          controls[c].TextChanged += WriteToSession;
        }
        else
        {
          var checkBox = controls[c] as CheckBox;
          if (checkBox != null) checkBox.CheckStateChanged += WriteToSession;
        }
      }
    }

    private void ListenForClosure(object sender, KeyEventArgs e)
    {
      if (e.Alt && e.KeyCode == Keys.F4) Close();
    }

    private void AttachEvents()
    {
      if (Visible)
      {
        LocationChanged += AdvancedWindow_LocationChanged;
        Owner.LocationChanged += Owner_LocationChanged;
      }
      else
      {
        ObserveInteraction(Controls);
        KeepToFormat.Initialize(UniqueResults, new KeepToFormat.Number
        {
          LowerBound = 0,
          Precision = 0,
          UpperBound = 255
        });
        KeepToFormat.Initialize(Seed, new KeepToFormat.Number
        {
          LowerBound = 0,
          Precision = 0,
          UpperBound = int.MaxValue
        });
        KeepToFormat.Initialize(CycleTime, new KeepToFormat.Number
        {
          LowerBound = 0,
          Precision = 0,
          UpperBound = long.MaxValue
        });
      }
    }

    private void AdvancedWindow_Shown(object sender, EventArgs e)
    {
      if (_readOnly)
      {
        Save.Enabled = false;
        LoadConfig.Enabled = false;
        UniqueResults.ReadOnly = true;
        Seed.ReadOnly = true;
        CycleTime.ReadOnly = true;
        SingleThread.CheckedChanged += (sender2, e2) => ReadFromSession();
        Debug.CheckedChanged += (sender2, e2) => ReadFromSession();
      }
      AttachEvents();
      Owner.Left -= Width / 2;
      Location = new Point(Owner.Right, Owner.Top + (Owner.Height - Height) / 2);
      ResumeLayout();
    }

    private void AdvancedWindow_LocationChanged(object sender, EventArgs e)
    {
      Owner.Location = new Point(Left - Owner.Width, Top - (Owner.Height - Height) / 2);
    }

    private void Owner_LocationChanged(object sender, EventArgs e)
    {
      if (Owner == null) return;
      Location = new Point(Owner.Right, Owner.Top + (Owner.Height - Height) / 2);
    }

    private void Save_Click(object sender, EventArgs e)
    {
      if (Helper.Presets.Any(configName => configName == Profiles.Text))
      {
        MessageBox.Show(Resources.AdvancedWindow_ProfileNameNotValid, Resources.AdvancedWindow_NotValid,
          MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      if (!Directory.Exists(Helper.ConfigFolder)) Directory.CreateDirectory(Helper.ConfigFolder);
      File.WriteAllLines(Helper.ConfigFolder + "\\" + Profiles.Text + ".cfg", _session.Config);
      _session.Name = Profiles.Text;
    }

    private void Load_Click(object sender, EventArgs e)
    {
      if (!Directory.Exists(Helper.ConfigFolder)) return;
      if (!File.Exists(Helper.ConfigFolder + "\\" + Profiles.Text + ".cfg"))
      {
        MessageBox.Show(Resources.AdvancedWindow_ProfileDoesNotExist, Resources.AdvancedWindow_DoesNotExist,
          MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        return;
      }
      _session.LoadConfig(File.ReadAllLines(Helper.ConfigFolder + "\\" + Profiles.Text + ".cfg"));
      _session.Name = Profiles.Text;
      ReadFromSession();
      Updated?.Invoke(this, EventArgs.Empty);
    }

    #endregion

    #region Read/Write

    private void ReadFromSession()
    {
      UniqueResults.Text = _session.UniqueResults + "";
      Seed.Text = _session.Seed + "";
      CycleTime.Text = _session.CycleTime + "";
      SingleThread.Checked = _session.ThreadCount == 1;
      Debug.Checked = _session.Debug;
    }

    private void WriteToSession(object sender, EventArgs e)
    {
      RetrieveFromCheckBoxes(sender as CheckBox);
      RetrieveFromTextBoxes(sender as TextBox);
    }

    private void RetrieveFromCheckBoxes(Control checkBox)
    {
      if (checkBox == null) return;
      var name = checkBox.Name;
      if (name == SingleThread.Name)
        _session.ThreadCount = SingleThread.Checked ? (byte)1 : Session.Session.DefaultThreadCount;
      else if (name == Debug.Name) _session.Debug = Debug.Checked;
    }

    private void RetrieveFromTextBoxes(Control textBox)
    {
      if (textBox == null) return;
      var name = textBox.Name;
      if (textBox.Text == '-' + "") return;
      if (name == UniqueResults.Name) _session.UniqueResults = byte.Parse(UniqueResults.Text);
      else if (name == Seed.Name) _session.Seed = int.Parse(Seed.Text);
      else if (name == CycleTime.Name) _session.CycleTime = long.Parse(CycleTime.Text);
    }

    #endregion
  }
}