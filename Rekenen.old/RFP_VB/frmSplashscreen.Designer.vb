﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSplashscreen
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.lblPrgrmName = New System.Windows.Forms.Label()
    Me.lblBuild = New System.Windows.Forms.Label()
    Me.SuspendLayout()
    '
    'lblPrgrmName
    '
    Me.lblPrgrmName.BackColor = System.Drawing.Color.Transparent
    Me.lblPrgrmName.FlatStyle = System.Windows.Forms.FlatStyle.Flat
    Me.lblPrgrmName.Font = New System.Drawing.Font("Berlin Sans FB", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblPrgrmName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(25, Byte), Integer), CType(CType(30, Byte), Integer))
    Me.lblPrgrmName.Location = New System.Drawing.Point(12, 208)
    Me.lblPrgrmName.Name = "lblPrgrmName"
    Me.lblPrgrmName.Size = New System.Drawing.Size(653, 54)
    Me.lblPrgrmName.TabIndex = 4
    Me.lblPrgrmName.Text = "NuCalc"
    Me.lblPrgrmName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lblBuild
    '
    Me.lblBuild.BackColor = System.Drawing.Color.Transparent
    Me.lblBuild.FlatStyle = System.Windows.Forms.FlatStyle.Flat
    Me.lblBuild.Font = New System.Drawing.Font("Berlin Sans FB", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblBuild.ForeColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(25, Byte), Integer), CType(CType(30, Byte), Integer))
    Me.lblBuild.Location = New System.Drawing.Point(48, 262)
    Me.lblBuild.Name = "lblBuild"
    Me.lblBuild.Size = New System.Drawing.Size(740, 30)
    Me.lblBuild.TabIndex = 5
    Me.lblBuild.Text = "Versie: 1.0.0.0"
    Me.lblBuild.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'frmSplashscreen
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.BackgroundImage = Global.RFP_VB.My.Resources.Resources.Splashscreen_Background
    Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
    Me.ClientSize = New System.Drawing.Size(800, 500)
    Me.Controls.Add(Me.lblBuild)
    Me.Controls.Add(Me.lblPrgrmName)
    Me.DoubleBuffered = True
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
    Me.MaximizeBox = False
    Me.MaximumSize = New System.Drawing.Size(800, 500)
    Me.MinimizeBox = False
    Me.MinimumSize = New System.Drawing.Size(800, 500)
    Me.Name = "frmSplashscreen"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.ResumeLayout(False)

  End Sub
    Friend WithEvents lblPrgrmName As System.Windows.Forms.Label
    Friend WithEvents lblBuild As System.Windows.Forms.Label
End Class
