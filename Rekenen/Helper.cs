﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Rekenen.Properties;
using Session;

// ReSharper disable UnusedMember.Global

namespace Rekenen
{
  public static class Helper
  {
    public enum Preset
    {
      Easy = 0,
      Medium = 1,
      Hard = 2,
      Custom = 3
    }

    public const string Salt = "fg654gf5ds46gfer4g";
    public const string Password = "d65f46s54f5fs23fq65g5x9f74fdsf54";
    public const string ConfigFolder = ".\\Cfg";
    public const string RecordFile = "stats.encrypted";
    public static readonly IList<string> Presets = new List<string>(new[] { "Easy", "Medium", "Hard", "Custom" });

    public static IList<string> GetProfiles()
    {
      var profiles = new List<string>();
      if (!Directory.Exists(ConfigFolder)) return profiles;
      var fileNames = Directory.GetFiles(ConfigFolder);
      profiles.AddRange(fileNames.Select(t => t.Substring(t.LastIndexOf("\\", StringComparison.InvariantCulture) + 1))
        .Select(name => name.Remove(name.IndexOf(".cfg", StringComparison.InvariantCulture), ".cfg".Length))
        .Where(s => s != "Easy" || s != "Medium" || s != "Hard"));
      return profiles;
    }

    public static string Translate(string preset)
    {
      var presets = Presets;
      for (var i = 0; i < presets.Count; i++)
      {
        if (presets[i] != preset) continue;
        return Resources.MainWindow_Presets.Split(';')[i];
      }
      return preset;
    }

    public static Session.Session GetSession(Preset preset)
    {
      var session = new Session.Session
      {
        Exercises = 10,
        UniqueResults = 10,
        Symbols = { Symbols.Type.Sum, Symbols.Type.Difference },
        Debug = true
      };
      switch (preset)
      {
        case Preset.Medium:
          session.Symbols.AddRange(new[] { Symbols.Type.Product, Symbols.Type.Division });
          session.NumberRange.SetBoundaries(1e2);
          session.Exclusions.Absolute = true;
          session.Exclusions.AddRange(new[] { 0d, 1 });
          session.Exclusions.Occurrences.Initialize(.5, 1);
          break;
        case Preset.Hard:
          session.Precision = 1;
          session.Symbols.AddRange(new[] { Symbols.Type.Product, Symbols.Type.Division });
          session.FactorRange.SetAll(4, 3);
          session.NumberRange.SetBoundaries(1e2);
          session.NumberRange.SetNumbers(0, 1e1);
          session.Exclusions.Absolute = true;
          session.Exclusions.AddRange(new[] { 0d, 1 });
          session.Exclusions.Occurrences.Initialize(1, 1);
          break;
      }
      session.Name = preset.ToString();
      return session;
    }

    public static string MsToString(long timeSpent)
    {
      if (timeSpent / 1000 < 60) return timeSpent / 1000d + "s";
      return timeSpent / 60000 + "min " + timeSpent % 60000 / 1000d + "s";
    }
  }
}