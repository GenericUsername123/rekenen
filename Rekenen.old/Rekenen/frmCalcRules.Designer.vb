﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCalcRules
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.webDisplayCalcRules = New System.Windows.Forms.WebBrowser()
        Me.SuspendLayout()
        '
        'webDisplayCalcRules
        '
        Me.webDisplayCalcRules.Dock = System.Windows.Forms.DockStyle.Fill
        Me.webDisplayCalcRules.Location = New System.Drawing.Point(0, 0)
        Me.webDisplayCalcRules.MinimumSize = New System.Drawing.Size(20, 20)
        Me.webDisplayCalcRules.Name = "webDisplayCalcRules"
        Me.webDisplayCalcRules.Size = New System.Drawing.Size(284, 266)
        Me.webDisplayCalcRules.TabIndex = 0
        '
        'frmCalcRules
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 266)
        Me.Controls.Add(Me.webDisplayCalcRules)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmCalcRules"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Rekenregels"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents webDisplayCalcRules As System.Windows.Forms.WebBrowser
End Class
