﻿''' <summary>
''' Deze dll bevat alle functies nodig om info te vergaren uit de applicatie die je gebruikt die je niet kan verkrijgen via My.Application en een standaard debug protocol.
''' (Versie: 0.9.7.1)
''' </summary>
''' <remarks></remarks>
Public Class clsAppInfo
    Public Shared DefaultFolderName As String = ""
    Public Shared DefaultExtension As String = ".*"
    Public Shared DefaultFilterName As String = "All files"
    Public Shared DefaultFilter As String = DefaultFilterName & "|" & "*" & DefaultExtension
    'Shown
    Public Shared AppDocuments As String = My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\"
    Public Shared AppMusic As String = My.Computer.FileSystem.SpecialDirectories.MyMusic & "\"
    Public Shared AppPictures As String = My.Computer.FileSystem.SpecialDirectories.MyPictures & "\"
    Public Shared AppDesktop As String = My.Computer.FileSystem.SpecialDirectories.Desktop & "\"
    'Discrete
    Public Shared AppTemp As String = My.Computer.FileSystem.SpecialDirectories.Temp & "\"
    Public Shared AppUserAppData As String = My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData & "\"
    Public Shared AppAllUserAppData As String = My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData & "\"

    ''' <summary>
    ''' Deze method geeft alle veriabelen die nodig zijn om bestanden te beheren de gewenste waarde.
    ''' </summary>
    ''' <param name="FolderName">De standaard foldernaam die je wilt gebruiken.</param>
    ''' <param name="Extension">De standaard extensie die je wilt gebruiken.</param>
    ''' <param name="FilterName">De standaard filternaam die je wilt gebruiken. (Eerste waarde uit filter voor "|".)</param>
    ''' <remarks></remarks>
    Public Shared Sub SetDocumentsInfo(Optional FolderName As String = "", Optional Extension As String = ".*", Optional FilterName As String = "All files")
        DefaultExtension = Extension
        DefaultFolderName = FolderName
        DefaultFilterName = FilterName
        DefaultFilter = DefaultFilterName & "|" & "*" & DefaultExtension
        If FolderName <> "" Then
            AppDocuments = My.Computer.FileSystem.SpecialDirectories.MyDocuments.ToString & "\" & FolderName & "\"
            AppMusic = My.Computer.FileSystem.SpecialDirectories.MyMusic.ToString & "\" & FolderName & "\"
            AppPictures = My.Computer.FileSystem.SpecialDirectories.MyPictures.ToString & "\" & FolderName & "\"
            AppDesktop = My.Computer.FileSystem.SpecialDirectories.Desktop.ToString & "\" & FolderName & "\"
            AppTemp = My.Computer.FileSystem.SpecialDirectories.Temp & "\" & FolderName & "\"
            AppUserAppData = My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData & "\" & FolderName & "\"
            AppAllUserAppData = My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData & "\" & FolderName & "\"
        End If
    End Sub

    ''' <summary>
    ''' Deze method geeft een gepaste weergave van de fout naar de gebruiker toe en in de logs.
    ''' </summary>
    ''' <param name="Exception">Exception als resultaat uit een interne fout.</param>
    ''' <param name="Message">Het bericht dat je wilt voortbrengen aan de gebruiker. (Gevolgt door MessageInfo.)</param>
    ''' <param name="MessageInfo">Een extra mededeling met de info die de gebruiker nodig heeft om met deze fout om te gaan.</param>
    ''' <param name="MessageBoxStyle">De stijl dat je wilt geven aan de messagebox</param>
    ''' <returns>Returns alle info die de developer nodig heeft.</returns>
    ''' <remarks></remarks>
    Public Shared Function ManageException(Exception As Exception, Optional Message As String = "", Optional MessageInfo As String = "Stuur de logs door naar Mathias als je dit opgelost wilt.", _
                               Optional MessageBoxStyle As MsgBoxStyle = MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
        If Message = "" Then
            MsgBox(Exception.Message, MessageBoxStyle, My.Application.Info.ProductName & ": Interne fout")
        Else
            If DefaultFolderName <> "" Then
                MsgBox(Message & vbCrLf & MessageInfo & vbCrLf & vbCrLf & "Locatie: Documenten\" & DefaultFolderName, MessageBoxStyle, My.Application.Info.ProductName & ": Interne fout")
            Else
                MsgBox(Message & vbCrLf & MessageInfo & vbCrLf & vbCrLf & "Locatie: Documenten", MessageBoxStyle, My.Application.Info.ProductName & ": Interne fout")
            End If
        End If
        Return My.Computer.Clock.LocalTime & vbCrLf & "Message: " & Exception.Message & vbCrLf & "Stack trace: " & Exception.StackTrace & vbCrLf & "Version: " & _
            My.Application.Info.Version.ToString & vbCrLf & vbCrLf & "_" & vbCrLf
    End Function

    Public Shared Function CreatedException(Message As String, Optional MessageInfo As String = "Stuur de logs door naar Mathias als je dit opgelost wilt.", _
                                            Optional MessageBoxStyle As MsgBoxStyle = MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
        
        If DefaultFolderName <> "" Then
            MsgBox(Message & vbCrLf & MessageInfo & vbCrLf & vbCrLf & "Locatie: Documenten\" & DefaultFolderName, MessageBoxStyle, My.Application.Info.ProductName & ": Interne fout")
        Else
            MsgBox(Message & vbCrLf & MessageInfo & vbCrLf & vbCrLf & "Locatie: Documenten", MessageBoxStyle, My.Application.Info.ProductName & ": Interne fout")
        End If
        Return My.Computer.Clock.LocalTime & vbCrLf & "Message: " & Message & vbCrLf & "Version: " & My.Application.Info.Version.ToString & vbCrLf & vbCrLf & "_" & vbCrLf
    End Function
End Class