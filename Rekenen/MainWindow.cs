﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Rekenen.Properties;
using Color = System.Drawing.Color;

namespace Rekenen
{
  public partial class MainWindow : Form
  {
    private readonly Button[] _presets = new Button[4];
    private byte _activePreset;
    private Session.Session _session;
    private BackgroundWorker _worker;

    public MainWindow()
    {
      InitializeComponent();
      PersonalRecords.LoadRecords();
      Presets();
      ConstructBody();
      AttachEvents();
    }

    private void ConstructBody()
    {
      Logo.BackgroundImage = Resources.DefaultIcon.ToBitmap();
      var space = new Size(Body.Width, Body.Height - StatusBar.Height);
      Start.Left = (int)(space.Width * .025);
      Start.Top = Start.Left;
      Start.Size = new Size((int)(space.Width * .4), 100);
      for (byte p = 0; p < _presets.Length; p++)
      {
        _presets[p].Location = new Point((p == 0 ? 0 : _presets[p - 1].Width + _presets[p - 1].Left) + Start.Left,
          Start.Top * 2 + Start.Height);
        _presets[p].Size = new Size((space.Width - Start.Left) / 4 - Start.Left, (int)(space.Height * .25));
      }
      Options.Location = new Point(Start.Left, _presets[0].Top + _presets[0].Height + Start.Top);
      Options.Size = new Size(space.Width - Start.Left * 2, (space.Height - Options.Top) / 2 - Start.Top);
      PersonalStats.Location = new Point(Start.Left, Options.Top + Options.Height + Start.Top);
      PersonalStats.Size = Options.Size;
    }

    private void SessionProcess(bool stop)
    {
      Start.Enabled = stop;
      if (!stop)
      {
        Status.Text = Resources.MainWindow_Initializing;
        _worker.RunWorkerAsync();
      }
      else
      {
        SessionProgress.Value = 0;
        _presets[_activePreset].Enabled = !stop;
      }
      Status.Visible = !stop;
      SessionProgress.Visible = !stop;
      CancelSession.Visible = !stop;
    }

    #region Events

    private void Options_Click(object sender, EventArgs e)
    {
      Start.Select();
      _presets[_activePreset].Enabled = true;
      _activePreset = 3;
      _presets[_activePreset].Enabled = false;
      new OptionWindow(_session).ShowDialog(this);
    }

    private void AttachEvents()
    {
      GuiTools.TitleBar.Initialize(this, TitleBar);
      ClientSizeChanged += (sender, e) => ConstructBody();
      Shown += (sender, e) => _presets[_activePreset].PerformClick();
      Closed += (sender, e) => PersonalRecords.SaveRecords();
      Start.Click += (sender, e) => SessionProcess(false);
      foreach (var preset in _presets) preset.Click += (sender, e) => preset_Click(preset);
      PersonalStats.Click += (sender, e) => new RecordsWindow().ShowDialog(this);
      _worker = new BackgroundWorker { WorkerReportsProgress = true, WorkerSupportsCancellation = true };
      _worker.DoWork += worker_DoWork;
      _worker.ProgressChanged += worker_ProgressChanged;
      _worker.RunWorkerCompleted += worker_RunWorkerCompleted;
      CancelSession.Click += (sender, e) => _worker.CancelAsync();
    }

    #endregion

    #region Presets

    private void Presets()
    {
      var color = new[]
      {
        Color.FromArgb(0x99, 0x33, 0x33),
        Color.FromArgb(0x33, 0x99, 0x33),
        Color.FromArgb(0x33, 0x33, 0x99),
        Color.FromArgb(0x99, 0x33, 0x99)
      };
      var text = Resources.MainWindow_Presets.Split(';');
      for (byte p = 0; p < _presets.Length; p++)
      {
        _presets[p] = new Button
        {
          Parent = Body,
          Anchor = AnchorStyles.Top,
          BackColor = Color.FromArgb(0x33, 0x33, 0x33),
          FlatAppearance =
          {
            BorderColor = Color.FromArgb(color[p].ToArgb() - 0xff),
            MouseOverBackColor = color[p]
          },
          FlatStyle = FlatStyle.Flat,
          Font = new Font("Microsoft Sans Serif", 24F, FontStyle.Regular, GraphicsUnit.Point, 0),
          ForeColor = Color.FromArgb(0xcc, 0xcc, 0xcc),
          Text = text[p],
          Name = text[p],
          TabIndex = Start.TabIndex + p
        };
      }
    }

    private void preset_Click(Control preset)
    {
      for (byte p = 0; p < _presets.Length; p++)
      {
        if (!_presets[p].Equals(preset)) _presets[p].Enabled = true;
        else
        {
          preset.Enabled = false;
          _activePreset = p;
          _session = Helper.GetSession((Helper.Preset)p);
        }
      }
    }

    #endregion

    #region Worker

    private void worker_DoWork(object sender, DoWorkEventArgs e)
    {
      _session.Run();
      var progression = .0;
      while (!_session.Initialized)
      {
        Thread.Sleep(50);
        if (!_worker.CancellationPending) continue;
        e.Cancel = true;
        break;
      }
      _worker.ReportProgress(0);
      while (!_session.Completed)
      {
        if (progression < _session.Progress)
        {
          progression = _session.Progress;
          _worker.ReportProgress((int)(_session.Progress * 100));
        }
        if (!_worker.CancellationPending) continue;
        e.Cancel = true;
        break;
      }
    }

    private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
      if (e.ProgressPercentage == 0) Status.Text = Resources.MainWindow_Busy;
      SessionProgress.Value = e.ProgressPercentage;
    }

    private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      if (e.Cancelled)
      {
        Status.Text = Resources.MainWindow_Canceling;
        _session.Cancel();
        SessionProgress.Value = 100;
        var cancelingWorker = new BackgroundWorker();
        cancelingWorker.RunWorkerCompleted += (o, args) => SessionProcess(true);
        cancelingWorker.DoWork += (o, args) =>
        {
          while (_session.IsBusy()) Thread.Sleep(50);
        };
        cancelingWorker.RunWorkerAsync();
        return;
      }
      CancelSession.Visible = false;
      SessionProgress.Value = 100;
      Status.Text = Resources.MainWindow_Ready;
      var sessionWindow = new SessionWindow(_session.Output().GetEnumerator(), _session.Exercises);
      sessionWindow.Closed += SessionWindow_Closed;
      if (!sessionWindow.IsDisposed) sessionWindow.ShowDialog(this);
      else SessionProcess(true);
    }

    private void SessionWindow_Closed(object sender, EventArgs e)
    {
      if (!PersonalRecords.Records.ContainsKey(_session.Name))
        PersonalRecords.Records.Add(_session.Name, new PersonalRecords.PersonalRecord(_session.Name));
      SessionProcess(true);
      var instance = (SessionWindow)sender;
      foreach (var record in PersonalRecords.Records)
      {
        if (record.Key != _session.Name) continue;
        var score = PersonalRecords.PersonalRecord.CalculateScore(instance.Accuracy, instance.TimeSpent);
        if (record.Value.Score > score) return;
        record.Value.Accuracy = instance.Accuracy;
        record.Value.TimeSpent = instance.TimeSpent;
      }
    }

    #endregion
  }
}