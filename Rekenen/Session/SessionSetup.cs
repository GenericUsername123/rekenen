﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

//ReSharper disable MemberCanBeProtected.Global

namespace Session
{
  public abstract class SessionSetup
  {
    private long _cycleTime;
    private byte _exercises;
    private sbyte _precision;
    private int _seed;
    public bool Debug;
    public string Name;
    protected Random Rnd;
    protected Symbols.Type[][] SymbolTable;
    public byte UniqueResults;

    protected SessionSetup(byte exercises)
    {
      NumberRange = new NumberRange(this);
      DefinedSymbolTable = new DefinedSymbolTable(this);
      Inclusions = new NumericalOccurrences(this);
      Exclusions = new NumericalOccurrences(this);
      Exercises = exercises;
      Precision = 0;
      UniqueResults = 0;
      CycleTime = 200;
    }

    public NumericalOccurrences Exclusions { get; protected set; }
    public NumericalOccurrences Inclusions { get; protected set; }
    public DefinedSymbolTable DefinedSymbolTable { get; protected set; }
    public NumberRange NumberRange { get; protected set; }
    public FactorRange FactorRange { get; protected set; } = new FactorRange();
    public Symbols Symbols { get; protected set; } = new Symbols();
    public double[] Results { get; protected set; }
    public double[][] Numbers { get; protected set; }

    public int Seed
    {
      get { return _seed; }
      set
      {
        Rnd = new Random(value);
        Inclusions.Occurrences.Seed = Rnd.Next();
        Exclusions.Occurrences.Seed = Rnd.Next();
        _seed = value;
      }
    }

    public sbyte Precision
    {
      [MethodImpl(MethodImplOptions.NoInlining)]
      get { return _precision; }
      set
      {
        _precision = Math.Abs(value) > 5 ? (value < 0 ? (sbyte)-5 : (sbyte)5) : value;
        NumberRange.Update();
      }
    }

    public byte Exercises
    {
      get { return _exercises; }
      set
      {
        _exercises = value == 0 ? (byte)1 : value;
        DefinedSymbolTable.Update();
        Inclusions.Occurrences.Update();
        Exclusions.Occurrences.Update();
      }
    }

    public long CycleTime
    {
      [MethodImpl(MethodImplOptions.NoInlining)]
      get { return _cycleTime; }
      set { _cycleTime = value == 0 ? long.MaxValue : Math.Abs(value); }
    }

    protected virtual void Update()
    {
      Numbers = new double[Exercises][];
      Results = new double[Exercises];
      if (Symbols.Count == 0) Symbols.Add(Symbols.Type.Sum);
      SymbolTable = new Symbols.Type[Exercises][];
    }

    protected int Factors(byte index)
    {
      var numSize = Rnd.Next(FactorRange.UpperBound - FactorRange.LowerBound + 1) + FactorRange.LowerBound;
      SymbolTable[index] = new Symbols.Type[numSize - 1];
      SymbolRecord(index);
      return numSize;
    }

    protected void SymbolRecord(byte index)
    {
      var symbolString = Symbols.ToString();
      var divisionExcluded = Inclusions.OnlyZero ? symbolString.Replace("4", "") : "";
      for (var s = 0; s < SymbolTable[index].Length; s++)
        if (Inclusions.Occurrences[index][s + 1] && !divisionExcluded.Equals(""))
        {
          var symbol = DefinedSymbolTable[index][s];
          if (symbol != 0 && symbol != Symbols.Type.Division) SymbolTable[index][s] = DefinedSymbolTable[index][s];
          else Enum.TryParse(divisionExcluded[Rnd.Next(divisionExcluded.Length)] + "", out SymbolTable[index][s]);
        }
        else if (DefinedSymbolTable[index][s] != 0) SymbolTable[index][s] = DefinedSymbolTable[index][s];
        else Enum.TryParse(symbolString[Rnd.Next(symbolString.Length)] + "", out SymbolTable[index][s]);
    }

    protected static byte[] GroupSizes(IList<Symbols.Type> symbols)
    {
      var groups = new int[symbols.Count];
      var g = 0;
      while (g < groups.Length) groups[g++]--;
      var lastSymbol = Symbols.Type.Unknown;
      int i = 0, s = 0, size = 0;
      var symbolIntegers = new int[2];
      while (s <= symbols.Count)
      {
        symbolIntegers[1] = (int)lastSymbol;
        if (s < symbols.Count)
        {
          symbolIntegers[0] = (int)symbols[s];
          if (symbolIntegers[0] % 2 == symbolIntegers[1] % 2 && symbolIntegers[1] != 0)
          {
            s++;
            continue;
          }
        }
        if (groups[i] != -1)
        {
          if (i != 0) size += groups[i - 1];
          groups[i] = s - size + (symbolIntegers[1] % 2 == 0 ? 1 : 0);
          if (s == symbols.Count)
          {
            if (i == 0) groups[i] += symbolIntegers[1] % 2 == 0 ? 0 : 1;
            else groups[i] += symbolIntegers[1] % 2 == 1 ? 1 : 0;
            s++;
          }
          i++;
        }
        else
        {
          groups[i] = 0;
          lastSymbol = symbols[s++];
        }
      }
      g = 0;
      g += groups.TakeWhile(group => group != -1).Count(group => group != 0);
      var result = new byte[g];
      g = 0;
      foreach (byte group in groups.TakeWhile(group => group != -1).Where(group => group != 0)) result[g++] = group;
      return result;
    }
  }

  public class DefinedSymbolTable : DefinedTable<Symbols.Type>
  {
    public DefinedSymbolTable(SessionSetup sessionSetup) : base(sessionSetup, FactorRange.Limit - 1)
    {
    }

    protected override bool IsIgnored(Symbols.Type item)
    {
      return (int)item > 4 || item == 0;
    }
  }
}