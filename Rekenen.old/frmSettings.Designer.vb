﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdApply = New System.Windows.Forms.Button()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.cmdOK = New System.Windows.Forms.Button()
        Me.chkLogs = New System.Windows.Forms.CheckBox()
        Me.grpDelete = New System.Windows.Forms.GroupBox()
        Me.chkEvaluations = New System.Windows.Forms.CheckBox()
        Me.chkPreferences = New System.Windows.Forms.CheckBox()
        Me.cmdDeleteAll = New System.Windows.Forms.Button()
        Me.chkProfile = New System.Windows.Forms.CheckBox()
        Me.grpDelete.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdApply
        '
        Me.cmdApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdApply.Location = New System.Drawing.Point(162, 137)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 1
        Me.cmdApply.Text = "Toepassen"
        Me.cmdApply.UseVisualStyleBackColor = True
        '
        'cmdCancel
        '
        Me.cmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel.Location = New System.Drawing.Point(81, 137)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 0
        Me.cmdCancel.Text = "Annuleren"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'cmdOK
        '
        Me.cmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdOK.Location = New System.Drawing.Point(10, 137)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(65, 23)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "OK"
        Me.cmdOK.UseVisualStyleBackColor = True
        '
        'chkLogs
        '
        Me.chkLogs.AutoSize = True
        Me.chkLogs.Location = New System.Drawing.Point(6, 19)
        Me.chkLogs.Name = "chkLogs"
        Me.chkLogs.Size = New System.Drawing.Size(179, 17)
        Me.chkLogs.TabIndex = 0
        Me.chkLogs.Text = "Logs van interne fouten en bugs"
        Me.chkLogs.UseVisualStyleBackColor = True
        '
        'grpDelete
        '
        Me.grpDelete.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpDelete.Controls.Add(Me.chkEvaluations)
        Me.grpDelete.Controls.Add(Me.chkPreferences)
        Me.grpDelete.Controls.Add(Me.cmdDeleteAll)
        Me.grpDelete.Controls.Add(Me.chkProfile)
        Me.grpDelete.Controls.Add(Me.chkLogs)
        Me.grpDelete.Location = New System.Drawing.Point(12, 12)
        Me.grpDelete.Name = "grpDelete"
        Me.grpDelete.Size = New System.Drawing.Size(225, 119)
        Me.grpDelete.TabIndex = 3
        Me.grpDelete.TabStop = False
        Me.grpDelete.Text = "Wissen"
        '
        'chkEvaluations
        '
        Me.chkEvaluations.AutoSize = True
        Me.chkEvaluations.Location = New System.Drawing.Point(6, 88)
        Me.chkEvaluations.Name = "chkEvaluations"
        Me.chkEvaluations.Size = New System.Drawing.Size(75, 17)
        Me.chkEvaluations.TabIndex = 102
        Me.chkEvaluations.Text = "Evaluaties"
        Me.chkEvaluations.UseVisualStyleBackColor = True
        '
        'chkPreferences
        '
        Me.chkPreferences.AutoSize = True
        Me.chkPreferences.Location = New System.Drawing.Point(6, 65)
        Me.chkPreferences.Name = "chkPreferences"
        Me.chkPreferences.Size = New System.Drawing.Size(81, 17)
        Me.chkPreferences.TabIndex = 101
        Me.chkPreferences.Text = "Voorkeuren"
        Me.chkPreferences.UseVisualStyleBackColor = True
        '
        'cmdDeleteAll
        '
        Me.cmdDeleteAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdDeleteAll.AutoSize = True
        Me.cmdDeleteAll.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.cmdDeleteAll.Location = New System.Drawing.Point(132, 90)
        Me.cmdDeleteAll.Name = "cmdDeleteAll"
        Me.cmdDeleteAll.Size = New System.Drawing.Size(86, 23)
        Me.cmdDeleteAll.TabIndex = 100
        Me.cmdDeleteAll.Text = "Selecteer alles"
        Me.cmdDeleteAll.UseVisualStyleBackColor = True
        '
        'chkProfile
        '
        Me.chkProfile.AutoSize = True
        Me.chkProfile.Location = New System.Drawing.Point(6, 42)
        Me.chkProfile.Name = "chkProfile"
        Me.chkProfile.Size = New System.Drawing.Size(100, 17)
        Me.chkProfile.TabIndex = 1
        Me.chkProfile.Text = "Gebruikerprofiel"
        Me.chkProfile.UseVisualStyleBackColor = True
        '
        'frmSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(249, 172)
        Me.Controls.Add(Me.grpDelete)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdApply)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(265, 310)
        Me.MinimumSize = New System.Drawing.Size(265, 210)
        Me.Name = "frmSettings"
        Me.Text = "Geheugen"
        Me.grpDelete.ResumeLayout(False)
        Me.grpDelete.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdApply As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents chkLogs As System.Windows.Forms.CheckBox
    Friend WithEvents grpDelete As System.Windows.Forms.GroupBox
    Friend WithEvents cmdDeleteAll As System.Windows.Forms.Button
    Friend WithEvents chkProfile As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreferences As System.Windows.Forms.CheckBox
    Friend WithEvents chkEvaluations As System.Windows.Forms.CheckBox
End Class
