﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace Session
{
  partial class Session
  {
    public IEnumerable<string> Config => new[]
    {
      "[Version " + FileVersionInfo.GetVersionInfo(Assembly.GetAssembly(GetType()).Location).FileVersion + "]",
      "[Basics]",
      "Exercises=" + Exercises,
      "Precision=" + Precision,
      "[Factor range]",
      "LowerBound=" + FactorRange.LowerBound,
      "UpperBound=" + FactorRange.UpperBound,
      "[Number range]",
      "LowerNumber=" + NumberRange.LowerNumber,
      "UpperNumber=" + NumberRange.UpperNumber,
      "LowerResult=" + NumberRange.LowerResult,
      "UpperResult=" + NumberRange.UpperResult,
      "LowerLimit=" + NumberRange.LowerLimit,
      "UpperLimit=" + NumberRange.UpperLimit,
      "[Symbols]",
      "Sum=" + Symbols.Contains(Symbols.Type.Sum),
      "Product=" + Symbols.Contains(Symbols.Type.Product),
      "Difference=" + Symbols.Contains(Symbols.Type.Difference),
      "Division=" + Symbols.Contains(Symbols.Type.Division),
      "[Inclusions]",
      "Absolute=" + Inclusions.Absolute,
      "PcntExercises=" + Inclusions.Occurrences.PcntExercises,
      "PcntFactors=" + Inclusions.Occurrences.PcntFactors,
      "List=" + Inclusions,
      "[Exclusions]",
      "Absolute=" + Exclusions.Absolute,
      "PcntExercises=" + Exclusions.Occurrences.PcntExercises,
      "PcntFactors=" + Exclusions.Occurrences.PcntFactors,
      "List=" + Exclusions,
      "[Advanced]",
      "UniqueResults=" + UniqueResults,
      "Seed=" + Seed,
      "CycleTime=" + CycleTime,
      "ThreadCount=" + ThreadCount,
      "Debug=" + Debug
    };

    public override string ToString()
    {
      return Config.Aggregate("", (s, s1) => s + s1 + '\n');
    }

    public void LoadConfig(string[] config)
    {
      if (config == null) return;
      var configString = config.Aggregate("", (content, s) => content + s + "\n");
      bool @bool;
      byte @byte;
      sbyte @sbyte;
      int @int;
      long @long;
      double @double;
      if (byte.TryParse(GetNextFromConfig("Exercises", ref configString), out @byte)) Exercises = @byte;
      if (sbyte.TryParse(GetNextFromConfig("Precision", ref configString), out @sbyte)) Precision = @sbyte;
      if (byte.TryParse(GetNextFromConfig("LowerBound", ref configString), out @byte)) FactorRange.LowerBound = @byte;
      if (byte.TryParse(GetNextFromConfig("UpperBound", ref configString), out @byte)) FactorRange.UpperBound = @byte;
      if (double.TryParse(GetNextFromConfig("LowerNumber", ref configString), out @double))
        NumberRange.LowerNumber = @double;
      if (double.TryParse(GetNextFromConfig("UpperNumber", ref configString), out @double))
        NumberRange.UpperNumber = @double;
      if (double.TryParse(GetNextFromConfig("LowerResult", ref configString), out @double))
        NumberRange.LowerResult = @double;
      if (double.TryParse(GetNextFromConfig("UpperResult", ref configString), out @double))
        NumberRange.UpperResult = @double;
      if (double.TryParse(GetNextFromConfig("LowerLimit", ref configString), out @double))
        NumberRange.LowerLimit = @double;
      if (double.TryParse(GetNextFromConfig("UpperLimit", ref configString), out @double))
        NumberRange.UpperLimit = @double;
      Symbols.Clear();
      if (bool.TryParse(GetNextFromConfig("Sum", ref configString), out @bool) && @bool) Symbols.Add(Symbols.Type.Sum);
      if (bool.TryParse(GetNextFromConfig("Product", ref configString), out @bool) && @bool)
        Symbols.Add(Symbols.Type.Product);
      if (bool.TryParse(GetNextFromConfig("Difference", ref configString), out @bool) && @bool)
        Symbols.Add(Symbols.Type.Difference);
      if (bool.TryParse(GetNextFromConfig("Division", ref configString), out @bool) && @bool)
        Symbols.Add(Symbols.Type.Division);
      if (bool.TryParse(GetNextFromConfig("Absolute", ref configString), out @bool)) Inclusions.Absolute = @bool;
      if (double.TryParse(GetNextFromConfig("PcntExercises", ref configString), out @double))
        Inclusions.Occurrences.PcntExercises = @double;
      if (double.TryParse(GetNextFromConfig("PcntFactors", ref configString), out @double))
        Inclusions.Occurrences.PcntFactors = @double;
      Inclusions.Clear();
      var temp = GetNextFromConfig("List", ref configString);
      foreach (var inclusion in temp.Substring(1, temp.IndexOf("}", StringComparison.InvariantCulture) - 1).Split(','))
        if (double.TryParse(inclusion, out @double)) Inclusions.Add(@double);
      if (bool.TryParse(GetNextFromConfig("Absolute", ref configString), out @bool)) Inclusions.Absolute = @bool;
      if (double.TryParse(GetNextFromConfig("PcntExercises", ref configString), out @double))
        Inclusions.Occurrences.PcntExercises = @double;
      if (double.TryParse(GetNextFromConfig("PcntFactors", ref configString), out @double))
        Inclusions.Occurrences.PcntFactors = @double;
      Exclusions.Clear();
      temp = GetNextFromConfig("List", ref configString);
      foreach (var exclusion in temp.Substring(1, temp.IndexOf("}", StringComparison.InvariantCulture) - 1).Split(','))
        if (double.TryParse(exclusion, out @double)) Exclusions.Add(@double);
      if (byte.TryParse(GetNextFromConfig("UniqueResults", ref configString), out @byte)) UniqueResults = @byte;
      if (int.TryParse(GetNextFromConfig("Seed", ref configString), out @int)) Seed = @int;
      if (long.TryParse(GetNextFromConfig("CycleTime", ref configString), out @long)) CycleTime = @long;
      if (byte.TryParse(GetNextFromConfig("ThreadCount", ref configString), out @byte)) ThreadCount = @byte;
      if (bool.TryParse(GetNextFromConfig("Debug", ref configString), out @bool)) Debug = @bool;
    }

    private string GetNextFromConfig(string name, ref string configString)
    {
      name += "=";
      configString = configString.Substring(configString.IndexOf(name, StringComparison.InvariantCulture) + name.Length);
      return configString.Substring(0, configString.IndexOf('\n'));
    }
  }
}