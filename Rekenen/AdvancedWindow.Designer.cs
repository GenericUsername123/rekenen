﻿using Rekenen.Properties;

namespace Rekenen
{
  partial class AdvancedWindow
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.Profiles = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.SingleThread = new System.Windows.Forms.CheckBox();
      this.Debug = new System.Windows.Forms.CheckBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.Seed = new System.Windows.Forms.TextBox();
      this.CycleTime = new System.Windows.Forms.TextBox();
      this.UniqueResults = new System.Windows.Forms.TextBox();
      this.Save = new System.Windows.Forms.Button();
      this.LoadConfig = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // Profiles
      // 
      this.Profiles.FormattingEnabled = true;
      this.Profiles.Location = new System.Drawing.Point(12, 12);
      this.Profiles.Name = "Profiles";
      this.Profiles.Size = new System.Drawing.Size(126, 21);
      this.Profiles.TabIndex = 0;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 71);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(0, 13);
      this.label1.TabIndex = 1;
      // 
      // SingleThread
      // 
      this.SingleThread.AutoSize = true;
      this.SingleThread.Location = new System.Drawing.Point(12, 146);
      this.SingleThread.Name = "SingleThread";
      this.SingleThread.Size = new System.Drawing.Size(15, 14);
      this.SingleThread.TabIndex = 6;
      this.SingleThread.UseVisualStyleBackColor = true;
      // 
      // Debug
      // 
      this.Debug.AutoSize = true;
      this.Debug.Location = new System.Drawing.Point(12, 169);
      this.Debug.Name = "Debug";
      this.Debug.Size = new System.Drawing.Size(15, 14);
      this.Debug.TabIndex = 7;
      this.Debug.UseVisualStyleBackColor = true;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 97);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(0, 13);
      this.label2.TabIndex = 4;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 123);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(0, 13);
      this.label3.TabIndex = 5;
      // 
      // Seed
      // 
      this.Seed.Location = new System.Drawing.Point(72, 94);
      this.Seed.MaxLength = 10;
      this.Seed.Name = "Seed";
      this.Seed.Size = new System.Drawing.Size(66, 20);
      this.Seed.TabIndex = 4;
      // 
      // CycleTime
      // 
      this.CycleTime.Location = new System.Drawing.Point(108, 120);
      this.CycleTime.MaxLength = 4;
      this.CycleTime.Name = "CycleTime";
      this.CycleTime.Size = new System.Drawing.Size(30, 20);
      this.CycleTime.TabIndex = 5;
      // 
      // UniqueResults
      // 
      this.UniqueResults.Location = new System.Drawing.Point(114, 68);
      this.UniqueResults.MaxLength = 3;
      this.UniqueResults.Name = "UniqueResults";
      this.UniqueResults.Size = new System.Drawing.Size(24, 20);
      this.UniqueResults.TabIndex = 3;
      // 
      // Save
      // 
      this.Save.Location = new System.Drawing.Point(12, 39);
      this.Save.Name = "Save";
      this.Save.Size = new System.Drawing.Size(60, 23);
      this.Save.TabIndex = 2;
      this.Save.Text = "Opslaan";
      this.Save.UseVisualStyleBackColor = true;
      this.Save.Click += new System.EventHandler(this.Save_Click);
      // 
      // LoadConfig
      // 
      this.LoadConfig.Location = new System.Drawing.Point(78, 39);
      this.LoadConfig.Name = "LoadConfig";
      this.LoadConfig.Size = new System.Drawing.Size(60, 23);
      this.LoadConfig.TabIndex = 1;
      this.LoadConfig.Text = "Laden";
      this.LoadConfig.UseVisualStyleBackColor = true;
      this.LoadConfig.Click += new System.EventHandler(this.Load_Click);
      // 
      // AdvancedWindow
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(150, 198);
      this.Controls.Add(this.LoadConfig);
      this.Controls.Add(this.Save);
      this.Controls.Add(this.UniqueResults);
      this.Controls.Add(this.CycleTime);
      this.Controls.Add(this.Seed);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.Debug);
      this.Controls.Add(this.SingleThread);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.Profiles);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Icon = global::Rekenen.Properties.Resources.DefaultIcon;
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(156, 224);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(156, 224);
      this.Name = "AdvancedWindow";
      this.Text = "Geavanceerd";
      this.Shown += new System.EventHandler(this.AdvancedWindow_Shown);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ComboBox Profiles;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.CheckBox SingleThread;
    private System.Windows.Forms.CheckBox Debug;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox Seed;
    private System.Windows.Forms.TextBox CycleTime;
    private System.Windows.Forms.TextBox UniqueResults;
    private System.Windows.Forms.Button Save;
    private System.Windows.Forms.Button LoadConfig;
  }
}