﻿using System;
using System.Threading;
using System.Windows.Forms;
using Rekenen.Properties;
using Session;

// ReSharper disable UnusedMember.Global

namespace Rekenen
{
  internal static class Program
  {
    /// <summary>
    ///   The main entry point for the application.
    /// </summary>
    [STAThread]
    private static void Main()
    {
      //Console.Setup();
      //while (true) Console.Loop();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.EnableVisualStyles();
      Application.Run(new MainWindow());
    }

    public static class Console
    {
      public const bool Clear = true;
      public const bool Wait = true;
      public static Session.Session Session;

      public static void Setup()
      {
        SetupConsole();
        Session = GetSession(4, 0, 1);
        //_session.Debug = true;
        Session.CycleTime = 300;
        Session.Exclusions.Add(0);
        Session.Exclusions.Occurrences.Initialize(1, 1);
      }

      public static void Loop()
      {
        while (true)
        {
          Execute(Clear, true);
          PrintOutput(Clear, Wait);
        }
      }

      public static void SetupConsole()
      {
        System.Console.Title = Resources.Test_Title;
        System.Console.BufferHeight = short.MaxValue - 1;
        System.Console.SetWindowSize(System.Console.BufferWidth, (int)(System.Console.LargestWindowHeight / 1.5));
        System.Console.SetWindowPosition(0, 0);
        System.Console.CursorVisible = false;
      }

      public static void PrintOutput(bool clear, bool wait)
      {
        var output = Session.Output();
        if (output == null) return;
        if (clear) System.Console.Clear();
        for (var e = 0; e < output.Length; e++)
          System.Console.WriteLine(output[e][0] + (output[e][1] == null ? "" : " = " + output[e][1]) + '\n');
        var isSent = false;
        while (Session.IsBusy())
          if (!isSent)
          {
            isSent = true;
            System.Console.WriteLine(Resources.MainWindow_Canceling);
          }
        if (!wait) return;
        System.Console.ReadKey(true);
      }

      public static void Execute(bool clear, bool verbose)
      {
        if (clear) System.Console.Clear();
        if (verbose) System.Console.WriteLine(Resources.MainWindow_Initializing);
        Session.Run();
        if (clear) System.Console.Clear();
        var progress = -1F;
        while (!Session.Completed)
        {
          Thread.Sleep(100);
          if (Session.Progress == progress || !verbose) continue;
          progress = Session.Progress;
          if (clear) System.Console.Clear();
          System.Console.WriteLine(progress.ToString("P"));
          System.Console.Write('[');
          var barWidth = System.Console.BufferWidth - 2;
          for (var exercise = 0; exercise < barWidth; exercise++)
          {
            System.Console.Write(exercise < barWidth * progress ? '*' : ' ');
          }
          System.Console.Write(']');
        }
        if (clear) System.Console.Clear();
        System.Console.WriteLine(Resources.MainWindow_Ready);
      }

      public static Session.Session GetSession(byte setupId, int seed, byte threads)
      {
        Session.Session session;
        switch (setupId)
        {
          case 0:
            session = new Session.Session(seed, threads)
            {
              Exercises = 255,
              Precision = 8,
              Symbols = { Symbols.Type.Sum, Symbols.Type.Difference, Symbols.Type.Product, Symbols.Type.Division }
            };
            session.FactorRange.SetAll(99, 99);
            session.NumberRange.SetAll(0, 2e2, -1e6, 1e6);
            break;
          case 1:
            session = new Session.Session(seed, threads)
            {
              Exercises = 255,
              Symbols = { Symbols.Type.Sum, Symbols.Type.Difference, Symbols.Type.Product, Symbols.Type.Division }
            };
            session.FactorRange.SetAll(1, 99);
            break;
          case 2:
            session = new Session.Session(seed, threads)
            {
              Exercises = 255,
              Symbols = { Symbols.Type.Sum, Symbols.Type.Difference }
            };
            session.FactorRange.SetAll(1, 99);
            break;
          case 3:
            session = new Session.Session(seed, threads)
            {
              Exercises = 10,
              Symbols = { Symbols.Type.Sum }
            };
            session.FactorRange.SetAll(1, 3);
            session.NumberRange.SetAll(0, 1e5, 0, 1e3, 0, 1e5);
            break;
          case 4:
            session = new Session.Session(seed, threads)
            {
              Exercises = 255,
              Precision = 8,
              Symbols = { Symbols.Type.Sum, Symbols.Type.Difference, Symbols.Type.Product, Symbols.Type.Division }
            };
            session.FactorRange.SetAll(6, 6);
            session.NumberRange.SetAll(0, 2, -1e6, 1e6);
            break;
          case 5:
            session = new Session.Session(seed, threads)
            {
              Exercises = 255,
              Precision = -2,
              Symbols = { Symbols.Type.Sum, Symbols.Type.Difference, Symbols.Type.Product, Symbols.Type.Division }
            };
            session.FactorRange.SetAll(6, 6);
            session.NumberRange.SetAll(0, 2e2, -1e6, 1e6);
            break;
          case 6:
            session = new Session.Session(seed, threads)
            {
              Exercises = 255,
              Precision = 2,
              Symbols = { Symbols.Type.Sum, Symbols.Type.Difference, Symbols.Type.Product, Symbols.Type.Division }
            };
            session.FactorRange.SetAll(4, 6);
            session.NumberRange.SetAll(-1e6, 1e6, -1e9, 1e9);
            break;
          default:
            session = new Session.Session(seed, threads);
            break;
        }
        return session;
      }
    }
  }
}