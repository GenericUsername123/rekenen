using System;

namespace Session
{
  public class Occurrences : DefinedTable<bool>
  {
    private double _pcntExercises;
    private double _pcntFactors;
    private Random _rnd;
    private int _seed;

    public Occurrences(SessionSetup sessionSetup, byte dim2) : base(sessionSetup, dim2)
    {
      PcntExercises = 1;
      PcntFactors = PcntExercises;
    }

    public int Seed
    {
      // ReSharper disable once UnusedMember.Global
      get { return _seed; }
      set
      {
        _rnd = new Random(value);
        _seed = value;
      }
    }

    public double PcntExercises
    {
      get { return _pcntExercises; }
      set
      {
        value = Math.Abs(value);
        _pcntExercises = value > 1 ? 1 : value;
      }
    }

    public double PcntFactors
    {
      get { return _pcntFactors; }
      set
      {
        value = Math.Abs(value);
        _pcntFactors = value > 1 ? 1 : value;
      }
    }

    public void Initialize(double pcntExercises, double pcntFactors)
    {
      PcntExercises = pcntExercises;
      PcntFactors = pcntFactors;
      Initialize();
    }

    public void Initialize()
    {
      var records = Items((int)(Length * PcntExercises), Length);
      byte r = 0;
      while (r < Length)
      {
        if (!records[r])
        {
          r++;
          continue;
        }
        var range = Items((int)(base[r].Length * PcntFactors), base[r].Length);
        SetRanges(0, range, ++r);
      }
    }

    private bool[] Items(int affectedCount, int length)
    {
      var items = new bool[length];
      while (affectedCount > 0)
      {
        var affected = _rnd.Next(length);
        if (items[affected]) continue;
        items[affected] = true;
        affectedCount--;
      }
      return items;
    }
  }
}