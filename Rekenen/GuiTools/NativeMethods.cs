using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Windows.Input;

// ReSharper disable UnusedMember.Global

namespace GuiTools
{
  public static class NativeMethods
  {
    public enum MapType : uint
    {
      MapvkVkToVsc = 0x0,
      MapvkVscToVk = 0x1,
      MapvkVkToChar = 0x2,
      MapvkVscToVkEx = 0x3
    }

    [DllImport("user32.dll")]
    private static extern int ToUnicode(uint wVirtKey, uint wScanCode, byte[] lpKeyState,
      [Out, MarshalAs(UnmanagedType.LPWStr, SizeParamIndex = 4)] StringBuilder pwszBuff, int cchBuff, uint wFlags);

    [DllImport("user32.dll")]
    private static extern bool GetKeyboardState(byte[] lpKeyState);

    [DllImport("user32.dll")]
    private static extern uint MapVirtualKey(uint uCode, MapType uMapType);

    public static char GetCharFromKey(Key key)
    {
      var ch = ' ';
      var virtualKey = KeyInterop.VirtualKeyFromKey(key);
      var keyboardState = new byte[256];
      GetKeyboardState(keyboardState);
      var scanCode = MapVirtualKey((uint)virtualKey, MapType.MapvkVkToVsc);
      var stringBuilder = new StringBuilder(2);
      var result = ToUnicode((uint)virtualKey, scanCode, keyboardState, stringBuilder, stringBuilder.Capacity, 0);
      switch (result)
      {
        case -1:
          break;
        case 0:
          break;
        case 1:
          {
            ch = stringBuilder[0];
            break;
          }
        default:
          {
            ch = stringBuilder[0];
            break;
          }
      }
      return ch;
    }

    // ReSharper disable once UnusedMethodReturnValue.Global
    public static bool TryParse(Keys keys, out char ch)
    {
      Key key;
      if (!Enum.TryParse(keys + "", out key))
      {
        ch = new char();
        return false;
      }
      ch = GetCharFromKey(key);
      return true;
    }

    public static char Parse(Keys keys)
    {
      return GetCharFromKey((Key)Enum.Parse(typeof(Key), keys + ""));
    }
  }
}