﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Timer = System.Timers.Timer;

namespace Session
{
  public sealed partial class Session : SessionSetup, IDisposable
  {
    public const byte DefaultThreadCount = 2;
    private readonly object _locker = new object();
    private byte[][] _groupTable;
    private int _id = -1;
    private byte _lastExercise;
    private int[] _numSizes;
    private Session[] _sessions;
    private BackgroundWorker[] _workers = new BackgroundWorker[0];

    public Session(int seed = int.MinValue, byte threadCount = DefaultThreadCount) : base(1)
    {
      if (seed == int.MinValue && Rnd == null) Seed = new Random().Next();
      if (Rnd != null) Seed = seed == int.MinValue ? Rnd.Next() : seed;
      ThreadCount = threadCount;
    }

    private Session(SessionSetup instance) : base(instance.Exercises)
    {
      Seed = instance.Seed;
      NumberRange = instance.NumberRange;
      FactorRange = instance.FactorRange;
      Symbols = instance.Symbols;
      UniqueResults = instance.UniqueResults;
      CycleTime = instance.CycleTime;
      Precision = instance.Precision;
      Exercises = instance.Exercises;
      DefinedSymbolTable = instance.DefinedSymbolTable;
      Debug = instance.Debug;
      Inclusions = instance.Inclusions;
      Exclusions = instance.Exclusions;
    }

    public byte ThreadCount
    {
      get { return (byte)_workers.Length; }
      set
      {
        if (_id != -1) return;
        value = value == 0 ? (byte)1 : value;
        _workers = new BackgroundWorker[value];
        _sessions = new Session[value];
        for (var t = 0; t < value; t++)
        {
          _workers[t] = new BackgroundWorker { WorkerSupportsCancellation = true };
          _workers[t].DoWork += (sender, e) =>
          {
            var id = (int)e.Argument;
            _sessions[id] = new Session(this) { Seed = Rnd.Next(), _id = id };
            _sessions[id].Update();
            while (!Initialized) Thread.Sleep(50);
            for (var ex = 0; ex < _sessions[id].Exercises; ex++)
            {
              _sessions[id].Exercise(_workers[id]);
              if (_workers[id].CancellationPending) e.Cancel = true;
              var progress = (float)ex / Exercises;
              if (progress > Progress) Progress = progress;
            }
            e.Result = e.Argument;
          };
          _workers[t].RunWorkerCompleted += (sender, e) =>
          {
            if (Completed /*|| e.Cancelled*/) return;
            lock (_locker)
            {
              if (!e.Cancelled)
              {
                var id = (int)e.Result;
                Cancel();
                Numbers = _sessions[id].Numbers;
                Results = _sessions[id].Results;
                SymbolTable = _sessions[id].SymbolTable;
                Progress = 1;
              }
              Completed = true;
            }
          };
        }
      }
    }

    public bool Initialized { get; private set; }
    public float Progress { get; private set; }
    public bool Completed { get; private set; }

    public void Dispose()
    {
      for (var t = 0; t < ThreadCount; t++)
      {
        _workers[t].Dispose();
        _sessions[t]?.Dispose();
      }
    }

    protected override void Update()
    {
      base.Update();
      Initialized = false;
      Progress = 0;
      Completed = false;
      _lastExercise = 0;
      _groupTable = new byte[Exercises][];
      _numSizes = new int[Exercises];
      for (byte e = 0; e < Exercises; e++)
      {
        _numSizes[e] = Factors(e);
        _groupTable[e] = GroupSizes(SymbolTable[e]);
      }
      if (_id != -1) Initialized = true;
    }

    private void Exercise(BackgroundWorker worker)
    {
      var size = _numSizes[_lastExercise];
      var p = Precision < 0 ? 0 : Precision;
      var nums = new double[size];
      if (Debug) for (var n = 0; n < nums.Length; n++) nums[n] = double.NaN;
      var sw = new Stopwatch();
      while (true)
      {
        sw.Restart();
        var groups = _groupTable[_lastExercise];
        var symbols = SymbolTable[_lastExercise];
        double result;
        do
        {
          result = 0;
          byte n = 0;
          for (byte gs = 0; gs < groups.Length && sw.ElapsedMilliseconds <= CycleTime; gs++)
          {
            if (worker.CancellationPending) return;
            var grpStart = n;
            var grp = Number(out nums[n], n, Precision);
            if (n != 0) if (symbols[n - 1] == Symbols.Type.Difference) grp *= -1;
            while (++n < groups[gs] + grpStart)
            {
              if (symbols[n - 1] == Symbols.Type.Division)
              {
                if (Math.Abs(grp % Number(out nums[n], n, Precision, true)) > 0) break;
                grp /= nums[n];
              }
              else if (symbols[n - 1] == Symbols.Type.Product)
                grp *= Number(out nums[n], n, Precision - NumericalOccurrences.HasPrecision(grp));
              else if (symbols[n - 1] == Symbols.Type.Sum) grp += Number(out nums[n], n, Precision);
              else grp -= Number(out nums[n], n, Precision);
              if (!NumberRange.WithinLimits(grp)) break;
            }
            if (n != groups[gs] + grpStart)
            {
              gs--;
              n = grpStart;
              continue;
            }
            result += grp;
          }
          result = Math.Round(result, p);
          if (UniqueResults <= 0) continue;
          var r = _lastExercise - UniqueResults;
          for (r = r < 0 ? 0 : r; r < _lastExercise; r++)
          {
            if (Math.Abs(Math.Round(result - Results[r], p)) > 0) continue;
            result = NumberRange.LowerResult * 10 - 1;
            break;
          }
        } while (!NumberRange.WithinBounds(true, result) && sw.ElapsedMilliseconds <= CycleTime);
        sw.Stop();
        if (sw.ElapsedMilliseconds > CycleTime)
        {
          SymbolRecord(_lastExercise);
          _groupTable[_lastExercise] = GroupSizes(SymbolTable[_lastExercise]);
          continue;
        }
        if (Debug)
        {
          if (worker.CancellationPending) return;
          var testNums = Array.ConvertAll(nums, input => double.IsNaN(input) ? 0M : (decimal)input);
          TestGenerated(testNums, (decimal)result, symbols, groups);
        }
        Results[_lastExercise] = result;
        Numbers[_lastExercise++] = nums;
        break;
      }
    }

    private double Number(out double num, byte index, int precision, bool division = false)
    {
      if (Inclusions.Occurrences[_lastExercise][index] && Inclusions.Count != 0)
      {
        var temp = Inclusions.Get(precision);
        if (!division) num = temp[Rnd.Next(Inclusions.Count)];
        else do num = temp[Rnd.Next(Inclusions.Count)]; while (num == 0);
        return num;
      }
      if (precision > Precision) precision = Precision;
      var epsilon = Math.Pow(10, -(precision < 0 ? precision : Rnd.Next(precision + 1)));
      if (Exclusions.Occurrences[_lastExercise][index] && Exclusions.Count != 0)
        do
        {
          num = Rnd.NextDouble() * (NumberRange.UpperNumber - NumberRange.LowerNumber + epsilon);
          num = Math.Floor((num + NumberRange.LowerNumber) / epsilon) * epsilon;
          if (division && num == 0) num = double.NaN;
        } while (double.IsNaN(num) || Exclusions.Contains(Exclusions.Absolute ? Math.Abs(num) : num));
      else
        do
        {
          num = Rnd.NextDouble() * (NumberRange.UpperNumber - NumberRange.LowerNumber + epsilon);
          num = Math.Floor((num + NumberRange.LowerNumber) / epsilon) * epsilon;
          if (division && num == 0) num = double.NaN;
        } while (double.IsNaN(num));
      return num;
    }

    #region Debug

    private void TestGenerated(IList<decimal> nums, decimal result, IList<Symbols.Type> symbols, IList<byte> groupSizes)
    {
      TestNums(nums, result);
      TestResult(nums, result, symbols, groupSizes);
    }

    private void TestNums(IList<decimal> nums, decimal result)
    {
      if (NumericalOccurrences.HasPrecision(result) > Precision && Precision > 0)
        throw new Exception("Result is too precise");
      if (!NumberRange.WithinBounds(true, (double)result)) throw new Exception("Result out of bounds");
      for (byte n = 0; n < nums.Count; n++)
      {
        if (nums[n] == decimal.MaxValue) throw new Exception("Not every number has been generated!");
        if (NumericalOccurrences.HasPrecision(nums[n]) > Precision && Math.Abs(nums[n]) > 0)
          throw new Exception("Number is too precise");
        if (!NumberRange.WithinBounds(false, (double)nums[n]) && !Inclusions.Contains((double)nums[n]))
          throw new Exception("Number out of bounds");
      }
    }

    private void TestResult(
      IList<decimal> nums,
      decimal result,
      IList<Symbols.Type> symbols,
      IList<byte> groupSizes
      )
    {
      var temp = 0M;
      byte n = 0;
      for (byte gs = 0; gs < groupSizes.Count; gs++)
      {
        var groupStart = n;
        var group = nums[n];
        if (n != 0) if (symbols[n - 1] == Symbols.Type.Difference) group *= -1;
        while (++n < groupSizes[gs] + groupStart)
          if (symbols[n - 1] == Symbols.Type.Division) group /= nums[n];
          else if (symbols[n - 1] == Symbols.Type.Product) group *= nums[n];
          else if (symbols[n - 1] == Symbols.Type.Difference) group -= nums[n];
          else if (symbols[n - 1] == Symbols.Type.Sum) group += nums[n];
        temp += group;
      }
      if (Math.Abs(Math.Round(result - temp, Precision > 0 ? Precision : 0)) > 0)
        throw new Exception("The result is incorrect!");
    }

    #endregion

    #region Execution

    public void Run(long lifeTime = long.MaxValue)
    {
      if (IsBusy()) throw new InvalidOperationException();
      var sw = new Stopwatch();
      sw.Start();
      Update();
      for (var t = 0; t < ThreadCount; t++) _workers[t].RunWorkerAsync(t);
      var delay = new Thread(() =>
      {
        var monitor = new Timer(50);
        monitor.Elapsed += (sender, e) =>
        {
          if (_sessions.Any(session => !session?.Initialized ?? false)) return;
          Initialized = true;
          if (!Completed && sw.ElapsedMilliseconds < lifeTime) return;
          sw.Stop();
          var interval = (int)monitor.Interval;
          while (IsBusy()) Thread.Sleep(interval);
          Dispose();
          monitor.Dispose();
        };
        while (_sessions.Where(session => session == null).GetEnumerator().MoveNext()) Thread.Sleep(20);
        monitor.Start();
      })
      {
        IsBackground = false,
        Name = "Delay"
      };
      delay.Start();
    }

    public bool IsBusy()
    {
      var t = 0;
      while (t < ThreadCount && !_workers[t].IsBusy) t++;
      return t != ThreadCount;
    }

    public void Cancel()
    {
      for (var t = 0; t < ThreadCount; t++) _workers[t].CancelAsync();
    }

    public string[][] Output()
    {
      var output = new string[Numbers.Length][];
      var e = 0;
      for (var o = 0; o < output.Length; o++)
      {
        output[o] = new string[2];
        if (Numbers[e] == null) continue;
        for (var n = 0; n < Numbers[e].Length; n++)
        {
          var num = Numbers[e][n];
          output[o][0] += num < 0 ? "(" + num + ')' : num + "";
          if (n < Numbers[e].GetUpperBound(0)) output[o][0] += Symbols.TypeToString(SymbolTable[e][n]);
        }
        output[o][1] += Math.Round(Results[e++], Precision > 0 ? Precision : 0);
      }
      return output;
    }

    #endregion
  }
}